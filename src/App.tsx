import './App.css'
import { ChangePassword,
  EnrollCreatorCache,
  ErrorBanner,
  GuestConfirmation,
  LoginCreatorCache,
  PasswordUpdated,
  RecoverPasswordCreatorCache,
  SendCode,
  TransmitDebugger,
  VerifyCode, } from './Components'
import { useTransmitRef } from './Contexts'

function App() {
  const { transmitRef } = useTransmitRef()
  const { activeScreen } = transmitRef.useTransmit()

  return (
    <div className="App">
      <TransmitDebugger />
      <ErrorBanner />
      {
        activeScreen.type === 'Unauthenticated' && (
          <>
            <EnrollCreatorCache />
            <LoginCreatorCache />
            <RecoverPasswordCreatorCache />
          </>
        )
      }
      <SendCode />
      <VerifyCode />
      <ChangePassword />
      <PasswordUpdated />
      <GuestConfirmation />
    </div>
  );
}

export default App;

import React, { createContext, FC, useContext, useEffect, useState } from 'react'

import { Session } from '@ally/transmitigator'

import { useTransmitRef } from './TransmitContext'

const ACCESS_TOKEN_REFRESH_INTERVAL = (5 * 60 - 10) * 1000 // 10 sec before 5 minutes

export interface SessionContextProps {
  session: Session
}

const SessionContext = createContext<SessionContextProps>({} as SessionContextProps)

export const useSession = (): SessionContextProps =>
  useContext(SessionContext)

export const SessionProvider: FC = ({ children }) => {
  const { transmitRef } = useTransmitRef()
  const { actions, sessionStatus } = transmitRef.useTransmit()
  const [session, setSession] = useState<Session>({} as Session)

  useEffect(() => {
    if (sessionStatus.status === 'Authenticated') {
      setSession(sessionStatus.data)
    } else {
      setSession({} as Session)
    }
  }, [sessionStatus.status])

  useEffect(() => {
    if (sessionStatus.status === 'Authenticated') {
      const intervalId = setInterval(() => {
        console.log('SessionContext: Refreshing access token')
        // const { access_token, userNamePvtEncrypt: username, } = session

        // This won't work when developing locally as apigee requires the `Ally-CIAM-Token`
        // which is tied to the `.ally.com` domain
        // actions.refreshAccessToken({
        //   access_token,
        //   username,
        // })
      }, ACCESS_TOKEN_REFRESH_INTERVAL)

      console.log(`SessionContext: Starting interval ${intervalId}`)

      return () => {
        console.log(`SessionContext: Clearing interval ${intervalId}`)
        clearInterval(intervalId)
      }
    }
  }, [sessionStatus.status, session])

  return (
    <SessionContext.Provider value={{ session }}>
      {children}
    </SessionContext.Provider>
  )
}

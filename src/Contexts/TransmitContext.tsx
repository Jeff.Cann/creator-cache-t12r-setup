import React, { createContext, FC, useContext } from 'react'

import Transmitigator, { TransmitRef } from '@ally/transmitigator'

export interface TransmitContext {
  transmitRef: TransmitRef
}

const TransmitContext = createContext<TransmitContext>({} as TransmitContext)

export const useTransmitRef = (): TransmitContext =>
  useContext(TransmitContext)

const transmitigator = Transmitigator({
  app: 'ciam_web_creator_cache',
  server: 'https://secure-dev.ally.com/acs/customers/authenticate',
  application: {
    id: 'ALLYCREATORCACHE',
    name: 'ACC',
    version: '1.0',
    apigeeApiKey: 'wU5VdAI4fv7jgsaAOJDREixc3MOaUs1R', // this is the secure-dev api key, should this be used?
  }
})

export const TransmitProvider: FC = ({ children }) => {
  const hookInstance = transmitigator.useTransmit()
  const transmitRef = {
    ...transmitigator,
    useTransmit: () => hookInstance,
  }

  return (
    <TransmitContext.Provider value={{ transmitRef }}>
      {children}
    </TransmitContext.Provider>
  )
}

import { useTransmitRef } from '../Contexts/TransmitContext'
import './TransmitDebugger.css'

function TransmitDebugger() {
  const { transmitRef } = useTransmitRef()
  const transmit = transmitRef.useTransmit()

  return (
    <div className="t12r-debugger">
      <h3>Transmitigator <i>activeScreen</i></h3>
      <pre className="active-screen-state">
        {JSON.stringify(transmit.activeScreen, undefined, 2)}
      </pre>

      <h3>Transmitigator <i>sessionStatus</i></h3>
      <pre className="active-screen-state">
        {JSON.stringify(transmit.sessionStatus, undefined, 2)}
      </pre>
    </div>
  )
}

export default TransmitDebugger

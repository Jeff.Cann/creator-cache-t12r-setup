import { useTransmitRef } from '../Contexts/TransmitContext'

function PasswordUpdated() {
  const { transmitRef } = useTransmitRef()
  const { activeScreen } = transmitRef.useTransmit()

  if (activeScreen.type !== 'PasswordUpdated') {
    return <></>
  }

  return (
    <>
      <h2>Password Updated</h2>
      <p>Password updated successfully</p>
    </>
  )
}

export default PasswordUpdated

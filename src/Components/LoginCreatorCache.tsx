import { useTransmitRef } from '../Contexts/TransmitContext'

function LoginCreatorCache() {
  const { transmitRef } = useTransmitRef()
  const transmit = transmitRef.useTransmit()

  const handleOnSubmit = (e: any) => {
    e.preventDefault()

    const username = e.target[0].value
    const password = e.target[1].value

    transmit.actions.loginCreatorCache({
      username,
      password,
    })
  }

  return (
    <div>
      <h2>Login Creator Cache</h2>
      <form onSubmit={handleOnSubmit}>
        <input placeholder="Username" />
        <input placeholder="Password" />
        <button type="submit">Login</button>
      </form>
    </div>
  )
}

export default LoginCreatorCache

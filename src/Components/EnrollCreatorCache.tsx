import { useTransmitRef } from '../Contexts/TransmitContext'

function EnrollCreatorCache() {
  const { transmitRef } = useTransmitRef()
  const transmit = transmitRef.useTransmit()

  const handleOnSubmit = (e: any) => {
    e.preventDefault()

    const email = e.target[0].value
    const firstName = e.target[1].value
    const lastName = e.target[2].value
    const password = e.target[3].value
    const phoneNumber = e.target[4].value

    transmit.actions.enrollCreatorCache({
      email,
      firstName,
      lastName,
      password,
      phoneNumber,
    })
  }

  return (
    <div>
      <h2>Enroll Creator Cache</h2>
      <form onSubmit={handleOnSubmit}>
        <input placeholder="Email" />
        <input placeholder="First Name" />
        <input placeholder="Last Name" />
        <input placeholder="Password" />
        <input placeholder="Phone Number" />
        <button type="submit">Enroll</button>
      </form>
    </div>
  )
}

export default EnrollCreatorCache

import { useTransmitRef } from '../Contexts/TransmitContext'

function GuestConfirmation() {
  const { transmitRef } = useTransmitRef()
  const { activeScreen } = transmitRef.useTransmit()

  const handleOnClick = (e: any) => {
    e.preventDefault()

    activeScreen.payload.onComplete()
  }

  if (activeScreen.type !== 'GuestConfirmation') {
    return <></>
  }

  return (
    <>
      <h2>GuestConfirmation</h2>
      <p>
        Enrolled successfully!
      </p>
      <button onClick={handleOnClick}>Continue</button>
    </>
  )
}

export default GuestConfirmation

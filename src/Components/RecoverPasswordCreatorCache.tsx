import { useTransmitRef } from '../Contexts/TransmitContext'

function RecoverPasswordCreatorCache() {
  const { transmitRef } = useTransmitRef()
  const transmit = transmitRef.useTransmit()

  const handleOnSubmit = (e: any) => {
    e.preventDefault()

    const username = e.target[0].value
    const lastName = e.target[1].value

    transmit.actions.recoverPasswordCreatorCache({
      username,
      lastName,
    })
  }

  return (
    <div>
      <h2>Recover Password Creator Cache</h2>
      <form onSubmit={handleOnSubmit}>
        <input placeholder="Username" />
        <input placeholder="Last Name" />
        <button type="submit">Submit</button>
      </form>
    </div>
  )
}

export default RecoverPasswordCreatorCache

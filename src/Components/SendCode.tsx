import { useTransmitRef } from '../Contexts/TransmitContext'

function SendCode() {
  const { transmitRef } = useTransmitRef()
  const { activeScreen } = transmitRef.useTransmit()

  const handleOnClick = () => {
    activeScreen.payload.onComplete(activeScreen.payload.targets[0] as any)
  }

  if (activeScreen.type !== 'SendCode') {
    return <></>
  }

  return (
    <>
      <h2>Send Code</h2>

      {activeScreen.payload.targets.map((target) => (
        <div>
          {target._description}
        </div>
      ))}

      <button onClick={handleOnClick}>Continue</button>
    </>
  )
}

export default SendCode

import { useTransmitRef } from '../Contexts/TransmitContext'

import './ErrorBanner.css'

function ErrorBanner() {
  const { transmitRef } = useTransmitRef()
  const { activeScreen: { payload } } = transmitRef.useTransmit()

  if (!payload?.error) {
    return <></>
  }

  let errorMessage = 'Something went wrong. Please try again'

  switch (payload.error) {
    case 'CIAM_E08_00_013':
      errorMessage = 'Email already exists'
      break
    case 'CIAM_E08_01_008':
      errorMessage = 'Invalid phone number'
      break
    case 'CIAM_E08_01_007':
      errorMessage = 'Invalid email address'
      break
    case 'CIAM_E01_09_001':
      errorMessage = 'Email not in Creator Cache beta'
      break
    case 'CIAM_E08_07_001':
      errorMessage = 'Combined last name and email does not exist in LDAP'
      break
    default:
  }

  return (
    <>
      <h2>Error</h2>
      <div className="error">{errorMessage} ({payload.error})</div>
    </>
  )
}

export default ErrorBanner

import React from 'react'
import { useTransmitRef } from '../Contexts/TransmitContext'
import { fetchOtp } from '../utils'
import './VerifyCode.css'

function VerifyCode() {
  const { transmitRef } = useTransmitRef()
  const { activeScreen } = transmitRef.useTransmit()
  const [otpCode, setOtpCode] = React.useState()

  const handleOnSubmit = (e: any) => {
    e.preventDefault()

    const otpCode = e.target[0].value
    activeScreen.payload.onComplete(otpCode)
  }

  const handleResendCode = (e: any): void => {
    e.preventDefault()

    activeScreen.payload.handleResend()
  }

  const fetchOtpCode = (): void => {
    fetchOtp().then((code) => {
      setOtpCode(code)
    })
  }

  if (activeScreen.type !== 'VerifyCode') {
    return <></>
  }

  return (
    <>
      <h2>Verify Code</h2>
      <form onSubmit={handleOnSubmit}>
        <div className="otp-input-wrapper">
          <input id="otpCode" placeholder="OTP Code" value={otpCode} />
          <button onClick={fetchOtpCode}>Fetch OTP</button>
          <button onClick={handleResendCode}>Resend Code</button>
        </div>
        <button type="submit">Continue</button>
      </form>
    </>
  )
}

export default VerifyCode

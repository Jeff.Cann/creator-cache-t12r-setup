import { useTransmitRef } from '../Contexts/TransmitContext'

function VerifyCode() {
  const { transmitRef } = useTransmitRef()
  const { activeScreen } = transmitRef.useTransmit()

  const handleOnSubmit = (e: any) => {
    e.preventDefault()

    const newPassword = e.target[0].value
    activeScreen.payload.onComplete({ newPassword })
  }

  if (activeScreen.type !== 'ChangePasswordCreatorCache') {
    return <></>
  }

  return (
    <>
      <h2>Change Password</h2>
      <form onSubmit={handleOnSubmit}>
        <input placeholder="New password" />
        <button type="submit">Continue</button>
      </form>
    </>
  )
}

export default VerifyCode

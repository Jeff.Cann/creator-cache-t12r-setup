export async function fetchOtp() {
  const fetchOptions = {
    credentials: 'same-origin'
  } as RequestInit

  let sid
  let did

  // In LLE's transmitigator will store otpHeaders in sessionStorage
  const otpHeaders = sessionStorage.getItem('otpHeaders')
  if (otpHeaders) { // Transmit
    const parsedOtpHeaders = JSON.parse(otpHeaders)
    sid = parsedOtpHeaders.session_id
    did = parsedOtpHeaders.device_id
  }

  if (sid && did) {
    return await fetch(`https://secure-dev.ally.com/acs/otp?sid=${sid}&did=${did}`, fetchOptions).then(response => response.json()).then(json => json.otp_code)
  }
}

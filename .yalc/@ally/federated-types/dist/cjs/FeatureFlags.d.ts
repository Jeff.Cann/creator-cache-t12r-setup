import { LDClient } from 'launchdarkly-js-client-sdk';
import { LoadState } from '@ally/use-data-domain';
/**
 * Represents the user object for the FeatureFlags service.
 * Currently, this maps to LaunchDarkly's interface.
 */
export interface FFUser {
    key?: string;
    anonymous?: boolean;
}
interface FeatureFlagClient extends LDClient {
    loadState: LoadState;
}
export declare type FeatureFlagClientName = 'bank' | 'routing' | 'auto';
export declare type FeatureFlags = Record<FeatureFlagClientName, FeatureFlagClient> & FeatureFlagClient;
export {};

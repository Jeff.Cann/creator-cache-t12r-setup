"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SessionStatuses = void 0;
var SessionStatuses;
(function (SessionStatuses) {
    SessionStatuses["Rehydrating"] = "Rehydrating";
    SessionStatuses["Authenticated"] = "Authenticated";
    SessionStatuses["Authenticating"] = "Authenticating";
    SessionStatuses["Unauthenticated"] = "Unauthenticated";
})(SessionStatuses = exports.SessionStatuses || (exports.SessionStatuses = {}));
//# sourceMappingURL=Session.js.map
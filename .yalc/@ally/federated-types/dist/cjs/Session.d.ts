import { Session as TransmitSession, AllyUserRole as TransmitAllyUserRole, LegacySession, LogoutOptions } from '@ally/transmitigator';
export interface SessionData extends TransmitSession {
    chatTokenExpiry?: number;
}
export declare type LegacySessionData = LegacySession;
export declare type AllyUserRole = TransmitAllyUserRole;
export declare type LOB = 'guest' | 'creditcard' | 'auto' | 'bank' | 'mortgage' | 'investment' | 'wealth';
export declare enum SessionStatuses {
    Rehydrating = "Rehydrating",
    Authenticated = "Authenticated",
    Authenticating = "Authenticating",
    Unauthenticated = "Unauthenticated"
}
export declare type SessionStatus = keyof typeof SessionStatuses;
export declare type BaseSession = {
    data: SessionData | null;
    status: SessionStatus;
    destroy: (options: LogoutOptions) => unknown;
};
/**
 * Represents an active user Session.
 * Includes functions for setting and deleting the current session. If `data`
 * is non-null, the session is active and `isAuthenticated` will be true.
 */
export interface Session {
    data: SessionData | null;
    status: SessionStatus;
    delSession: (reason?: string) => Promise<void>;
    setSession: (input: Partial<BaseSession> | null) => void;
    requireAuth: (redirect?: string | false) => void;
    accessTokenRefreshing?: boolean;
    mayBeHasLegacySession?: boolean;
    fetchLegacySession: (tokenType?: string) => any;
    addAllyId: () => Promise<string>;
}

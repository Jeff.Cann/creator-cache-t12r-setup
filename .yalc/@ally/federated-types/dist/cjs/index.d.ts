import { HostData } from '@ally/data-types-host';
import { TransmitRef } from '@ally/transmitigator';
import { EnvironmentInfo } from '@ally/which-env';
import { ParsedQuery } from 'query-string';
import { ActionBar } from './ActionBar';
import { Analytics } from './Analytics';
import { BrowserHistory } from './BrowserHistory';
import { FeatureFlags } from './FeatureFlags';
import { GlobalNav } from './GlobalNav';
import { RemoteLogger } from './Log';
import { Session } from './Session';
import { SubNav } from './SubNav';
import { Utils } from './Utils';
/**
 * Host Services provided to each remote app from the client app via
 * `channel.send`. The remote app can receive these services by listening to
 * the host channel using `channel.listen`. Updates to these values will be
 * sent through the channel.
 */
export interface HostServices {
    analytics: Analytics;
    env: EnvironmentInfo;
    featureFlags: FeatureFlags;
    hostData: HostData;
    log: RemoteLogger;
    subnav: SubNav;
    actionBar: ActionBar;
    globalnav: GlobalNav;
    session: Session;
    utils: Utils;
    query: ParsedQuery<string>;
    transmitRef: TransmitRef;
    /**
     * Browser history object. Remotes must pass this object to
     * their Router component as the history prop in order to
     * properly receive url parameter changes caused by the host router
     */
    browserHistory: BrowserHistory;
    redirectToMortgage: (loanNumber?: string, opts?: {
        onCancel?: () => void;
    }) => void;
}
export * from './ActionBar';
export * from './Analytics';
export * from './BrowserHistory';
export * from './FeatureFlags';
export * from './GlobalNav';
export * from './Log';
export * from './Session';
export * from './SubNav';
export * from './Utils';

import { AuthSessionData, FetchOptions, FetchResponse } from '@ally/fido';
export interface DomainFetchOptions extends Omit<FetchOptions, 'baseUrl' | 'endpointUrl'> {
    url: string;
}
export declare type DomainFetchSignature = <T = unknown>(options: DomainFetchOptions, sessionData?: AuthSessionData) => Promise<FetchResponse<T>>;
declare type ACSFetchSignature = DomainFetchSignature;
declare type CAPIFetchSignature = DomainFetchSignature;
declare type AssetsFetchSignature = DomainFetchSignature;
export declare type FetchUtils = {
    assets: AssetsFetchSignature;
    ACS: ACSFetchSignature;
    CAPI: CAPIFetchSignature;
};
/**
 * Type 'number' is added to `selectedCategory` and `openHelpModal` to
 * support the legacy signature. This is to enable
 * backwards compatability for consumers that haven't
 * updated this library, that pass in a number or string
 * via enum.
 */
export declare type Utils = {
    fetch: FetchUtils;
    helpModalState: {
        status: 'Open' | 'Closed';
        selectedCategory: string | null | number;
    };
    openHelpModal: (category?: string | number) => void;
    setHelpModalState: (options: {
        status: 'Open' | 'Closed';
        selectedCategory: string | null | number;
    }) => void;
};
export {};

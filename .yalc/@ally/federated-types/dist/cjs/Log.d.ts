import { Logger } from '@ally/whisper';
/**
 * Extends the whisper interface to include a `track` function. The host
 * provides this as a wrapper around `logRocket.track`.
 */
export declare type RemoteLogger = Logger & {
    track: (event: string) => void;
};

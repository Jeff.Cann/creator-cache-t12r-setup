import { useHistory } from 'react-router-dom';
export declare type BrowserHistory = ReturnType<typeof useHistory>;

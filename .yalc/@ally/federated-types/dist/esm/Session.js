export var SessionStatuses;
(function (SessionStatuses) {
    SessionStatuses["Rehydrating"] = "Rehydrating";
    SessionStatuses["Authenticated"] = "Authenticated";
    SessionStatuses["Authenticating"] = "Authenticating";
    SessionStatuses["Unauthenticated"] = "Unauthenticated";
})(SessionStatuses || (SessionStatuses = {}));
//# sourceMappingURL=Session.js.map
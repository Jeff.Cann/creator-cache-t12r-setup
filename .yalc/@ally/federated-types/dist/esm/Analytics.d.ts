export declare type TrackEventParams = {
    eventType: string;
    eventName: string;
    eventTag: string;
};
export declare type TrackPageViewParams = {
    eventType: string;
    pageName: string;
    options?: Record<string, unknown>;
};
export declare type TrackImpressionParams = {
    eventType: string;
    impressions: Record<string, unknown>;
};
export declare type Analytics = {
    mountDigitalDataObject: () => void;
    trackEvent: (params: TrackEventParams) => void;
    trackPageView: (params: TrackPageViewParams) => void;
    trackImpression: (params: TrackImpressionParams) => void;
};

import { ReactElement } from 'react';
declare type MetadataType = Record<string, string>;
interface BaseNavItemSchema {
    content: string | ReactElement;
    key: string;
    metadata?: MetadataType;
    isHiddenMobile?: boolean;
}
export interface SubNavButtonSchema extends BaseNavItemSchema {
    type: 'button';
    onClick: () => void;
}
export interface SubNavLinkSchema extends BaseNavItemSchema {
    type: 'link';
    url: string;
    onClick?: () => void;
}
export interface SubNavDropdownSchema {
    type: 'menu';
    items: SubNavSchema;
    triggerContent: string | ReactElement;
    key: string;
    metadata?: MetadataType;
    isHiddenMobile?: boolean;
}
export declare type SubNavSchema = (SubNavLinkSchema | SubNavButtonSchema | SubNavDropdownSchema)[];
export interface SubNav {
    set: (schema: SubNavSchema | ((prev: SubNavSchema) => SubNavSchema)) => void;
    schema: SubNavSchema;
    Schema: SchemaClass;
}
export interface SchemaFactoryInstance {
    schema: SubNavSchema;
    sortOrder: string[];
    set(schema: SubNavSchema): SchemaFactoryInstance;
    add(partialSchema: SubNavSchema): SchemaFactoryInstance;
    remove(itemsToRemove: string[]): SchemaFactoryInstance;
    reset(): SchemaFactoryInstance;
}
export declare type SchemaClass = {
    new (schema: SubNavSchema, sortOrder?: string[]): SchemaFactoryInstance;
};
export {};

import { ReactElement } from 'react';
export interface LinkConfig {
    content: string;
    path: string;
}
export interface RemoteConfig {
    type: 'REMOTE';
    innerContent: ReactElement;
}
export interface ActionBar {
    notifications: {
        set(remoteNotifications: RemoteConfig[]): void;
        get(): RemoteConfig[];
    };
}

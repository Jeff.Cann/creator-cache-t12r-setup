import { Dispatch, SetStateAction } from 'react';
export interface GlobalNav {
    setIsHidden: Dispatch<SetStateAction<boolean>>;
    isHidden: boolean;
}

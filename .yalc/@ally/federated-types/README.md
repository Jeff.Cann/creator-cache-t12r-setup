# @ally/federated-types

**Types shared by the AllyNext host application and its remote applications.**  
This package exports the types for the AllyNext host app's `HostServices`. That
is, the services that are passed to each remote application through the
Federator's `Channel` message passing mechanism.

The types in this package are consumed by both the host and its consuming
remotes. Types here should be limited in scope to only the services provided by
the host. If it's not part of the host's services consumer interface **do not
add it here.**

## Scripts

**This package comes equipped with the following scripts:**

| Name   | Description                              |
| ------ | ---------------------------------------- |
| format | Formats the package code using prettier. |
| lint   | Runs `eslint` against the package.       |

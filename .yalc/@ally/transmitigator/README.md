# @ally/transmitigator

> Mitigating the woes of the Transmit SDK

## Usage

**Note:** If you're working from within the ally-next ecosystem, you should be using the instance of transmitigator provided by in host services.

```ts
const { transmitRef } = useHostServices()

transmitRef.loginWithUser({
  username: '<username>',
  password: '<password>',
})
```

This package exports a factory function that will return a set of "actions" for
interacting with the Transmit SDK. Each action accepts an optional `options`
argument and returns a `Promise` that will resolve with a [Session](#session)
object.

```ts
import transmitigator from '@ally/transmitigator'

const { logout, rehydrate, loginWithUser, forgotPassword } = transmitigator({
  /* Optional Config */
})

// An example of rehydration/logging in... session will either be null, or contain the user's session
let session = await rehydrate()

if (!session) {
  session = await loginWithUser({
    username: '<username>',
    password: '<password>',
  })
}
```

## Transmitigator Factory Config

The factory function takes some optional Transmit configuration, but for AOS/DAO
purposes, _you'll probably want the defaults._

```ts
interface Config {
  // The application name to pass to transmit.
  // Defaults to "ciam_web".
  app: string
  // An instance of the TransmitSDK, will default to using the one on the window
  // That is: window.xmsdk.XmSdk()
  sdk: TransmitSDK.TransmitSDKXm
  // The URL of the Transmit server to use. Defaults to using the correct one
  // the the current runtime environment based on the window location.
  server: string
  // The transmit logging verbosity level.
  // Must pass in a LogLevel enum value (exported here as LogLevel)
  logLevel: TransmitSDK.LogLevel
  // A UIHandler class.
  // Can provide a custom one if you want custom views (don't, it's complicated).
  UIHandler: DispatchUIHandler
  // If true, a session will only exist for the current tab. Rehydration will be
  // skipped and the user will be prompted for login if a new tab is opened
  // (or even re-opened from history).
  // Defaults to `false`.
  allowCrossTabSession?: boolean
}
```

## Session

Most action return an optional `Session` object.

```ts
export interface Session {
  mfa?: MFA
  mxEnrolled: boolean
  jwtToken: string
  access_token: string
  allyUserRole: AllyUserRole
  lastLoginTime: string | null
  resourceIndex: string
  AwsAccessToken: string
  rememberMeToken: string | null
  CSRFChallengeToken: string
  userNamePvtEncrypt: string
  profileVerification?: AnnualVerification
  passwordHintPvtBlock: string
  deviceData: DeviceData
}
```

## Action Input

The following are the options that are available for each action:

```ts
// `/actions/logout.ts`
interface LogoutOptions {
  /* None... */
}

// `/actions/login-with-user.ts`
interface LoginWithUserOptions {
  username: string
  password: string
}

// `/actions/forgot-password.ts`
interface ForgotPasswordOptions {
  ssn: string
  username: string
  recoveryOptions: 'changePassword' | 'emailHint'
}

// `/actions/rehydrate.ts`
interface RehydrateOptions {
  rememberMeFlag?: string
  rememberMeToken?: string
  inv_comeback_msg_id?: string
  sf_login_msg_id?: string
}

// `/actions/cenlar-sso.ts`
interface CenlarSSOOptions {
  access_token: string
  deviceData?: DeviceData
  loan_number: string
  api_key: string
  refresh_token: string
  journey_version?: string
  username: string
}

// `/actions/add-ally-id.ts`
interface AddAllyIdOptions {
  access_token: string
  username: string
  deviceData?: DeviceData
  journey_version?: string
}

// `/actions/get-atomic-access-token.ts`
interface GetAtomicAccessTokenOptions {
  access_token: string
  username: string
  guid: string
  accountsArray: Account[]
  deviceData?: DeviceData
  journey_version?: string
}
interface Account {
  accountNumber: string
  routingNumber: string
  title: string
  type: string // 'savings', 'checking', or 'paycard'
}

// `/actions/refresh-access-token.ts`
interface RefreshAccessTokenOptions {
  access_token: string
  username: string
  deviceData?: DeviceData
  tokenID?: string
  destination?: string
  journey_version?: string
}

// `/actions/refresh-access-token.ts`
interface RefreshAccessTokenOptions {
  access_token: string
  username: string
  deviceData?: DeviceData
  tokenID?: string
  destination?: string
  journey_version?: string
}
```

## Action responses

Some actions do not return a standard session object, instead they have custom responses.

```ts
// transfer-ticket
{
  type: 'ticket',
  ticketID: '<ticketID>',
}

// legacy-session
{
  type: 'legacySession',
  legacySession: {
    CSRFChallengeToken: '<CSRFChallengeToken>',
    AwsAccessToken: '<AwsAccessToken',
    legacyChatToken: '<legacyChatToken',
  },
}

// chat token refresh
{
  type: 'chatTokenRefresh',
  legacyChatToken: '<legacyChatToken>',
}

// cenlar-sso
{
  type: 'cenlarSso',
  cenlarSso: {
    redirect_url: '<redirect_url>',
  },
}

// get-security-profile
{
  type: 'getSecurityProfile',
  securityProfile: {
    passwordHint: '<passwordHint>',
    alwaysChallenge: {
      web: false,
      mobile: false,
    },
    isBiometricEnrolled: false,
    mfaDeliveryMethods: [
      {
        deliveryMethodId: 'string',
        deliveryMethodType: 'string',
        deliveryMethodValuePvtBlock: 'string',
      }
    ],
  }
}

// get-atomic-access-token
{
  type: 'getAtomicAccessToken',
  atomic_access_token: '<atomic_access_token>',
}
```

## Feature Flags

Feature Flags can be passed to Transmitigator after initialization using the `setFeatureFlags()` function

```ts
import transmitigator from '@ally/transmitigator'

const transmitigator = transmitigator({})
const transmit = transmitigator.useTransmit()

transmit.setFeatureFlags({}: LDFlagSet)
```

Feature Flags are automatically added to all outgoing journey invocations based on the whitelist defined in getFlagParams.ts. Simply add feature flags by name to be included.

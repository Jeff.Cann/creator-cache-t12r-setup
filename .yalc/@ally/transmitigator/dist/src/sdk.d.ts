import { NewRiskID } from './types';
export declare const isObject: (x: unknown) => boolean;
export declare const getSdk: (global?: Window & typeof globalThis) => Required<Pick<Window, 'xmsdk' | 'com'>>;
declare const com: {
    ts: {
        mobile: {
            sdk: typeof TransmitSDK;
        };
    };
}, xmsdk: {
    XmSdk: () => TransmitSDK.TransmitSDKXm;
};
declare const myRiskID: NewRiskID;
declare const LogLevel: typeof TransmitSDK.LogLevel, FormInput: typeof TransmitSDK.FormInput, OtpChannel: typeof TransmitSDK.OtpChannel, RedirectType: typeof TransmitSDK.RedirectType, RedirectInput: typeof TransmitSDK.RedirectInput, PasswordInput: typeof TransmitSDK.PasswordInput, ControlRequest: typeof TransmitSDK.ControlRequest, ConfirmationInput: typeof TransmitSDK.ConfirmationInput, ControlRequestType: typeof TransmitSDK.ControlRequestType, AuthenticationError: typeof TransmitSDK.AuthenticationError, UIAssetsDownloadMode: typeof TransmitSDK.UIAssetsDownloadMode, RedirectResponseType: typeof TransmitSDK.RedirectResponseType, OtpInputOtpSubmission: typeof TransmitSDK.OtpInputOtpSubmission, OtpInputRequestResend: typeof TransmitSDK.OtpInputRequestResend, SDKConnectionSettings: typeof TransmitSDK.SDKConnectionSettings, InputOrControlResponse: typeof TransmitSDK.InputOrControlResponse, AuthenticationErrorCode: typeof TransmitSDK.AuthenticationErrorCode, JsonDataProcessingResult: typeof TransmitSDK.JsonDataProcessingResult, AuthenticationErrorRecovery: typeof TransmitSDK.AuthenticationErrorRecovery, AuthenticatorSelectionResult: typeof TransmitSDK.AuthenticatorSelectionResult, TargetBasedAuthenticatorInput: typeof TransmitSDK.TargetBasedAuthenticatorInput, TransportRequest: typeof TransmitSDK.TransportRequest, TransportResponse: typeof TransmitSDK.TransportResponse;
export { com, xmsdk, myRiskID, LogLevel, FormInput, OtpChannel, RedirectType, RedirectInput, PasswordInput, ControlRequest, ConfirmationInput, ControlRequestType, AuthenticationError, UIAssetsDownloadMode, RedirectResponseType, OtpInputOtpSubmission, OtpInputRequestResend, SDKConnectionSettings, InputOrControlResponse, AuthenticationErrorCode, JsonDataProcessingResult, AuthenticatorSelectionResult, TargetBasedAuthenticatorInput, AuthenticationErrorRecovery, TransportRequest, TransportResponse, };

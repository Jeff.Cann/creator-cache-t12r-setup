"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteTransmitSession = exports.asUIConsumer = exports.asSession = exports.createSession = exports.handleAtomicAccessToken = exports.handleSecurityProfile = exports.handleCenlarSso = exports.handleChatToken = exports.handleLegacySession = exports.handleTicketResponse = exports.handleResponseV2 = exports.decodeTokenV2 = exports.toConsumableError = exports.delSavedUsername = exports.setSavedUsername = exports.getInvestFastTrackUserIfPresent = exports.getSavedUsernameSliceAndToken = exports.getUnixEpochGMT = exports.getAYearFromNowGMT = exports.getCookie = exports.registerTab = exports.getAppInfoParams = exports.getSSOTicketID = exports.isValidSSOTicket = exports.getTransmitTicket = exports.deleteRefreshTicketID = exports.isValidRefreshTicket = exports.unregisterTab = exports.getIsSameTab = exports.noop = void 0;
var jwt_decode_1 = __importDefault(require("jwt-decode"));
var constants_1 = require("./constants");
var whisper_1 = __importDefault(require("./whisper"));
function noop() {
    /* No operation */
}
exports.noop = noop;
var JEFFERSON_COOKIE = 'jeffersonCookieSunset';
var crypto = window.crypto || window.msCrypto;
var storedWindowName = sessionStorage.getItem('xtabvar');
var domain;
var NODE_ENV = process.env.NODE_ENV;
// Sets the domain value to store on cookies (i.e. the jeffersonCookieSunset).
// Note jest has issues with any domain for some reason...
switch (true) {
    case /localhost|127\.0\.0\.1|0\.0\.0\.0/.test(window.location.href):
        domain = NODE_ENV === 'test' ? '' : "domain=" + window.location.hostname;
        break;
    default:
        /* istanbul ignore next */
        domain = 'domain=.ally.com';
}
function getIsSameTab() {
    return window.name === storedWindowName;
}
exports.getIsSameTab = getIsSameTab;
function unregisterTab() {
    window.name = '';
}
exports.unregisterTab = unregisterTab;
var isValidRefreshTicket = function (ticketID) {
    return typeof ticketID === 'string' && /^iv|sf|sc|tt|au|pr/.test(ticketID);
};
exports.isValidRefreshTicket = isValidRefreshTicket;
var getTicketID = function () {
    var _a = window.location, hash = _a.hash, search = _a.search;
    var hashedSearch = hash && hash.split('?')[1];
    var urlParams = new URLSearchParams(hashedSearch || search);
    var ticketID = urlParams.get('ticketID');
    if (ticketID) {
        whisper_1.default.info({ message: "Detected ticketID: " + ticketID });
    }
    return ticketID;
};
/**
 * We need to delete the ticketID from the URL to prevent potential re-use.
 */
var deleteRefreshTicketID = function () {
    var ticketID = getTicketID();
    if (exports.isValidRefreshTicket(ticketID)) {
        var _a = window.location, hash = _a.hash, search = _a.search, pathname = _a.pathname;
        var _b = __read(hash ? hash.split('?') : ['', ''], 2), hashedPathName = _b[0], hashedSearch = _b[1];
        var path = hashedPathName || pathname;
        var urlParams = new URLSearchParams(search || hashedSearch);
        urlParams.delete('ticketID');
        window.history.replaceState({}, '', "" + path + (urlParams.toString().length > 0 ? "?" + urlParams : ''));
    }
};
exports.deleteRefreshTicketID = deleteRefreshTicketID;
var getTransmitTicket = function () {
    var ticketID = getTicketID();
    if (!exports.isValidRefreshTicket(ticketID)) {
        whisper_1.default.info({ message: "Invalid ticketID prefix: " + ticketID });
        return null;
    }
    return ticketID;
};
exports.getTransmitTicket = getTransmitTicket;
var isValidSSOTicket = function (ticketID) {
    return ticketID === 'SSO_OLA';
};
exports.isValidSSOTicket = isValidSSOTicket;
var getSSOTicketID = function () {
    var ticketID = getTicketID();
    return exports.isValidSSOTicket(ticketID) ? ticketID : undefined;
};
exports.getSSOTicketID = getSSOTicketID;
function getAppInfoParams(_a) {
    var id = _a.id, name = _a.name, version = _a.version, apigeeApiKey = _a.apigeeApiKey;
    return {
        applicationId: id,
        applicationName: name,
        applicationVersion: version,
        'api-key': apigeeApiKey,
        client: {
            name: 'web',
            location: 'secure',
            version: '1.0',
        },
    };
}
exports.getAppInfoParams = getAppInfoParams;
/**
 * Generates a unique random string for the current tab and assigns it to
 * window.name. This is used to prevent sessions across tabs. If the session
 * is created in a window with name=x, and a new tab is opened, the names won't
 * match and rehydration will be skipped.
 */
function registerTab() {
    window.name = crypto.getRandomValues(new Uint32Array(1))[0].toString();
    sessionStorage.setItem('xtabvar', window.name);
}
exports.registerTab = registerTab;
/**
 * Fetches a cookie by name.
 */
function getCookie(name) {
    var _a = __read(("; " + document.cookie).split("; " + name + "="), 2), value = _a[1];
    return value === null || value === void 0 ? void 0 : value.split(';').shift();
}
exports.getCookie = getCookie;
/**
 * Gets a year from now in GMT time.
 * Useful for setting a cookie's expiration from one year from now.
 */
function getAYearFromNowGMT() {
    return new Date(Date.now() + 3.154e10).toUTCString();
}
exports.getAYearFromNowGMT = getAYearFromNowGMT;
/**
 * Used to invalid cookies.
 * Returns exactly `Thu, 01 Jan 1970 00:00:00 GMT`
 */
function getUnixEpochGMT() {
    return new Date(0).toUTCString();
}
exports.getUnixEpochGMT = getUnixEpochGMT;
function getSavedUsernameSliceAndFastTrackValues() {
    var maybeCookie = getCookie(JEFFERSON_COOKIE);
    return maybeCookie ? maybeCookie.split(':') : [];
}
/**
 * If the username has been saved, this retrieves it from the value of the
 * jeffersonCookieSunset in the format: [<username slice>, <token>]
 *
 * If cookie is invest fast track portion only, return empty array
 */
function getSavedUsernameSliceAndToken() {
    var _a = __read(getSavedUsernameSliceAndFastTrackValues(), 2), maybeUser = _a[0], maybeToken = _a[1];
    // invest fast track is formatted ::user:invfast, this results in two empty values
    if (maybeUser && maybeToken) {
        return [maybeUser, maybeToken];
    }
    return [];
}
exports.getSavedUsernameSliceAndToken = getSavedUsernameSliceAndToken;
function getInvestFastTrackUserIfPresent() {
    var cookie = getCookie(JEFFERSON_COOKIE);
    return cookie ? cookie.split(':')[2] : null;
}
exports.getInvestFastTrackUserIfPresent = getInvestFastTrackUserIfPresent;
/**
 * Sets the saved username.
 *
 * Updated requirements:
 *
 * 1. User without save username, not invest
 * **No difference from today, no cookie should be set**
 * 2. User with save username, not invest
 * **pant...:5647389587473829**
 * 3. User with save username, invest
 * **pant...:65748398r7t438473:pant:invfast**
 * 4. User without save username, invest
 * **::pant:invfast**
 */
function setSavedUsername(_a) {
    var username = _a.userNamePvtEncrypt, token = _a.rememberMeToken, isInvestUser = _a.allyUserRole.investment;
    if (!token && !isInvestUser) {
        return;
    }
    whisper_1.default.debug({
        message: 'Found remember me token or user has invest accounts...saving Jefferson Cookie.',
    });
    var shortUsername = username.slice(0, 4);
    var investFastTrack = isInvestUser ? ":" + shortUsername + ":invfast" : '';
    var value = token ? shortUsername + "...:" + token : ':';
    document.cookie = JEFFERSON_COOKIE + "=" + value + investFastTrack + ";path=/;expires=" + getAYearFromNowGMT() + ";" + domain;
}
exports.setSavedUsername = setSavedUsername;
/**
 * Deletes the saved username
 */
function delSavedUsername() {
    var maybeInvestUser = getInvestFastTrackUserIfPresent();
    if (maybeInvestUser) {
        document.cookie = JEFFERSON_COOKIE + "=::" + maybeInvestUser + ":invfast;path=/;expires=" + getAYearFromNowGMT() + ";" + domain;
        return;
    }
    document.cookie = JEFFERSON_COOKIE + "=;path=/;expires=" + getUnixEpochGMT() + ";" + domain;
}
exports.delSavedUsername = delSavedUsername;
/**
 * Converts a "Transmit Error" object to a plain old JavaScript Error.
 * This will return an error object with the following properties:
 * - `code`: The Transmit error code, this is a number.
 * - `data`: An object containing error data about the error that occurred.
 */
function toConsumableError(e) {
    var data = e.getData();
    var code = e.getErrorCode();
    var message = e.getMessage() || 'An unknown error occurred.';
    return Object.assign(new Error(message), {
        code: code,
        data: data,
    });
}
exports.toConsumableError = toConsumableError;
function decodeTokenV2(idToken) {
    var decodedToken = jwt_decode_1.default(idToken);
    var mappedToken = Object.entries(decodedToken).map(function (_a) {
        var _b = __read(_a, 2), k = _b[0], v = _b[1];
        return [
            constants_1.SESSION_KEY_MAP[k] || k,
            v,
        ];
    });
    return Object.fromEntries(mappedToken);
}
exports.decodeTokenV2 = decodeTokenV2;
function handleResponseV2(authenticationResult) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
    var maybeIdToken = (_c = (_b = (_a = authenticationResult) === null || _a === void 0 ? void 0 : _a._internalData) === null || _b === void 0 ? void 0 : _b.json_data) === null || _c === void 0 ? void 0 : _c.id_token;
    var maybeAccessToken = (_f = (_e = (_d = authenticationResult) === null || _d === void 0 ? void 0 : _d._internalData) === null || _e === void 0 ? void 0 : _e.json_data) === null || _f === void 0 ? void 0 : _f.access_token;
    var maybeScope = ((_j = (_h = (_g = authenticationResult) === null || _g === void 0 ? void 0 : _g._internalData) === null || _h === void 0 ? void 0 : _h.json_data) === null || _j === void 0 ? void 0 : _j.scope) || '';
    if (maybeIdToken && maybeAccessToken) {
        var sessionData = decodeTokenV2(maybeIdToken);
        return __assign(__assign({}, sessionData), { access_token: maybeAccessToken, scope: maybeScope });
    }
    return null;
}
exports.handleResponseV2 = handleResponseV2;
function handleTicketResponse(ticketID) {
    whisper_1.default.info({
        message: ['ticketID', ticketID],
    });
    return { type: 'ticket', ticketID: ticketID };
}
exports.handleTicketResponse = handleTicketResponse;
function handleLegacySession(data) {
    var csrfChallengeToken = data.csrfChallengeToken, awsAccessToken = data.awsAccessToken, legacyChatToken = data.legacyChatToken;
    var legacySession = {
        CSRFChallengeToken: csrfChallengeToken,
        AwsAccessToken: awsAccessToken,
        legacyChatToken: legacyChatToken,
    };
    whisper_1.default.info({
        message: ['Legacy Session Data', legacySession],
    });
    return { type: 'legacySession', legacySession: legacySession };
}
exports.handleLegacySession = handleLegacySession;
function handleChatToken(chatToken) {
    var legacyChatToken = chatToken || '';
    whisper_1.default.info({
        message: ['Chat Token Refreshed', { legacyChatToken: legacyChatToken }],
    });
    return { type: 'chatTokenRefresh', legacyChatToken: legacyChatToken };
}
exports.handleChatToken = handleChatToken;
function handleCenlarSso(data) {
    var redirect_url = data.redirect_url;
    var cenlarSso = {
        redirect_url: redirect_url,
    };
    whisper_1.default.info({
        message: ['Cenlar SSO Data', cenlarSso],
    });
    return { type: 'cenlarSso', cenlarSso: cenlarSso };
}
exports.handleCenlarSso = handleCenlarSso;
function handleSecurityProfile(data) {
    var passwordHint = data.passwordHint, alwaysChallenge = data.alwaysChallenge, isBiometricEnrolled = data.isBiometricEnrolled, mfaDeliveryMethods = data.mfaDeliveryMethods;
    var securityProfile = {
        passwordHint: passwordHint,
        alwaysChallenge: alwaysChallenge,
        isBiometricEnrolled: isBiometricEnrolled,
        mfaDeliveryMethods: mfaDeliveryMethods,
    };
    whisper_1.default.info({
        message: ['Get Security Profile Data', securityProfile],
    });
    return { type: 'getSecurityProfile', securityProfile: securityProfile };
}
exports.handleSecurityProfile = handleSecurityProfile;
function handleAtomicAccessToken(data) {
    var atomic_access_token = data.atomic_access_token;
    whisper_1.default.info({
        message: ['Get Atomic Access Token Data', atomic_access_token],
    });
    return { type: 'getAtomicAccessToken', atomic_access_token: atomic_access_token };
}
exports.handleAtomicAccessToken = handleAtomicAccessToken;
/**
 * Creates a session object, or returns a ticketID.
 * This is used either to create a valid session, a ticketID, or an "anonymous" session.
 */
function createSession(transmitResponse) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s;
    if (transmitResponse === void 0) { transmitResponse = null; }
    if (!transmitResponse) {
        return null;
    }
    var deviceData = transmitResponse.deviceData, authenticationResult = transmitResponse.authenticationResult, ignoreTabRegistration = transmitResponse.ignoreTabRegistration, token_type = transmitResponse.token_type;
    // TICKET
    var maybeTicket = (_c = (_b = (_a = authenticationResult) === null || _a === void 0 ? void 0 : _a._internalData) === null || _b === void 0 ? void 0 : _b.json_data) === null || _c === void 0 ? void 0 : _c.ticketID;
    if (maybeTicket) {
        if (!ignoreTabRegistration)
            registerTab();
        return handleTicketResponse(maybeTicket);
    }
    // LEGACY SESSION
    var maybeCSRFToken = (_f = (_e = (_d = authenticationResult) === null || _d === void 0 ? void 0 : _d._internalData) === null || _e === void 0 ? void 0 : _e.json_data) === null || _f === void 0 ? void 0 : _f.csrfChallengeToken;
    if (maybeCSRFToken) {
        if (!ignoreTabRegistration)
            registerTab();
        return handleLegacySession(authenticationResult._internalData.json_data);
    }
    // CHAT TOKEN REFRESH
    // TODO: can this be done without checking the token_type? otherwise this is inflexible and
    // has to be changed with each added token_type value
    if (token_type === 'chat_only' ||
        token_type === 'chat_only_auto_unmigrated') {
        var mayBeLegacyChatToken = (_j = (_h = (_g = authenticationResult) === null || _g === void 0 ? void 0 : _g._internalData) === null || _h === void 0 ? void 0 : _h.json_data) === null || _j === void 0 ? void 0 : _j.legacyChatToken;
        return handleChatToken(mayBeLegacyChatToken);
    }
    // CENLAR SSO
    var maybeRedirectUrl = (_m = (_l = (_k = authenticationResult) === null || _k === void 0 ? void 0 : _k._internalData) === null || _l === void 0 ? void 0 : _l.json_data) === null || _m === void 0 ? void 0 : _m.redirect_url;
    if (maybeRedirectUrl) {
        return handleCenlarSso(authenticationResult._internalData.json_data);
    }
    // SECURITY PROFILE
    var json_data = (_p = (_o = authenticationResult) === null || _o === void 0 ? void 0 : _o._internalData) === null || _p === void 0 ? void 0 : _p.json_data;
    if (json_data) {
        var maybePasswordHint = Object.prototype.hasOwnProperty.call(json_data, 'passwordHint');
        if (maybePasswordHint) {
            return handleSecurityProfile(authenticationResult._internalData.json_data);
        }
    }
    // ATOMIC ACCESS TOKEN
    var maybeAtomicAccessToken = (_s = (_r = (_q = authenticationResult) === null || _q === void 0 ? void 0 : _q._internalData) === null || _r === void 0 ? void 0 : _r.json_data) === null || _s === void 0 ? void 0 : _s.atomic_access_token;
    if (maybeAtomicAccessToken) {
        return handleAtomicAccessToken(authenticationResult._internalData.json_data);
    }
    // SESSION
    var tokenData = handleResponseV2(authenticationResult);
    if (!tokenData)
        return null;
    if (!ignoreTabRegistration)
        registerTab();
    var session = __assign(__assign({}, tokenData), { deviceData: deviceData });
    setSavedUsername(session);
    whisper_1.default.info({
        message: ['Session data', __assign(__assign({}, session), { passwordHintPvtBlock: '*' })],
    });
    if (window.allytm && session.aaosId) {
        ;
        window.allytm.event('customEvent', {
            name: 'profileID',
            type: 'userAttribute',
            attr: {
                user: {
                    attributes: {
                        profileID: session.aaosId,
                    },
                },
            },
        });
    }
    return {
        type: 'session',
        session: session,
    };
}
exports.createSession = createSession;
/**
 * HOF that wraps an action and converts its return value to an optional Session
 * object.
 */
function asSession(f) {
    var _this = this;
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, Promise.resolve()
                        .then(function () { return f.apply(void 0, __spread(args)); })
                        .then(createSession)];
            });
        });
    };
}
exports.asSession = asSession;
/**
 * HOF that wraps an action and sets up the UIHandler prior to invoking the
 * function.
 */
function asUIConsumer(f) {
    return function (config, options) {
        if (options === void 0) { options = {}; }
        var sdk = config.sdk, UIHandler = config.UIHandler;
        var _a = options.onScreenUpdate, onScreenUpdate = _a === void 0 ? noop : _a, _b = options.onScreenLoading, onScreenLoading = _b === void 0 ? noop : _b, _c = options.onScreenError, onScreenError = _c === void 0 ? noop : _c, _d = options.onScreenRedirect, onScreenRedirect = _d === void 0 ? noop : _d;
        var handleScreenUpdate = function (data) {
            whisper_1.default.debug({ message: ['Journey screen dispatched', data] });
            onScreenUpdate(data);
        };
        sdk.setUiHandler(new UIHandler(config, handleScreenUpdate, onScreenError, onScreenLoading, onScreenRedirect));
        return f(config, options);
    };
}
exports.asUIConsumer = asUIConsumer;
function deleteTransmitSession(_a) {
    var sdk = _a.sdk;
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!sdk.currentSession)
                        return [2 /*return*/];
                    whisper_1.default.debug({
                        message: 'Removing existing transmit session',
                    });
                    return [4 /*yield*/, sdk.logout().catch(function (e) {
                            /* istanbul ignore next */ // there is no reason (or easy way to test) a whisper message
                            whisper_1.default.warn({
                                message: ['Unable to log out from SDK. This could be expected.', e],
                            });
                        })];
                case 1:
                    _b.sent();
                    return [2 /*return*/];
            }
        });
    });
}
exports.deleteTransmitSession = deleteTransmitSession;
//# sourceMappingURL=utils.js.map
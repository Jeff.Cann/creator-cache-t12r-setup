"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AV_ROUTE_FLAG = exports.VERIFICATION_STATUS_KEY = exports.STORED_CIF_KEY = exports.INTERRUPTS_STATUS_KEY = exports.PROFILE_VERIFICATION_STATUS = exports.DEVICE_PRINT_ELEMENT_ID = exports.SESSION_KEY_MAP = exports.DEFAULT_POLICY_NAMES = exports.DEFAULT_APP_INFO = exports.DEFAULT_SERVER = exports.DEFAULT_APP = exports.aaosUrl = void 0;
var which_env_1 = __importDefault(require("@ally/which-env"));
// -------------------------------------------------------------------------- //
// Transmit Configuration Settings
// -------------------------------------------------------------------------- //
var servers = {
    qa1: 'https://secure-qa1.ally.com/acs/customers/authenticate',
    qa2: 'https://secure-qa2.ally.com/acs/customers/authenticate',
    qa3: 'https://secure-qa3.ally.com/acs/customers/authenticate',
    cap: 'https://secure-cap.ally.com/acs/customers/authenticate',
    fix: 'https://secure-fix.ally.com/acs/customers/authenticate',
    dev: window.location.origin + "/acs/customers/authenticate",
    prod: 'https://secure.ally.com/acs/customers/authenticate',
    psp: 'https://secure-prodsupt.ally.com/acs/customers/authenticate',
    local: 
    /* istanbul ignore next */
    process.env.TRANSMIT_PREFIX ||
        window.location.origin + "/acs/customers/authenticate",
    emulatorLLE: window.location.origin + "/acs/customers/authenticate",
    emulatorProd: window.location.origin + "/acs/customers/authenticate",
};
var server = which_env_1.default(window.location.href).pick(servers);
// TODO: can we get these added to a shared location?
// NOTE: these are also set in the ember and host repos, and should have the same values
var apigeeApiKey = which_env_1.default(window.location.href).pick({
    dev: 'wU5VdAI4fv7jgsaAOJDREixc3MOaUs1R',
    cap: 'BdWWhZRHCmdNq74CJYfaozM2gZCZqEDs',
    fix: 'BdWWhZRHCmdNq74CJYfaozM2gZCZqEDs',
    qa1: 'BdWWhZRHCmdNq74CJYfaozM2gZCZqEDs',
    qa2: 'BdWWhZRHCmdNq74CJYfaozM2gZCZqEDs',
    qa3: 'BdWWhZRHCmdNq74CJYfaozM2gZCZqEDs',
    prod: 'XhnbsDYmBensf2vqVXAfv0uD7SJBA27u',
    local: 'wU5VdAI4fv7jgsaAOJDREixc3MOaUs1R',
});
var aaosEnvs = {
    local: 'https://www-cit2.ally.com/auto/online-services/access/pre-login-ciam.html',
    dev: 'https://www-cit2.ally.com/auto/online-services/access/pre-login-ciam.html',
    qa1: 'https://www-uat1.ally.com/auto/online-services/access/pre-login-ciam.html',
    qa2: 'https://www-uat2.ally.com/auto/online-services/access/pre-login-ciam.html',
    qa3: 'https://www-uat2.ally.com/auto/online-services/access/pre-login-ciam.html',
    cap: 'https://www-cap.int.ally.com/auto/online-services/access/pre-login-ciam.html',
    psp: 'https://www-prodsupt.ally.com/auto/online-services/access/pre-login-ciam.html',
    fix: 'https://www-fix.int.ally.com/auto/online-services/access/pre-login-ciam.html',
    prod: 'https://www.ally.com/auto/online-services/access/pre-login-ciam.html',
};
exports.aaosUrl = which_env_1.default(window.location.href).pick(aaosEnvs);
exports.DEFAULT_APP = 'ciam_web';
exports.DEFAULT_SERVER = server;
exports.DEFAULT_APP_INFO = {
    id: 'ALLYUSBOLB',
    name: 'AOB',
    version: '1.0',
    apigeeApiKey: apigeeApiKey,
};
// -------------------------------------------------------------------------- //
// Journey Policy IDs
// -------------------------------------------------------------------------- //
exports.DEFAULT_POLICY_NAMES = {
    cenlarSso: 'CENLAR_SSO',
    login: 'AUTHENTICATION_CUSTOMER',
    loginRememberMe: 'REMEMBERME_SECURE',
    logout: 'CUSTOMER_LOGOUT',
    discovery: 'DISCOVERY_SECURE',
    forgotUsername: 'FORGOT_USERNAME',
    forgotPassword: 'FORGOT_PASSWORD',
    recoverPassword: 'RECOVER_PASSWORD',
    investComeback: 'INVEST_COMEBACK',
    internalSessionRehydrate: 'SESSION_REHYDRATE',
    transferTicket: 'TRANSFER_TICKET_REQUEST',
    accessToken: 'ACCESS_TOKEN_REQUEST',
    legacySession: 'GET_LEGACY_SESSION',
    onlineEnrollment: 'ONLINE_ENROLLMENT',
    enrollGuest: 'ENROLL_GUEST',
    addAllyId: 'ADD_ALLY_ID',
    getSecurityProfile: 'SELFSERVICE_GET_SECURITY_PROFILE',
    updateSecurityProfile: 'SELFSERVICE_UPDATE_SECURITY_PROFILE',
    businessRecoverPassword: 'BUSINESS_USER_RECOVER_PWD',
    updateUsername: 'SELFSERVICE_UPDATE_USERNAME',
    getAtomicAccessToken: 'GET_ATOMIC_ACCESS_TOKEN',
    enrollCreatorCache: 'ENROLL_CREATOR_CACHE',
    loginCreatorCache: 'CREATOR_CACHE_AUTHENTICATION',
    recoverPasswordCreatorCache: 'RECOVER_PASSWORD_CREATOR_CACHE',
};
var SESSION_KEY_MAP;
(function (SESSION_KEY_MAP) {
    SESSION_KEY_MAP["sub"] = "userNamePvtEncrypt";
    SESSION_KEY_MAP["ally:guid"] = "guid";
    SESSION_KEY_MAP["ally:allyid"] = "allyid";
    SESSION_KEY_MAP["ally:cif"] = "cif";
    SESSION_KEY_MAP["ally:cupid"] = "cupid";
    SESSION_KEY_MAP["ally:investId"] = "investId";
    SESSION_KEY_MAP["ally:lapiid"] = "lapiid";
    SESSION_KEY_MAP["ally:psi"] = "psi";
    SESSION_KEY_MAP["ally:relationships"] = "relationships";
    SESSION_KEY_MAP["ally:userRelationships"] = "allyUserRole";
    SESSION_KEY_MAP["ally:profileVerification"] = "profileVerification";
    SESSION_KEY_MAP["ally:lastLoginTime"] = "lastLoginTime";
    SESSION_KEY_MAP["ally:resourceIndex"] = "resourceIndex";
    SESSION_KEY_MAP["ally:remembermeToken"] = "rememberMeToken";
    SESSION_KEY_MAP["ally:SSOREQUIRED"] = "SSOREQUIRED";
    SESSION_KEY_MAP["ally:SSOURL"] = "SSOURL";
    SESSION_KEY_MAP["ally:rsaStatusIndicator"] = "rsaStatusIndicator";
    SESSION_KEY_MAP["ally:aaosId"] = "aaosId";
    SESSION_KEY_MAP["ally:tpn"] = "tpn";
    SESSION_KEY_MAP["ally:interrupts"] = "interrupts";
})(SESSION_KEY_MAP = exports.SESSION_KEY_MAP || (exports.SESSION_KEY_MAP = {}));
// Element id comes from Ember implementation of device print
exports.DEVICE_PRINT_ELEMENT_ID = 'user_countils';
var PROFILE_VERIFICATION_STATUS;
(function (PROFILE_VERIFICATION_STATUS) {
    PROFILE_VERIFICATION_STATUS["PROMPT"] = "PROMPT";
    PROFILE_VERIFICATION_STATUS["SKIP"] = "SKIP";
    PROFILE_VERIFICATION_STATUS["VERIFIED"] = "VERIFIED";
    PROFILE_VERIFICATION_STATUS["NOT_VERIFIED_PROMPT"] = "NOT_VERIFIED_PROMPT";
})(PROFILE_VERIFICATION_STATUS = exports.PROFILE_VERIFICATION_STATUS || (exports.PROFILE_VERIFICATION_STATUS = {}));
exports.INTERRUPTS_STATUS_KEY = 'interruptsStatus';
exports.STORED_CIF_KEY = 'storedcif';
exports.VERIFICATION_STATUS_KEY = 'profileVerificationStatus';
exports.AV_ROUTE_FLAG = 'FF_use-new-av-route';
//# sourceMappingURL=constants.js.map
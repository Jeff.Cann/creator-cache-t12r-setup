"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setup = exports.maybeStoreOtpHeaders = void 0;
var which_env_1 = __importDefault(require("@ally/which-env"));
var whisper_1 = __importDefault(require("./whisper"));
var hooks_1 = __importDefault(require("./hooks"));
var rawActions = __importStar(require("./actions"));
var utils_1 = require("./utils");
var AllyTransmitUIHandler_1 = require("./AllyTransmitUIHandler");
var sdk_1 = require("./sdk");
var constants_1 = require("./constants");
var logout = rawActions.logout, rehydrate = rawActions.rehydrate, loginWithUser = rawActions.loginWithUser, forgotUsername = rawActions.forgotUsername, forgotPassword = rawActions.forgotPassword, recoverPassword = rawActions.recoverPassword, transferTicket = rawActions.transferTicket, refreshAccessToken = rawActions.refreshAccessToken, legacySession = rawActions.legacySession, onlineEnrollment = rawActions.onlineEnrollment, cenlarSso = rawActions.cenlarSso, enrollGuest = rawActions.enrollGuest, addAllyId = rawActions.addAllyId, getSecurityProfile = rawActions.getSecurityProfile, updateSecurityProfile = rawActions.updateSecurityProfile, updateUsername = rawActions.updateUsername, getAtomicAccessToken = rawActions.getAtomicAccessToken, enrollCreatorCache = rawActions.enrollCreatorCache, loginCreatorCache = rawActions.loginCreatorCache, recoverPasswordCreatorCache = rawActions.recoverPasswordCreatorCache;
var defaults = {
    myRiskID: sdk_1.myRiskID,
    app: constants_1.DEFAULT_APP,
    sdk: sdk_1.xmsdk.XmSdk(),
    server: constants_1.DEFAULT_SERVER,
    policies: constants_1.DEFAULT_POLICY_NAMES,
    application: constants_1.DEFAULT_APP_INFO,
    logLevel: sdk_1.LogLevel.Critical,
    UIHandler: AllyTransmitUIHandler_1.AllyTransmitUIHandler,
};
/**
 * Transmit's `cancelCurrentRunningControlFlow` will cancel any currently
 * running action. However, you can't start a new session until the original
 * action Promise is rejected. This fixes that quirk. We keep track of the
 * currently running action in `config.activeAction`... each time an action
 * is triggered, we cancel the currently running one and wait for the previous
 * one to reject.
 */
function stupidlyCancelsAndWaitsForExistingAction(f) {
    var _this = this;
    return function (config) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return __awaiter(_this, void 0, void 0, function () {
            var sdk, activeAction, deferred;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sdk = config.sdk, activeAction = config.activeAction;
                        if (!sdk.currentSession) return [3 /*break*/, 2];
                        deferred = activeAction.catch(utils_1.noop);
                        sdk.cancelCurrentRunningControlFlow();
                        return [4 /*yield*/, deferred];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/, (config.activeAction = f.apply(void 0, __spread([config], args)))];
                }
            });
        });
    };
}
/**
 * Wraps actions so that they wait for initialization before invocation.
 * Given some config, returns a function that takes a callback and returns
 * a function that when invoked will wait for the SDK to initialize before
 * invoking the given callback.
 */
function handlesSDKOnReadyActions(config) {
    return function (callback) {
        return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var execute = stupidlyCancelsAndWaitsForExistingAction(callback);
            return config.sdkInitPromise.then(function () { return execute.apply(void 0, __spread([config], args)); });
        };
    };
}
function maybeStoreOtpHeaders(headers) {
    var otpHeaders = headers.reduce(function (acc, val) {
        if (val.type === 'device_id') {
            acc.device_id = val.device_id;
        }
        if (val.type === 'session_id') {
            acc.session_id = val.session_id;
        }
        return acc;
    }, {});
    if (otpHeaders.device_id && otpHeaders.session_id) {
        sessionStorage.setItem('otpHeaders', JSON.stringify(otpHeaders));
    }
}
exports.maybeStoreOtpHeaders = maybeStoreOtpHeaders;
var AllyTransportProvider = /** @class */ (function () {
    function AllyTransportProvider() {
    }
    /* istanbul ignore next */
    AllyTransportProvider.prototype.sendRequest = function (request) {
        var _this = this;
        var _url = request._url, _method = request._method, _headers = request._headers, _bodyJson = request._bodyJson;
        return new Promise(function (resolve, reject) {
            fetch(_url, {
                method: _method,
                headers: _headers.reduce(function (a, v) {
                    var _a;
                    return (__assign(__assign({}, a), (_a = {}, _a[v._name] = v._value, _a)));
                }, {
                    'content-type': 'application/json',
                }),
                body: _bodyJson,
            })
                .then(function (response) { return __awaiter(_this, void 0, void 0, function () {
                var json;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, response.json()];
                        case 1:
                            json = _a.sent();
                            return [2 /*return*/, __assign(__assign({}, json), { status: response.status })];
                    }
                });
            }); })
                .then(function (data) {
                var tResponse = new sdk_1.TransportResponse();
                tResponse.setStatus(data.status);
                tResponse.setHeaders(data.headers);
                delete data.status; // remove status before stringifying
                tResponse.setBodyJson(JSON.stringify(data));
                maybeStoreOtpHeaders(data.headers);
                resolve(tResponse);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    return AllyTransportProvider;
}());
/**
 * Derived values to be "injected" into the given factory options.
 * This is used to store off the `sdkReadyPromise` and to setup the SDK for
 * public consumption.
 */
function setup(options) {
    var app = options.app, sdk = options.sdk, server = options.server, logLevel = options.logLevel, UIHandler = options.UIHandler;
    sdk.setUiHandler(new UIHandler(options, utils_1.noop, utils_1.noop, utils_1.noop, utils_1.noop));
    sdk.setConnectionSettings(sdk_1.SDKConnectionSettings.create(server, app));
    sdk.setLogLevel(logLevel);
    sdk.setUiAssetsDownloadMode(sdk_1.UIAssetsDownloadMode.Disable);
    var env = which_env_1.default(window.location.href);
    if (!env.isProd) {
        sdk.setTransportProvider(new AllyTransportProvider());
    }
    return {
        sdk: sdk,
        activeAction: Promise.resolve(),
        sdkInitPromise: sdk.initialize(),
    };
}
exports.setup = setup;
/**
 * Transmit factory function.
 * Returns an object with all of the transmit actions and (optional react hooks)
 */
function factory(options) {
    if (options === void 0) { options = {}; }
    var policies = __assign(__assign({}, defaults.policies), options.policies);
    var defaulted = __assign(__assign(__assign({}, defaults), options), { policies: policies });
    whisper_1.default.debug({
        message: [
            'Initialized with options',
            {
                initial: __assign(__assign({}, options), { sdk: true }),
                resolved: __assign(__assign({}, defaulted), { sdk: true }),
            },
        ],
    });
    var deferred = handlesSDKOnReadyActions(__assign(__assign({}, defaulted), setup(defaulted)));
    var actions = {
        logout: deferred(logout),
        rehydrate: deferred(rehydrate),
        loginWithUser: deferred(loginWithUser),
        forgotUsername: deferred(forgotUsername),
        forgotPassword: deferred(forgotPassword),
        recoverPassword: deferred(recoverPassword),
        transferTicket: deferred(transferTicket),
        refreshAccessToken: deferred(refreshAccessToken),
        legacySession: deferred(legacySession),
        onlineEnrollment: deferred(onlineEnrollment),
        cenlarSso: deferred(cenlarSso),
        enrollGuest: deferred(enrollGuest),
        addAllyId: deferred(addAllyId),
        getSecurityProfile: deferred(getSecurityProfile),
        updateSecurityProfile: deferred(updateSecurityProfile),
        updateUsername: deferred(updateUsername),
        getAtomicAccessToken: deferred(getAtomicAccessToken),
        enrollCreatorCache: deferred(enrollCreatorCache),
        loginCreatorCache: deferred(loginCreatorCache),
        recoverPasswordCreatorCache: deferred(recoverPasswordCreatorCache),
    };
    return __assign(__assign({}, hooks_1.default(actions)), actions);
}
exports.default = factory;
//# sourceMappingURL=factory.js.map
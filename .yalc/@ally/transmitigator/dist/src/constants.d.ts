export declare const aaosUrl: string;
export declare const DEFAULT_APP = "ciam_web";
export declare const DEFAULT_SERVER: string;
export declare const DEFAULT_APP_INFO: {
    id: string;
    name: string;
    version: string;
    apigeeApiKey: string;
};
export declare const DEFAULT_POLICY_NAMES: {
    cenlarSso: string;
    login: string;
    loginRememberMe: string;
    logout: string;
    discovery: string;
    forgotUsername: string;
    forgotPassword: string;
    recoverPassword: string;
    investComeback: string;
    internalSessionRehydrate: string;
    transferTicket: string;
    accessToken: string;
    legacySession: string;
    onlineEnrollment: string;
    enrollGuest: string;
    addAllyId: string;
    getSecurityProfile: string;
    updateSecurityProfile: string;
    businessRecoverPassword: string;
    updateUsername: string;
    getAtomicAccessToken: string;
    enrollCreatorCache: string;
    loginCreatorCache: string;
    recoverPasswordCreatorCache: string;
};
export declare enum SESSION_KEY_MAP {
    'sub' = "userNamePvtEncrypt",
    'ally:guid' = "guid",
    'ally:allyid' = "allyid",
    'ally:cif' = "cif",
    'ally:cupid' = "cupid",
    'ally:investId' = "investId",
    'ally:lapiid' = "lapiid",
    'ally:psi' = "psi",
    'ally:relationships' = "relationships",
    'ally:userRelationships' = "allyUserRole",
    'ally:profileVerification' = "profileVerification",
    'ally:lastLoginTime' = "lastLoginTime",
    'ally:resourceIndex' = "resourceIndex",
    'ally:remembermeToken' = "rememberMeToken",
    'ally:SSOREQUIRED' = "SSOREQUIRED",
    'ally:SSOURL' = "SSOURL",
    'ally:rsaStatusIndicator' = "rsaStatusIndicator",
    'ally:aaosId' = "aaosId",
    'ally:tpn' = "tpn",
    'ally:interrupts' = "interrupts"
}
export declare const DEVICE_PRINT_ELEMENT_ID = "user_countils";
export declare enum PROFILE_VERIFICATION_STATUS {
    PROMPT = "PROMPT",
    SKIP = "SKIP",
    VERIFIED = "VERIFIED",
    NOT_VERIFIED_PROMPT = "NOT_VERIFIED_PROMPT"
}
export declare const INTERRUPTS_STATUS_KEY = "interruptsStatus";
export declare const STORED_CIF_KEY = "storedcif";
export declare const VERIFICATION_STATUS_KEY = "profileVerificationStatus";
export declare const AV_ROUTE_FLAG = "FF_use-new-av-route";

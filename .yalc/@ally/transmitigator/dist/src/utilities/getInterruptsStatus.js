"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getInterruptsStatus = void 0;
var constants_1 = require("../constants");
var getInterruptsStatus = function () {
    var interruptsStatus = sessionStorage.getItem(constants_1.INTERRUPTS_STATUS_KEY);
    return interruptsStatus ? JSON.parse(interruptsStatus) : {};
};
exports.getInterruptsStatus = getInterruptsStatus;
//# sourceMappingURL=getInterruptsStatus.js.map
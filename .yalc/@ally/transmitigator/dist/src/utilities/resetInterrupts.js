"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resetInterrupts = void 0;
var constants_1 = require("../constants");
var resetInterrupts = function () {
    window.sessionStorage.removeItem(constants_1.INTERRUPTS_STATUS_KEY);
};
exports.resetInterrupts = resetInterrupts;
//# sourceMappingURL=resetInterrupts.js.map
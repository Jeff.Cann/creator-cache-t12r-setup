"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFlagParams = void 0;
var whisper_1 = __importDefault(require("../whisper"));
/**
 * Whitelisted feature flags to be sent on every request
 */
var featureFlagWhitelist = ['FF_auto-capability'];
/**
 * Takes in optional `featureFlags` and `featureFlagsOverride` and returns an object
 * containing a filtered set of featureFlags based on the featureFlagWhitelist, plus any
 * provided flags in `featureFlagsOverride`. Overrides are not subject to filtering and,
 * as the variable name suggests, will override any value resolved from LaunchDarkly.
 *
 * The `featureFlags` prop should not be set by the individual action caller, instead they're
 * set using the `setFeatureFlags()` function provided by the `useTransmit()`` hook.
 */
var getFlagParams = function (options) {
    var _a = options.featureFlags, featureFlags = _a === void 0 ? {} : _a, _b = options.featureFlagsOverride, featureFlagsOverride = _b === void 0 ? {} : _b;
    if (!featureFlags && !featureFlagsOverride) {
        whisper_1.default.debug({
            message: 'getFlagParams(): No featureFlags or featureFlagsOverride provided',
        });
        return { launchdarkly_flags: {} };
    }
    // Filter all feature flags by the whitelist
    var filteredFlags = Object.keys(featureFlags)
        .filter(function (key) { return featureFlagWhitelist.includes(key); })
        .reduce(function (acc, key) {
        var _a;
        return __assign(__assign({}, acc), (_a = {}, _a[key] = featureFlags[key], _a));
    }, {});
    var flagParams = {
        launchdarkly_flags: __assign(__assign({}, filteredFlags), featureFlagsOverride),
    };
    whisper_1.default.debug({ message: ['getFlagParams() returning:', flagParams] });
    return flagParams;
};
exports.getFlagParams = getFlagParams;
//# sourceMappingURL=getFlagParams.js.map
import { ActionFeatureFlagOptions, LDFlagSet } from '../types';
interface FlagParams {
    launchdarkly_flags: LDFlagSet;
}
/**
 * Takes in optional `featureFlags` and `featureFlagsOverride` and returns an object
 * containing a filtered set of featureFlags based on the featureFlagWhitelist, plus any
 * provided flags in `featureFlagsOverride`. Overrides are not subject to filtering and,
 * as the variable name suggests, will override any value resolved from LaunchDarkly.
 *
 * The `featureFlags` prop should not be set by the individual action caller, instead they're
 * set using the `setFeatureFlags()` function provided by the `useTransmit()`` hook.
 */
export declare const getFlagParams: (options: ActionFeatureFlagOptions) => FlagParams;
export {};

import { Session } from '..';
interface Interrupt {
    interruptID: number;
    interruptType: string;
    lob: string;
    priority: string;
}
declare type OptionalExceptFor<T, TRequired extends keyof T> = Partial<T> & Pick<T, TRequired>;
interface PartialSession extends OptionalExceptFor<Session, 'allyUserRole' | 'profileVerification'> {
    interrupts?: Interrupt[];
}
export declare const checkAndHandleInterrupts: (session: PartialSession | null, navigate: (destination: string) => void, variation: (key: string, defaultValue?: any) => any, redirectPathOverride?: string | undefined) => void;
export {};

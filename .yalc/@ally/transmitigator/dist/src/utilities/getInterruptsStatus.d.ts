export interface InterruptsStatus {
    interruptsComplete?: boolean;
    tpn?: string;
    bankruptcyAccepted?: boolean;
}
export declare const getInterruptsStatus: () => InterruptsStatus;

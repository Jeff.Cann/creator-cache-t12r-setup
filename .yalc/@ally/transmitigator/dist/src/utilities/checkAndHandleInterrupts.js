"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkAndHandleInterrupts = void 0;
var whisper_1 = __importDefault(require("../whisper"));
var constants_1 = require("../constants");
var _1 = require(".");
var NOT_VERIFIED_PROMPT = constants_1.PROFILE_VERIFICATION_STATUS.NOT_VERIFIED_PROMPT, PROMPT = constants_1.PROFILE_VERIFICATION_STATUS.PROMPT, SKIP = constants_1.PROFILE_VERIFICATION_STATUS.SKIP, VERIFIED = constants_1.PROFILE_VERIFICATION_STATUS.VERIFIED;
function getRedirect(override) {
    if (override) {
        return encodeURIComponent(override);
    }
    var _a = window.location, pathname = _a.pathname, search = _a.search;
    return encodeURIComponent("" + pathname + search);
}
function checkIsProfileVerificationRequired(session) {
    var _a, _b;
    // Check if user has a session
    if (!session)
        return false;
    // Check if user is a bank Customer
    if (!((_a = session.allyUserRole) === null || _a === void 0 ? void 0 : _a.bank))
        return false;
    // Check if user has profile verification information (if not assume they do not need to be prompted)
    if (!((_b = session.profileVerification) === null || _b === void 0 ? void 0 : _b.annualVerificationStatus))
        return false;
    // Check if user is verified
    if (session.profileVerification.annualVerificationStatus !== NOT_VERIFIED_PROMPT)
        return false;
    // Check if same customer (for session storage values)
    var storedCif = window.sessionStorage.getItem(constants_1.STORED_CIF_KEY);
    var isSameCustomer = storedCif === session.cif;
    if (!isSameCustomer) {
        // Clear session storage values when it is not the same customer
        window.sessionStorage.removeItem(constants_1.STORED_CIF_KEY);
        window.sessionStorage.removeItem(constants_1.VERIFICATION_STATUS_KEY);
    }
    // Check session storage for 'SKIP' or 'VERIFIED' status
    var storedStatus = window.sessionStorage.getItem(constants_1.VERIFICATION_STATUS_KEY);
    return storedStatus !== SKIP && storedStatus !== VERIFIED;
}
var checkAndHandleInterrupts = function (session, navigate, variation, redirectPathOverride) {
    if (!session) {
        whisper_1.default.debug({
            message: 'checkAndHandleInterrupts(): no session data provided',
        });
        return;
    }
    var interrupts = session.interrupts;
    var hasInterrupts = interrupts && interrupts.length;
    var redirectionPath = null;
    var interruptsComplete = _1.getInterruptsStatus().interruptsComplete;
    var isAlreadyOnInterruptsRoute = /^\/interrupts/.test(window.location.pathname);
    var isAlreadyOnAVRoute = /^\/annual-profile-verification/.test(window.location.pathname);
    // If already on interrupts or AV routes don't navigate, this shouldn't occur
    // but this logic existed in one AV detection instance being replaced with this
    // logic, so we're keeping it
    if (isAlreadyOnInterruptsRoute || isAlreadyOnAVRoute) {
        return;
    }
    // check interrupts
    if (hasInterrupts && !interruptsComplete) {
        redirectionPath = "/interrupts?redirect=" + getRedirect(redirectPathOverride);
        whisper_1.default.debug({
            message: "checkAndHandleInterrupts(): interrupts found, redirecting to " + redirectionPath,
        });
    }
    // if no interrupts, check AV, otherwise let interrupts remote handle AV
    if (!hasInterrupts || interruptsComplete) {
        if (checkIsProfileVerificationRequired(session)) {
            var baseRoute = variation(constants_1.AV_ROUTE_FLAG, false)
                ? 'interrupts'
                : 'annual-profile-verification';
            redirectionPath = "/" + baseRoute + "?redirect=" + getRedirect(redirectPathOverride);
            window.sessionStorage.setItem(constants_1.VERIFICATION_STATUS_KEY, PROMPT);
            whisper_1.default.debug({
                message: "checkAndHandleInterrupts(): AV required, redirecting to " + redirectionPath,
            });
        }
    }
    if (redirectionPath) {
        navigate(redirectionPath);
    }
};
exports.checkAndHandleInterrupts = checkAndHandleInterrupts;
//# sourceMappingURL=checkAndHandleInterrupts.js.map
import { ErrorDispatcher, JourneyDispatcher } from './AllyTransmitUIHandler';
import { DeviceData } from './headers';
import { AnyFunction, Application, ApplicationParams, CenlarSSOAuthenticated, SecurityProfile, Config, LegacySession, Session, TransmitResponse } from './types';
export interface UIConsumer {
    onScreenUpdate?: JourneyDispatcher;
    onScreenLoading?: () => void;
    onScreenError?: ErrorDispatcher;
    onScreenRedirect?: () => void;
    deviceData?: DeviceData;
}
export declare type AuthTokenData = Omit<Session, 'deviceData'>;
export interface DecodedTransmitTokenV1 {
    'CIAM-App-Token': AuthTokenData;
}
export declare type DecodedTransmitTokenV2 = AuthTokenData;
export declare function noop(): void;
export declare function getIsSameTab(): boolean;
export declare function unregisterTab(): void;
export declare const isValidRefreshTicket: (ticketID: unknown) => ticketID is string;
/**
 * We need to delete the ticketID from the URL to prevent potential re-use.
 */
export declare const deleteRefreshTicketID: () => void;
export declare const getTransmitTicket: () => string | null;
export declare const isValidSSOTicket: (ticketID: string | null) => ticketID is string;
export declare const getSSOTicketID: () => string | void;
export declare function getAppInfoParams({ id, name, version, apigeeApiKey, }: Application): ApplicationParams;
/**
 * Generates a unique random string for the current tab and assigns it to
 * window.name. This is used to prevent sessions across tabs. If the session
 * is created in a window with name=x, and a new tab is opened, the names won't
 * match and rehydration will be skipped.
 */
export declare function registerTab(): void;
/**
 * Fetches a cookie by name.
 */
export declare function getCookie(name: string): string | void;
/**
 * Gets a year from now in GMT time.
 * Useful for setting a cookie's expiration from one year from now.
 */
export declare function getAYearFromNowGMT(): string;
/**
 * Used to invalid cookies.
 * Returns exactly `Thu, 01 Jan 1970 00:00:00 GMT`
 */
export declare function getUnixEpochGMT(): string;
/**
 * If the username has been saved, this retrieves it from the value of the
 * jeffersonCookieSunset in the format: [<username slice>, <token>]
 *
 * If cookie is invest fast track portion only, return empty array
 */
export declare function getSavedUsernameSliceAndToken(): string[];
export declare function getInvestFastTrackUserIfPresent(): string | null;
/**
 * Sets the saved username.
 *
 * Updated requirements:
 *
 * 1. User without save username, not invest
 * **No difference from today, no cookie should be set**
 * 2. User with save username, not invest
 * **pant...:5647389587473829**
 * 3. User with save username, invest
 * **pant...:65748398r7t438473:pant:invfast**
 * 4. User without save username, invest
 * **::pant:invfast**
 */
export declare function setSavedUsername({ userNamePvtEncrypt: username, rememberMeToken: token, allyUserRole: { investment: isInvestUser }, }: Session): void;
/**
 * Deletes the saved username
 */
export declare function delSavedUsername(): void;
/**
 * Converts a "Transmit Error" object to a plain old JavaScript Error.
 * This will return an error object with the following properties:
 * - `code`: The Transmit error code, this is a number.
 * - `data`: An object containing error data about the error that occurred.
 */
export declare function toConsumableError(e: TransmitSDK.AuthenticationError): Error;
export interface SessionPayload {
    type: 'session';
    session: Session;
}
export interface TicketPayload {
    type: 'ticket';
    ticketID: string;
}
export interface LegacySessionPayload {
    type: 'legacySession';
    legacySession: LegacySession;
}
export interface CenlarSSOPayload {
    type: 'cenlarSso';
    cenlarSso: CenlarSSOAuthenticated;
}
export interface ChatTokenPayload {
    type: 'chatTokenRefresh';
    legacyChatToken: string;
}
export interface GetSecurityProfilePayload {
    type: 'getSecurityProfile';
    securityProfile: SecurityProfile;
}
export interface GetAtomicAccessTokenPayload {
    type: 'getAtomicAccessToken';
    atomic_access_token: string;
}
export declare function decodeTokenV2(idToken: string): AuthTokenData;
export declare function handleResponseV2(authenticationResult: TransmitSDK.AuthenticationResult): AuthTokenData | null;
export declare type TransmitAuthPayload = SessionPayload | TicketPayload | LegacySessionPayload | ChatTokenPayload | CenlarSSOPayload | GetSecurityProfilePayload | GetAtomicAccessTokenPayload;
export declare function handleTicketResponse(ticketID: string): TransmitAuthPayload;
export declare function handleLegacySession(data: any): TransmitAuthPayload;
export declare function handleChatToken(chatToken: string | null): TransmitAuthPayload;
export declare function handleCenlarSso(data: any): TransmitAuthPayload;
export declare function handleSecurityProfile(data: any): TransmitAuthPayload;
export declare function handleAtomicAccessToken(data: any): TransmitAuthPayload;
/**
 * Creates a session object, or returns a ticketID.
 * This is used either to create a valid session, a ticketID, or an "anonymous" session.
 */
export declare function createSession(transmitResponse?: TransmitResponse | null): TransmitAuthPayload | null;
/**
 * HOF that wraps an action and converts its return value to an optional Session
 * object.
 */
export declare function asSession<F extends (...args: any[]) => Promise<TransmitResponse | null>>(f: F): (...args: Parameters<F>) => Promise<TransmitAuthPayload | null>;
/**
 * HOF that wraps an action and sets up the UIHandler prior to invoking the
 * function.
 */
export declare function asUIConsumer<T extends Record<string, any>, F extends AnyFunction = AnyFunction>(f: F): (config: Config, options?: T & UIConsumer) => ReturnType<F>;
export declare function deleteTransmitSession({ sdk }: Config): Promise<void>;

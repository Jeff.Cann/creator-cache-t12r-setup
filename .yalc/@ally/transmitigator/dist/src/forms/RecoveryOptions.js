"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecoveryOptionsFormSession = void 0;
var sdk_1 = require("../sdk");
var Base_1 = require("./Base");
var RecoveryOptionsFormSession = /** @class */ (function (_super) {
    __extends(RecoveryOptionsFormSession, _super);
    function RecoveryOptionsFormSession(onScreenLoading, onScreenUpdate, _a) {
        var _b = _a.isCustomer, isCustomer = _b === void 0 ? false : _b, _c = _a.isResetPassword, isResetPassword = _c === void 0 ? false : _c, _d = _a.errorCode, errorCode = _d === void 0 ? null : _d;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.isCustomer = isCustomer;
        _this.isResetPassword = isResetPassword;
        _this.errorCode = errorCode;
        return _this;
    }
    RecoveryOptionsFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'RecoveryOptions',
                payload: {
                    onComplete: function (input) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(input));
                    },
                    status: 'None',
                    errorCode: _this.errorCode,
                    isCustomer: _this.isCustomer,
                    isResetPassword: _this.isResetPassword,
                },
            });
        });
    };
    return RecoveryOptionsFormSession;
}(Base_1.BaseUIFormSession));
exports.RecoveryOptionsFormSession = RecoveryOptionsFormSession;
//# sourceMappingURL=RecoveryOptions.js.map
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnrollAdditionalVerificationAutoInputFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var EnrollAdditionalVerificationAutoInputFormSession = /** @class */ (function (_super) {
    __extends(EnrollAdditionalVerificationAutoInputFormSession, _super);
    function EnrollAdditionalVerificationAutoInputFormSession(onScreenLoading, onScreenUpdate, _a) {
        var errorCode = _a.errorCode, type = _a.type;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.error = errorCode;
        _this.type = type;
        return _this;
    }
    EnrollAdditionalVerificationAutoInputFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'EnrollAutoInput',
                payload: {
                    onComplete: function (input) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(input));
                    },
                    navigateToAutoTypeSelect: function () {
                        resolve(sdk_1.FormInput.createEscapeRequest({
                            getId: function () { return 'repick'; },
                            getPresentation: function () {
                                return TransmitSDK.ActionEscapeOptionPresentation.Action;
                            },
                            getDisplayName: function () { return 'repick'; },
                        }, null));
                    },
                    status: 'None',
                    type: _this.type,
                    error: _this.error,
                },
            });
        });
    };
    return EnrollAdditionalVerificationAutoInputFormSession;
}(Base_1.BaseUIFormSession));
exports.EnrollAdditionalVerificationAutoInputFormSession = EnrollAdditionalVerificationAutoInputFormSession;
//# sourceMappingURL=VerifyAutoInput.js.map
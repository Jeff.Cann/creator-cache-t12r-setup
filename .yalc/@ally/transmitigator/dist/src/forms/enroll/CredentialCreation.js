"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnrollCredentialCreationFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var EnrollCredentialCreationFormSession = /** @class */ (function (_super) {
    __extends(EnrollCredentialCreationFormSession, _super);
    function EnrollCredentialCreationFormSession(onScreenLoading, onScreenUpdate, _a) {
        var _b = _a.phone, phone = _b === void 0 ? '' : _b, _c = _a.email, email = _c === void 0 ? '' : _c, allyUserRoles = _a.allyUserRoles;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.phone = phone;
        _this.email = email;
        _this.allyUserRoles = allyUserRoles;
        return _this;
    }
    EnrollCredentialCreationFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'EnrollCredentialCreation',
                payload: {
                    onComplete: function (input) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(input));
                    },
                    status: 'None',
                    error: _this.error,
                    suggestedUsername1: _this.suggestedUsername1,
                    suggestedUsername2: _this.suggestedUsername2,
                    phone: _this.phone,
                    email: _this.email,
                    allyUserRoles: _this.allyUserRoles,
                },
            });
        });
    };
    EnrollCredentialCreationFormSession.prototype.onError = function (payload) {
        if (payload) {
            var errorCode = payload.errorCode, suggestedUsername1 = payload.suggestedUsername1, suggestedUsername2 = payload.suggestedUsername2;
            this.error = errorCode;
            this.suggestedUsername1 = suggestedUsername1;
            this.suggestedUsername2 = suggestedUsername2;
        }
    };
    return EnrollCredentialCreationFormSession;
}(Base_1.BaseUIFormSession));
exports.EnrollCredentialCreationFormSession = EnrollCredentialCreationFormSession;
//# sourceMappingURL=CredentialCreation.js.map
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnrollAdditionalVerificationAutoTypeSelectFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var EnrollAdditionalVerificationAutoTypeSelectFormSession = /** @class */ (function (_super) {
    __extends(EnrollAdditionalVerificationAutoTypeSelectFormSession, _super);
    function EnrollAdditionalVerificationAutoTypeSelectFormSession() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EnrollAdditionalVerificationAutoTypeSelectFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'EnrollAutoTypeSelect',
                payload: {
                    onComplete: function (input) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(input));
                    },
                    status: 'None',
                },
            });
        });
    };
    return EnrollAdditionalVerificationAutoTypeSelectFormSession;
}(Base_1.BaseUIFormSession));
exports.EnrollAdditionalVerificationAutoTypeSelectFormSession = EnrollAdditionalVerificationAutoTypeSelectFormSession;
//# sourceMappingURL=VerifyAutoTypeSelect.js.map
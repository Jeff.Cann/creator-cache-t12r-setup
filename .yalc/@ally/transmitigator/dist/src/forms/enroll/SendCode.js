"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnrollSendCodeFormSession = exports.getOtpType = exports.serializePayloadToTargets = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var authenticators_1 = require("../../authenticators");
function serializePayloadToTargets(_a) {
    var emails = _a.emails, phones = _a.phones;
    var targets = [];
    var idx = 0;
    emails.forEach(function (email, index) {
        targets.push({
            _channel: sdk_1.OtpChannel.Email,
            _channelIndex: index,
            _description: email,
            _targetIdentifier: "" + idx,
        });
        idx += 1;
    });
    phones.forEach(function (phone, index) {
        targets.push({
            _channel: sdk_1.OtpChannel.Sms,
            _channelIndex: index,
            _description: phone,
            _targetIdentifier: "" + idx,
        });
        idx += 1;
        targets.push({
            _channel: sdk_1.OtpChannel.VoiceCall,
            _channelIndex: index,
            _description: phone,
            _targetIdentifier: "" + idx,
        });
        idx += 1;
    });
    return targets;
}
exports.serializePayloadToTargets = serializePayloadToTargets;
function getOtpType(target) {
    switch (target._channel) {
        case sdk_1.OtpChannel.Email:
            return 'email';
        case sdk_1.OtpChannel.Sms:
            return 'sms';
        case sdk_1.OtpChannel.VoiceCall:
            return 'voice';
        default:
            return '';
    }
}
exports.getOtpType = getOtpType;
var EnrollSendCodeFormSession = /** @class */ (function (_super) {
    __extends(EnrollSendCodeFormSession, _super);
    function EnrollSendCodeFormSession(onScreenLoading, onScreenUpdate, payload) {
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.targets = serializePayloadToTargets(payload);
        return _this;
    }
    EnrollSendCodeFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'SendCode',
                payload: {
                    status: _this.error ? 'Error' : 'None',
                    targets: _this.targets,
                    getOptionType: authenticators_1.getOptionType,
                    suppressionStatus: 'None',
                    onComplete: function (target) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest({
                            otpSelection: target._description,
                            method: getOtpType(target),
                        }));
                    },
                },
            });
        });
    };
    return EnrollSendCodeFormSession;
}(Base_1.BaseUIFormSession));
exports.EnrollSendCodeFormSession = EnrollSendCodeFormSession;
//# sourceMappingURL=SendCode.js.map
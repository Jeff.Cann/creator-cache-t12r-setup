import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface EnrollConfirmationScreen {
    type: 'EnrollConfirmation';
    payload: EnrollConfirmationProps;
}
export interface EnrollConfirmationPayload {
    userSelection: string;
}
export interface EnrollConfirmationProps {
    onComplete: (payload: EnrollConfirmationPayload) => void;
    status: AsyncStatus;
    error?: string | Error;
    fullName: string;
    email: string;
    phone: string;
    username: string;
    userSelection: string;
    shouldEndSession: boolean;
}
export interface EnrollConfirmationInput {
    fullName: string;
    email: string;
    phone: string;
    username: string;
    userSelection: string;
}
export declare class EnrollConfirmationFormSession extends BaseUIFormSession {
    protected fullName: string;
    protected email: string;
    protected phone: string;
    protected username: string;
    protected userSelection: string;
    protected shouldEndSession: boolean;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { fullName, email, phone, username, userSelection, }: EnrollConfirmationInput);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
    endSession(): void;
}

import { JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
import { ReadableDeliveryMethod } from '../../authenticators';
interface TargetInfo {
    target: string;
    channel: ReadableDeliveryMethod;
}
export interface EnrollVerifyCodePayload {
    otpInputCount: number;
    targetInfo: TargetInfo;
}
export declare class EnrollVerifyCodeFormSession extends BaseUIFormSession {
    private otpInputCount;
    private maskedDeliveryOption;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, payload: EnrollVerifyCodePayload);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}
export {};

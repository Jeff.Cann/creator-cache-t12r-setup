"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnrollConfirmationFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var EnrollConfirmationFormSession = /** @class */ (function (_super) {
    __extends(EnrollConfirmationFormSession, _super);
    function EnrollConfirmationFormSession(onScreenLoading, onScreenUpdate, _a) {
        var fullName = _a.fullName, email = _a.email, phone = _a.phone, username = _a.username, userSelection = _a.userSelection;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.fullName = fullName;
        _this.email = email;
        _this.phone = phone;
        _this.username = username;
        _this.userSelection = userSelection;
        _this.shouldEndSession = false;
        return _this;
    }
    EnrollConfirmationFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'EnrollConfirmation',
                payload: {
                    onComplete: function (input) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(input));
                    },
                    status: 'None',
                    fullName: _this.fullName,
                    email: _this.email,
                    phone: _this.phone,
                    username: _this.username,
                    userSelection: _this.userSelection,
                    shouldEndSession: _this.shouldEndSession,
                },
            });
        });
    };
    EnrollConfirmationFormSession.prototype.endSession = function () {
        this.shouldEndSession = true;
        this.onScreenUpdate({
            type: 'EnrollConfirmation',
            payload: {
                onComplete: function () { return undefined; },
                status: 'None',
                fullName: this.fullName,
                email: this.email,
                phone: this.phone,
                username: this.username,
                userSelection: this.userSelection,
                shouldEndSession: this.shouldEndSession,
            },
        });
    };
    return EnrollConfirmationFormSession;
}(Base_1.BaseUIFormSession));
exports.EnrollConfirmationFormSession = EnrollConfirmationFormSession;
//# sourceMappingURL=Confirmation.js.map
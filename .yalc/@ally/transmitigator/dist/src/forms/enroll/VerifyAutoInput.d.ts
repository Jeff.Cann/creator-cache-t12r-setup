import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export declare type AutoVerificationType = 'accountNumber' | 'vin';
export interface EnrollAdditionalAutoInputScreen {
    type: 'EnrollAutoInput';
    payload: EnrollAdditionalAutoProps;
}
export interface EnrollAdditionalAutoProps {
    onComplete: (payload: EnrollAdditionalAutoInputPayload) => void;
    navigateToAutoTypeSelect: () => void;
    status: AsyncStatus;
    type: AutoVerificationType;
    error?: string | Error;
}
export interface EnrollAdditionalAutoInputPayload {
    accountNumber?: string;
    vin?: string;
}
export interface EnrollAdditionalAutoInputInput {
    errorCode?: string;
    type: AutoVerificationType;
}
export declare class EnrollAdditionalVerificationAutoInputFormSession extends BaseUIFormSession {
    protected type: AutoVerificationType;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { errorCode, type }: EnrollAdditionalAutoInputInput);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

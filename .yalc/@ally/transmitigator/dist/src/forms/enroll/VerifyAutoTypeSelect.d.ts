import { AsyncStatus } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface EnrollAdditionalAutoTypeSelectScreen {
    type: 'EnrollAutoTypeSelect';
    payload: EnrollAdditionalAutoTypeSelectProps;
}
export interface EnrollAdditionalAutoTypeSelectProps {
    onComplete: (payload: EnrollAdditionalAutoTypeSelectPayload) => void;
    status: AsyncStatus;
}
export interface EnrollAdditionalAutoTypeSelectPayload {
    accountNumber: boolean;
    vin: boolean;
}
export declare class EnrollAdditionalVerificationAutoTypeSelectFormSession extends BaseUIFormSession {
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

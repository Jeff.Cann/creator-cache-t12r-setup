import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession, TransmitFormErrorPayload } from '../Base';
import { AllyUserRole } from '../../types';
export interface EnrollCredentialCreationScreen {
    type: 'EnrollCredentialCreation';
    payload: EnrollCredentialCreationProps;
}
export interface EnrollCredentialCreationPayload {
    username: string;
    password: string;
    passwordHint: string;
    secretQuestion: string;
    secretAnswer: string;
    phone?: string;
    email?: string;
    smsConsent: boolean;
    voiceConsent: boolean;
}
export interface EnrollCredentialCreationInput {
    phone?: string;
    email?: string;
    allyUserRoles: AllyUserRole;
}
export interface EnrollCredentialCreationProps extends EnrollCredentialCreationInput {
    onComplete: (payload: EnrollCredentialCreationPayload) => void;
    status: AsyncStatus;
    error?: string | Error;
    suggestedUsername1?: string;
    suggestedUsername2?: string;
}
interface CredentialCreationTransmitFormErrorPayload extends TransmitFormErrorPayload {
    suggestedUsername1?: string;
    suggestedUsername2?: string;
}
export declare class EnrollCredentialCreationFormSession extends BaseUIFormSession {
    protected suggestedUsername1?: string;
    protected suggestedUsername2?: string;
    protected phone?: string;
    protected email?: string;
    protected allyUserRoles: AllyUserRole;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { phone, email, allyUserRoles }: EnrollCredentialCreationInput);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
    onError(payload: CredentialCreationTransmitFormErrorPayload | null): void;
}
export {};

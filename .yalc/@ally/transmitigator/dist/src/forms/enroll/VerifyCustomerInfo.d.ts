import { AsyncStatus } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface EnrollVerifyCustomerInfoScreen {
    type: 'VerifyCustomerInfo';
    payload: EnrollVerifyCustomerInfoProps;
}
interface EnrollVerifyCustomerBasePayload {
    isAutoBusinessUser: boolean;
}
export interface EnrollVerifyCustomerInfoPayload extends EnrollVerifyCustomerBasePayload {
    lastName?: string;
    dateOfBirth: string;
    taxID: string;
}
export interface EnrollVerifyCustomerAutoPayload extends EnrollVerifyCustomerBasePayload {
    tin: string;
    accountNumber: string;
    vin: string;
}
export declare type EnrollVerifyCustomerPayloadUnion = EnrollVerifyCustomerInfoPayload | EnrollVerifyCustomerAutoPayload;
export interface EnrollVerifyCustomerInfoProps {
    onComplete: (payload: EnrollVerifyCustomerPayloadUnion) => void;
    status: AsyncStatus;
    error?: string | Error;
}
export declare class EnrollVerifyCustomerFormSession extends BaseUIFormSession {
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}
export {};

import { JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
import { Target } from '../../authenticators';
export interface EnrollSendCodePayload {
    emails: string[];
    phones: string[];
}
export declare function serializePayloadToTargets({ emails, phones, }: EnrollSendCodePayload): Partial<Target>[];
export declare function getOtpType(target: Partial<Target>): string;
export declare class EnrollSendCodeFormSession extends BaseUIFormSession {
    private targets?;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, payload: EnrollSendCodePayload);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

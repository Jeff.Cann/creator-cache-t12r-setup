"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnrollVerifyCodeFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var EnrollVerifyCodeFormSession = /** @class */ (function (_super) {
    __extends(EnrollVerifyCodeFormSession, _super);
    function EnrollVerifyCodeFormSession(onScreenLoading, onScreenUpdate, payload) {
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.otpInputCount = payload.otpInputCount;
        _this.maskedDeliveryOption = {
            type: payload.targetInfo.channel,
            value: payload.targetInfo.target,
        };
        return _this;
    }
    EnrollVerifyCodeFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'VerifyCode',
                payload: {
                    otpInputCount: _this.otpInputCount,
                    status: _this.error ? 'Error' : 'None',
                    targetsLength: 1,
                    maskedDeliveryOption: _this.maskedDeliveryOption,
                    // TODO: would adding loading here work?
                    handleResend: function () {
                        _this.error = undefined;
                        resolve(sdk_1.FormInput.createEscapeRequest({
                            getId: function () { return 'resend'; },
                            getPresentation: function () {
                                return TransmitSDK.ActionEscapeOptionPresentation.Action;
                            },
                            getDisplayName: function () { return 'resend'; },
                        }, null));
                    },
                    navigateToSendCode: function () {
                        _this.error = undefined;
                        resolve(sdk_1.FormInput.createEscapeRequest({
                            getId: function () { return 'repick'; },
                            getPresentation: function () {
                                return TransmitSDK.ActionEscapeOptionPresentation.Action;
                            },
                            getDisplayName: function () { return 'repick'; },
                        }, null));
                    },
                    onComplete: function (otp) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest({
                            otpCounter: _this.otpInputCount,
                            otp: otp,
                        }));
                        // NOTE: should be derived from the incoming payload, not manually incremented as below
                        _this.otpInputCount += 1;
                    },
                },
            });
        });
    };
    return EnrollVerifyCodeFormSession;
}(Base_1.BaseUIFormSession));
exports.EnrollVerifyCodeFormSession = EnrollVerifyCodeFormSession;
//# sourceMappingURL=VerifyCode.js.map
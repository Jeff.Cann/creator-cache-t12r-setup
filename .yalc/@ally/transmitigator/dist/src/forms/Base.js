"use strict";
/* eslint-disable @typescript-eslint/ban-types */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseUIFormSession = void 0;
var utils_1 = require("../utils");
var whisper_1 = __importDefault(require("../whisper"));
var BaseUIFormSession = /** @class */ (function () {
    function BaseUIFormSession(onScreenUpdate, onScreenLoading, onScreenError) {
        this.onContinue = utils_1.noop;
        this.onScreenUpdate = onScreenUpdate;
        this.onScreenLoading = onScreenLoading;
        this.onScreenError = onScreenError;
    }
    BaseUIFormSession.prototype.startSession = function (clientContext, actionContext) {
        this.context = clientContext;
    };
    BaseUIFormSession.prototype.promiseFormInput = function () {
        return Promise.reject(new Error('Not implemented...'));
    };
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    BaseUIFormSession.prototype.endSession = function () { };
    /**
     * The payload returned by transmit is as follows:
     * {
     *   data: {
     *     data: { // this payload object below
     *       errorCode: '',
     *       errorDesc: ''
     *     }
     *   }
     * }
     */
    BaseUIFormSession.prototype.onError = function (payload) {
        if (payload) {
            this.error = payload.errorCode;
            whisper_1.default.error({
                message: "Transmit form session error " + payload.errorCode + " occurred",
            });
        }
    };
    return BaseUIFormSession;
}());
exports.BaseUIFormSession = BaseUIFormSession;
//# sourceMappingURL=Base.js.map
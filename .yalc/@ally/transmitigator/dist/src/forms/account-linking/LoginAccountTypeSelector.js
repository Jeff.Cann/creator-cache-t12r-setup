"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginAccountTypeSelectorFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var LoginAccountTypeSelectorFormSession = /** @class */ (function (_super) {
    __extends(LoginAccountTypeSelectorFormSession, _super);
    function LoginAccountTypeSelectorFormSession() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoginAccountTypeSelectorFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'LoginAccountTypeSelector',
                payload: {
                    onComplete: function (payload) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(payload));
                    },
                    status: 'None',
                    error: _this.error,
                },
            });
        });
    };
    return LoginAccountTypeSelectorFormSession;
}(Base_1.BaseUIFormSession));
exports.LoginAccountTypeSelectorFormSession = LoginAccountTypeSelectorFormSession;
//# sourceMappingURL=LoginAccountTypeSelector.js.map
import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface UpdateUsernameScreen {
    type: 'UpdateUsername';
    payload: UpdateUsernameProps;
}
export interface UpdateUsernameOutgoingPayload {
    newUsername: string;
}
export interface UpdateUsernameIncomingPayload {
    username: string;
}
export interface UpdateUsernameProps {
    onComplete: (payload: UpdateUsernameOutgoingPayload) => void;
    status: AsyncStatus;
    username: string;
    error?: string | undefined | Error;
}
export declare class UpdateUsernameFormSession extends BaseUIFormSession {
    protected username: string;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { username }: UpdateUsernameIncomingPayload);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

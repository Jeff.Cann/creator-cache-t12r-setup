import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
import { UserData } from '../../types';
export interface AddAutoAccountSuccessScreen {
    type: 'AddAutoAccountSuccess';
    payload: AddAutoAccountSuccessProps;
}
export interface AddAutoAccountSuccessIncomingPayload {
    username: string;
    userData: UserData | null;
}
export interface AddAutoAccountSuccessProps {
    onComplete: () => void;
    status: AsyncStatus;
    username: string;
    userData: UserData | null;
    error?: string | Error;
}
export interface AddAutoAccountSuccessOutgoingPayload {
    username: string;
}
export declare class AddAutoAccountSuccessFormSession extends BaseUIFormSession {
    protected username: string;
    protected userData: UserData | null;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { username, userData }: AddAutoAccountSuccessIncomingPayload);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

import { AsyncStatus } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface LoginAccountTypeSelectorScreen {
    type: 'LoginAccountTypeSelector';
    payload: LoginAccountTypeSelectorProps;
}
export interface LoginAccountTypeSelectorProps {
    onComplete: (payload: LoginAccountTypeSelectorOutgoingPayload) => void;
    status: AsyncStatus;
    error?: string | Error;
}
export interface LoginAccountTypeSelectorOutgoingPayload {
    isAuto: boolean;
}
export declare class LoginAccountTypeSelectorFormSession extends BaseUIFormSession {
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

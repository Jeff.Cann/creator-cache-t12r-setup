"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.LinkingSuccessFormSession = void 0;
var Base_1 = require("../Base");
var sdk_1 = require("../../sdk");
var LinkingSuccessFormSession = /** @class */ (function (_super) {
    __extends(LinkingSuccessFormSession, _super);
    function LinkingSuccessFormSession(onScreenLoading, onScreenUpdate, _a) {
        var username = _a.username, userData = _a.userData, userRelationships = _a.userRelationships;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.username = username;
        _this.userData = userData;
        _this.userRelationships = userRelationships;
        return _this;
    }
    LinkingSuccessFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.onScreenUpdate({
                type: 'LinkingSuccess',
                payload: {
                    onComplete: function () {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest({}));
                    },
                    username: _this.username,
                    userData: _this.userData,
                    userRelationships: _this.userRelationships,
                    status: 'None',
                    error: _this.error,
                },
            });
        });
    };
    return LinkingSuccessFormSession;
}(Base_1.BaseUIFormSession));
exports.LinkingSuccessFormSession = LinkingSuccessFormSession;
//# sourceMappingURL=LinkingSuccess.js.map
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateUsernameFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var UpdateUsernameFormSession = /** @class */ (function (_super) {
    __extends(UpdateUsernameFormSession, _super);
    function UpdateUsernameFormSession(onScreenLoading, onScreenUpdate, _a) {
        var _b = _a.username, username = _b === void 0 ? '' : _b;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.username = username;
        return _this;
    }
    UpdateUsernameFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'UpdateUsername',
                payload: {
                    onComplete: function (input) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(input));
                    },
                    status: 'None',
                    error: _this.error,
                    username: _this.username,
                },
            });
        });
    };
    return UpdateUsernameFormSession;
}(Base_1.BaseUIFormSession));
exports.UpdateUsernameFormSession = UpdateUsernameFormSession;
//# sourceMappingURL=UpdateUsername.js.map
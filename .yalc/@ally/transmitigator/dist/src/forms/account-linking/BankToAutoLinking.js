"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BankToAutoLinkingFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var BankToAutoLinkingFormSession = /** @class */ (function (_super) {
    __extends(BankToAutoLinkingFormSession, _super);
    function BankToAutoLinkingFormSession(onScreenLoading, onScreenUpdate, _a) {
        var userRelationships = _a.userRelationships, username = _a.username, errorCode = _a.errorCode;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.userRelationships = userRelationships;
        _this.authenticatedUsername = username;
        _this.error = errorCode !== null && errorCode !== void 0 ? errorCode : '';
        return _this;
    }
    BankToAutoLinkingFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'BankToAutoLinking',
                payload: {
                    onComplete: function (payload) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(payload));
                    },
                    onSkip: function () {
                        resolve(sdk_1.FormInput.createEscapeRequest({
                            getId: function () { return 'skip'; },
                            getPresentation: function () {
                                return TransmitSDK.ActionEscapeOptionPresentation.Action;
                            },
                            getDisplayName: function () { return 'skip'; },
                        }, null));
                    },
                    status: 'None',
                    userRelationships: _this.userRelationships,
                    authenticatedUsername: _this.authenticatedUsername,
                    error: _this.error,
                },
            });
        });
    };
    return BankToAutoLinkingFormSession;
}(Base_1.BaseUIFormSession));
exports.BankToAutoLinkingFormSession = BankToAutoLinkingFormSession;
//# sourceMappingURL=BankToAutoLinking.js.map
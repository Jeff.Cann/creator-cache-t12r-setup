"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddAutoAccountSuccessFormSession = void 0;
var Base_1 = require("../Base");
var sdk_1 = require("../../sdk");
var AddAutoAccountSuccessFormSession = /** @class */ (function (_super) {
    __extends(AddAutoAccountSuccessFormSession, _super);
    function AddAutoAccountSuccessFormSession(onScreenLoading, onScreenUpdate, _a) {
        var username = _a.username, userData = _a.userData;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.username = username;
        _this.userData = userData;
        return _this;
    }
    AddAutoAccountSuccessFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.onScreenUpdate({
                type: 'AddAutoAccountSuccess',
                payload: {
                    onComplete: function () {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest({}));
                    },
                    username: _this.username,
                    userData: _this.userData,
                    status: 'None',
                    error: _this.error,
                },
            });
        });
    };
    return AddAutoAccountSuccessFormSession;
}(Base_1.BaseUIFormSession));
exports.AddAutoAccountSuccessFormSession = AddAutoAccountSuccessFormSession;
//# sourceMappingURL=AddAutoAccountSuccess.js.map
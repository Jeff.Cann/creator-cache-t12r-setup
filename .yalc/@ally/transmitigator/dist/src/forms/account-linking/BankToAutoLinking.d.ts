import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
import { AllyUserRole } from '../../types';
export interface BankToAutoLinkingScreen {
    type: 'BankToAutoLinking';
    payload: BankToAutoLinkingProps;
}
export interface BankToAutoLinkingIncomingPayload {
    userRelationships: AllyUserRole;
    username: string;
    errorCode?: string | Error;
}
export interface BankToAutoLinkingProps {
    onComplete: (payload: BankToAutoLinkingOutgoingPayload) => void;
    onSkip: () => void;
    status: AsyncStatus;
    userRelationships: AllyUserRole;
    authenticatedUsername: string;
    error?: string | Error;
}
export interface BankToAutoLinkingOutgoingPayload {
    username: string;
    password: string;
}
export declare class BankToAutoLinkingFormSession extends BaseUIFormSession {
    protected userRelationships: AllyUserRole;
    protected authenticatedUsername: string;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { userRelationships, username, errorCode, }: BankToAutoLinkingIncomingPayload);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

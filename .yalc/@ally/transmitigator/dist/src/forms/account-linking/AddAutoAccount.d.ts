import { AsyncStatus } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface AddAutoAccountScreen {
    type: 'AddAutoAccount';
    payload: AddAutoAccountProps;
}
export interface AddAutoAccountProps {
    onComplete: () => void;
    status: AsyncStatus;
    error?: string | Error;
}
export declare class AddAutoAccountFormSession extends BaseUIFormSession {
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

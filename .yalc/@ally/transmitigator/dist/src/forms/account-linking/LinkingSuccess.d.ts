import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
import { AllyUserRole, UserData } from '../../types';
export interface LinkingSuccessScreen {
    type: 'LinkingSuccess';
    payload: LinkingSuccessProps;
}
export interface LinkingSuccessIncomingPayload {
    username: string;
    userRelationships: AllyUserRole;
    userData: UserData | null;
}
export interface LinkingSuccessProps {
    onComplete: () => void;
    status: AsyncStatus;
    username: string;
    userData: UserData | null;
    userRelationships: AllyUserRole;
    error?: string | Error;
}
export declare class LinkingSuccessFormSession extends BaseUIFormSession {
    protected username: string;
    protected userRelationships: AllyUserRole;
    protected userData: UserData | null;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { username, userData, userRelationships }: LinkingSuccessIncomingPayload);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

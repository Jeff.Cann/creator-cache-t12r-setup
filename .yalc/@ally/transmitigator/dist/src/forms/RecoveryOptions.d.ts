import { AsyncStatus, JourneyDispatcher } from '../AllyTransmitUIHandler';
import { BaseUIFormSession } from './Base';
export interface RecoveryOptionsScreen {
    type: 'RecoveryOptions';
    payload: RecoveryOptionsProps;
}
export interface RecoveryOptionsPayload {
    ssn?: string;
    passwordHint: boolean;
    createNewPassword: boolean;
    isResetPassword: boolean;
}
export interface RecoveryOptionsProps {
    onComplete: (payload: RecoveryOptionsPayload) => void;
    status: AsyncStatus;
    error?: string;
    errorCode: null | string;
    isCustomer: boolean;
    isResetPassword: boolean;
}
export interface RecoveryOptionsInput {
    isCustomer: boolean;
    isResetPassword: boolean;
    errorCode: null | string;
}
export declare class RecoveryOptionsFormSession extends BaseUIFormSession {
    protected isCustomer: boolean;
    protected isResetPassword: boolean;
    protected errorCode: null | string;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { isCustomer, isResetPassword, errorCode, }: RecoveryOptionsInput);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

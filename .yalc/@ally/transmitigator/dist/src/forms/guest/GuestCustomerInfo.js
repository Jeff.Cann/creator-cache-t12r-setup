"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.GuestEnrollCustomerInfoFormSession = void 0;
var sdk_1 = require("../../sdk");
var GuestBase_1 = require("./GuestBase");
var GuestEnrollCustomerInfoFormSession = /** @class */ (function (_super) {
    __extends(GuestEnrollCustomerInfoFormSession, _super);
    function GuestEnrollCustomerInfoFormSession() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GuestEnrollCustomerInfoFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'GuestCustomerInfo',
                payload: {
                    // this will never actualy be called
                    onComplete: function () {
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest({}));
                    },
                    status: 'None',
                    error: _this.error,
                },
            });
        });
    };
    return GuestEnrollCustomerInfoFormSession;
}(GuestBase_1.GuestBaseUIFormSession));
exports.GuestEnrollCustomerInfoFormSession = GuestEnrollCustomerInfoFormSession;
//# sourceMappingURL=GuestCustomerInfo.js.map
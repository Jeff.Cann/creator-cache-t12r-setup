import { AsyncStatus } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface GuestEnrollConfirmationScreen {
    type: 'GuestConfirmation';
    payload: GuestEnrollConfirmationProps;
}
export interface GuestEnrollConfirmationProps {
    onComplete: () => void;
    status: AsyncStatus;
    error?: string | Error;
}
export declare class GuestEnrollConfirmationFormSession extends BaseUIFormSession {
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

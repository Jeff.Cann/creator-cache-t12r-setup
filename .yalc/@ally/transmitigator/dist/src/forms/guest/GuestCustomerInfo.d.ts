import { AsyncStatus } from '../../AllyTransmitUIHandler';
import { GuestBaseUIFormSession } from './GuestBase';
export interface GuestEnrollCustomerInfoScreen {
    type: 'GuestCustomerInfo';
    payload: GuestEnrollCustomerInfoProps;
}
export interface GuestEnrollCustomerInfoProps {
    onComplete?: () => any;
    status: AsyncStatus;
    error?: string | Error;
}
export declare class GuestEnrollCustomerInfoFormSession extends GuestBaseUIFormSession {
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

import { AsyncStatus } from '../../AllyTransmitUIHandler';
import { GuestBaseUIFormSession } from './GuestBase';
export interface GuestEnrollCredentialCreationScreen {
    type: 'GuestCredentialCreation';
    payload: GuestEnrollCredentialCreationProps;
}
export interface GuestEnrollCredentialCreationPayload {
    loginName: string;
    password: string;
    passwordHint: string;
    registerDevice: boolean;
}
export interface GuestEnrollCredentialCreationProps {
    onComplete: (payload: GuestEnrollCredentialCreationPayload) => void;
    status: AsyncStatus;
    error?: string | Error;
}
export declare class GuestEnrollCredentialCreationFormSession extends GuestBaseUIFormSession {
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

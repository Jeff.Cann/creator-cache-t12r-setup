"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.GuestBaseUIFormSession = exports.getErrorFromPayload = void 0;
var Base_1 = require("../Base");
var getErrorFromPayload = function (payload) {
    var _a;
    var errors = (_a = payload === null || payload === void 0 ? void 0 : payload.application_data) === null || _a === void 0 ? void 0 : _a._validation_errors;
    if (errors) {
        // We can only handle one error code at a time, so just return the first
        var keys = Object.keys(errors);
        if (keys && keys.length) {
            return errors[keys[0]].code;
        }
    }
    return undefined;
};
exports.getErrorFromPayload = getErrorFromPayload;
var GuestBaseUIFormSession = /** @class */ (function (_super) {
    __extends(GuestBaseUIFormSession, _super);
    function GuestBaseUIFormSession(onScreenLoading, onScreenUpdate, payload) {
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.error = exports.getErrorFromPayload(payload);
        return _this;
    }
    return GuestBaseUIFormSession;
}(Base_1.BaseUIFormSession));
exports.GuestBaseUIFormSession = GuestBaseUIFormSession;
//# sourceMappingURL=GuestBase.js.map
import { JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
interface ErrorProps {
    code: string;
    message: string;
}
/**
 * Errors are returned in the following format:
 *  application_data: {
      ...,
      _validation_errors: {
        loginName: {
          code: "CIAM_E08_02_001"
          message: "Username EXIST! Try another Username or Login now."
        }
      }
    }
 */
export interface GuestBasePayload {
    application_data: {
        _validation_errors: {
            [key: string]: ErrorProps;
        };
    };
}
export declare const getErrorFromPayload: (payload?: GuestBasePayload | undefined) => string | undefined;
export declare class GuestBaseUIFormSession extends BaseUIFormSession {
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, payload?: GuestBasePayload);
}
export {};

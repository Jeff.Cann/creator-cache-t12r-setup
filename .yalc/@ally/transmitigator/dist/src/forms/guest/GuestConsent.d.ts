import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface GuestEnrollConsentScreen {
    type: 'GuestConsent';
    payload: GuestEnrollConsentProps;
}
export interface GuestConsentOption {
    contentUrl: string;
    version: string;
    title: string;
    consentType: string;
}
export interface GuestEnrollConsentProps {
    onComplete: () => void;
    status: AsyncStatus;
    error?: string | Error;
    consents: GuestConsentOption[];
    userData: GuestConsentUserData;
}
export interface GuestConsentUserData {
    cupid: string | null;
    tpn: string | null;
    profileId: string | null;
    guid: string | null;
}
export interface GuestEnrollConsentPayload {
    guestEsign?: GuestConsentOption;
    guestTos?: GuestConsentOption;
    creditcardEsign?: GuestConsentOption;
    creditcardTos?: GuestConsentOption;
    consolidatedEsign?: GuestConsentOption;
    autoTos?: GuestConsentOption;
    bankTos?: GuestConsentOption;
    investTos?: GuestConsentOption;
    userData: GuestConsentUserData;
}
export declare class GuestEnrollConsentFormSession extends BaseUIFormSession {
    protected consents: GuestConsentOption[];
    protected userData: GuestConsentUserData;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, payload: GuestEnrollConsentPayload);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

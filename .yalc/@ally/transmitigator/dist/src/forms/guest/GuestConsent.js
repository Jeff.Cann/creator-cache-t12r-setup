"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GuestEnrollConsentFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
function getConsents(payload) {
    var guestEsign = payload.guestEsign, guestTos = payload.guestTos, creditcardEsign = payload.creditcardEsign, creditcardTos = payload.creditcardTos, consolidatedEsign = payload.consolidatedEsign, autoTos = payload.autoTos, bankTos = payload.bankTos, investTos = payload.investTos;
    var consents = [];
    if (consolidatedEsign) {
        consents.push(__assign(__assign({}, consolidatedEsign), { title: 'Consent to Receive Information Electronically', consentType: 'electronicConsent' }));
    }
    if (creditcardEsign) {
        consents.push(__assign(__assign({}, creditcardEsign), { title: 'Consent to Receive Information Electronically', consentType: 'electronicConsent' }));
    }
    if (guestEsign) {
        consents.push(__assign(__assign({}, guestEsign), { title: 'Consent to Receive Information Electronically', consentType: 'electronicConsent' }));
    }
    if (autoTos) {
        consents.push(__assign(__assign({}, autoTos), { title: 'Ally Auto Terms of Use', consentType: 'termsOfUse' }));
    }
    if (bankTos) {
        consents.push(__assign(__assign({}, bankTos), { title: 'Online Banking Services Agreement', consentType: 'termsOfUse' }));
    }
    if (creditcardTos) {
        consents.push(__assign(__assign({}, creditcardTos), { title: 'Ally Credit Card Online Terms of Use', consentType: 'termsOfUse' }));
    }
    if (guestTos) {
        consents.push(__assign(__assign({}, guestTos), { title: 'Ally Guest User Services Terms and Conditions', consentType: 'termsOfUse' }));
    }
    if (investTos) {
        consents.push(__assign(__assign({}, investTos), { title: 'Ally Invest Terms of Use', consentType: 'termsOfUse' }));
    }
    return consents;
}
var GuestEnrollConsentFormSession = /** @class */ (function (_super) {
    __extends(GuestEnrollConsentFormSession, _super);
    function GuestEnrollConsentFormSession(onScreenLoading, onScreenUpdate, payload) {
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.consents = getConsents(payload);
        _this.userData = payload.userData;
        return _this;
    }
    GuestEnrollConsentFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'GuestConsent',
                payload: {
                    onComplete: function () {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest({}));
                    },
                    status: 'None',
                    error: _this.error,
                    consents: _this.consents,
                    userData: _this.userData,
                },
            });
        });
    };
    return GuestEnrollConsentFormSession;
}(Base_1.BaseUIFormSession));
exports.GuestEnrollConsentFormSession = GuestEnrollConsentFormSession;
//# sourceMappingURL=GuestConsent.js.map
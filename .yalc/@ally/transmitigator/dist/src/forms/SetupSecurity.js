"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SetupSecurityFormSession = exports.serializeOTPMethods = exports.normalizeOTPMethods = void 0;
var sdk_1 = require("../sdk");
var Base_1 = require("./Base");
/**
 * Converts the single object returned from the server into a more UI friendly array
 * and removing unnecessary keys from the payload
 */
var normalizeOTPMethods = function (methods) {
    // Since OTP methods are returned in a dumb way, we need to prune non-OTP keys
    var methodsOnly = Object.keys(methods).reduce(function (obj, key, _) {
        if (Object.prototype.hasOwnProperty.call(methods, key) &&
            /^allyRCotpDelivery/.test(key)) {
            obj[key] = methods[key];
        }
        return obj;
    }, {});
    return new Array(6).fill(undefined).reduce(function (options, _, i) {
        var _a;
        var offset = (i + 1).toString();
        var kValue = "allyRCotpDeliveryValue" + offset;
        var value = methodsOnly[kValue];
        var kMethod = "allyRCotpDeliveryMethod" + offset;
        var type = (_a = methodsOnly[kMethod]) === null || _a === void 0 ? void 0 : _a.toUpperCase();
        if (value) {
            options.push({
                type: type,
                value: value,
            });
        }
        return options;
    }, []);
};
exports.normalizeOTPMethods = normalizeOTPMethods;
/**
 * Converts the UI friendly array back into the format the API expects.
 * Adds any existing EMAIL otp options if space allows. Only 6 OTP methods can be set
 * at a time. EMAIL otp options will be overwritten in favor of SMS options.
 */
var serializeOTPMethods = function (originalOptions, otpMethods) {
    var originalEmails = originalOptions.filter(function (_a) {
        var type = _a.type;
        return type === 'EMAIL';
    });
    return otpMethods
        .concat(originalEmails) // merge existing EMAIL options with otpMethods from UI
        .concat(Array(6).fill({ type: '', value: '' })) // server requires all 6 keys
        .slice(0, 6) // only use the first 6
        .reduce(function (acc, cur, i) {
        var _a;
        var value = cur.value, type = cur.type;
        var objectSuffix = i + 1;
        return __assign(__assign({}, acc), (_a = {}, _a["allyRCotpDeliveryValue" + objectSuffix] = value, _a["allyRCotpDeliveryMethod" + objectSuffix] = type, _a));
    }, {});
};
exports.serializeOTPMethods = serializeOTPMethods;
var SetupSecurityFormSession = /** @class */ (function (_super) {
    __extends(SetupSecurityFormSession, _super);
    function SetupSecurityFormSession(onScreenLoading, onScreenUpdate, options, shouldConfirmOptions) {
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        var normalizedOptions = exports.normalizeOTPMethods(options);
        _this.originalOptions = normalizedOptions;
        // We only allow SMS otp options now
        _this.options = normalizedOptions.filter(function (_a) {
            var type = _a.type;
            return type === 'SMS';
        });
        _this.shouldConfirmOptions = shouldConfirmOptions;
        return _this;
    }
    SetupSecurityFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'SetupSecurity',
                payload: {
                    onComplete: function (formData) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(exports.serializeOTPMethods(_this.originalOptions, formData)));
                    },
                    status: 'None',
                    options: _this.options,
                    shouldConfirmOptions: _this.shouldConfirmOptions,
                },
            });
        });
    };
    return SetupSecurityFormSession;
}(Base_1.BaseUIFormSession));
exports.SetupSecurityFormSession = SetupSecurityFormSession;
//# sourceMappingURL=SetupSecurity.js.map
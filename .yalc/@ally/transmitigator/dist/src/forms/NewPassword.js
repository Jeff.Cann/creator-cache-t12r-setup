"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewPasswordFormSession = void 0;
var sdk_1 = require("../sdk");
var Base_1 = require("./Base");
var NewPasswordFormSession = /** @class */ (function (_super) {
    __extends(NewPasswordFormSession, _super);
    function NewPasswordFormSession(onScreenLoading, onScreenUpdate, _a) {
        var userID = _a.userID, errorCode = _a.errorCode, isBiometricEnrolled = _a.isBiometricEnrolled;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.username = userID;
        _this.errorCode = errorCode;
        _this.isBiometricEnrolled = isBiometricEnrolled || false;
        return _this;
    }
    NewPasswordFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'NewPassword',
                payload: {
                    onComplete: function (input) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(input));
                    },
                    status: 'None',
                    /* username is needed for client side validation so we can ensure that new password !== username */
                    username: _this.username,
                    errorCode: _this.errorCode,
                    isBiometricEnrolled: _this.isBiometricEnrolled,
                },
            });
        });
    };
    return NewPasswordFormSession;
}(Base_1.BaseUIFormSession));
exports.NewPasswordFormSession = NewPasswordFormSession;
//# sourceMappingURL=NewPassword.js.map
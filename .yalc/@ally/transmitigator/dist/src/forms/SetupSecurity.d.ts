import { AsyncStatus, JourneyDispatcher } from '../AllyTransmitUIHandler';
import { BaseUIFormSession } from './Base';
export declare type DeliveryMethod = 'SMS' | 'EMAIL' | '';
export interface OTPDeliveryMethods {
    allyRCotpDeliveryValue1: string;
    allyRCotpDeliveryValue2: string;
    allyRCotpDeliveryValue3: string;
    allyRCotpDeliveryValue4: string;
    allyRCotpDeliveryValue5: string;
    allyRCotpDeliveryValue6: string;
    allyRCotpDeliveryMethod1: DeliveryMethod;
    allyRCotpDeliveryMethod2: DeliveryMethod;
    allyRCotpDeliveryMethod3: DeliveryMethod;
    allyRCotpDeliveryMethod4: DeliveryMethod;
    allyRCotpDeliveryMethod5: DeliveryMethod;
    allyRCotpDeliveryMethod6: DeliveryMethod;
}
export interface OTPPayloadInput extends OTPDeliveryMethods {
    MFA_STATUS?: 'MFA SETUP' | 'MFA VERIFIED' | 'MFA UNVERIFIED';
    interdiction_flag?: boolean;
}
export interface SetupSecurityScreen {
    type: 'SetupSecurity';
    payload: SetupSecurityProps;
}
export interface SetupSecurityProps {
    onComplete: (formData: OTPMethod[]) => void;
    status: AsyncStatus;
    options: OTPMethod[];
    shouldConfirmOptions: boolean;
}
export interface OTPMethod {
    type: DeliveryMethod;
    value: string;
}
/**
 * Converts the single object returned from the server into a more UI friendly array
 * and removing unnecessary keys from the payload
 */
export declare const normalizeOTPMethods: (methods: OTPPayloadInput) => OTPMethod[];
/**
 * Converts the UI friendly array back into the format the API expects.
 * Adds any existing EMAIL otp options if space allows. Only 6 OTP methods can be set
 * at a time. EMAIL otp options will be overwritten in favor of SMS options.
 */
export declare const serializeOTPMethods: (originalOptions: OTPMethod[], otpMethods: OTPMethod[]) => OTPDeliveryMethods;
export declare class SetupSecurityFormSession extends BaseUIFormSession {
    protected originalOptions: OTPMethod[];
    protected options: OTPMethod[];
    protected shouldConfirmOptions: boolean;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, options: OTPPayloadInput, shouldConfirmOptions: boolean);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfirmSsnFormSession = void 0;
var sdk_1 = require("../../sdk");
var Base_1 = require("../Base");
var ConfirmSsnFormSession = /** @class */ (function (_super) {
    __extends(ConfirmSsnFormSession, _super);
    function ConfirmSsnFormSession(onScreenLoading, onScreenUpdate, _a) {
        var errorCode = _a.errorCode;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.errorCode = errorCode;
        return _this;
    }
    ConfirmSsnFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'ConfirmSSN',
                payload: {
                    onComplete: function (input) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(input));
                    },
                    status: 'None',
                    errorCode: _this.errorCode,
                },
            });
        });
    };
    return ConfirmSsnFormSession;
}(Base_1.BaseUIFormSession));
exports.ConfirmSsnFormSession = ConfirmSsnFormSession;
//# sourceMappingURL=ConfirmSSN.js.map
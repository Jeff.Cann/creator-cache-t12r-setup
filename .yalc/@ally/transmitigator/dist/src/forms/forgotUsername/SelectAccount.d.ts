import { AsyncStatus } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface SelectAccountScreen {
    type: 'SelectAccount';
    payload: SelectAccountProps;
}
export interface SelectAccountOutgoingPayload {
    auto: boolean;
    bank: boolean;
    guest: boolean;
}
export interface SelectAccountProps {
    onComplete: (payload: SelectAccountOutgoingPayload) => void;
    status: AsyncStatus;
    error?: string | Error;
}
export declare class SelectAccountFormSession extends BaseUIFormSession {
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

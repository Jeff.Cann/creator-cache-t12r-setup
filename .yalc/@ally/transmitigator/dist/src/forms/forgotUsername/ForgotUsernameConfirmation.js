"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ForgotUsernameConfirmationFormSession = void 0;
var Base_1 = require("../Base");
var ForgotUsernameConfirmationFormSession = /** @class */ (function (_super) {
    __extends(ForgotUsernameConfirmationFormSession, _super);
    function ForgotUsernameConfirmationFormSession(onScreenLoading, onScreenUpdate, _a) {
        var loginName = _a.loginName;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        // converting to username so it is consistent with other parts of the app
        _this.username = loginName;
        return _this;
    }
    ForgotUsernameConfirmationFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.onScreenUpdate({
                type: 'ForgotUsernameConfirmation',
                payload: {
                    onComplete: function () {
                        resolve({ username: _this.username });
                    },
                    username: _this.username,
                    status: 'None',
                },
            });
        });
    };
    return ForgotUsernameConfirmationFormSession;
}(Base_1.BaseUIFormSession));
exports.ForgotUsernameConfirmationFormSession = ForgotUsernameConfirmationFormSession;
//# sourceMappingURL=ForgotUsernameConfirmation.js.map
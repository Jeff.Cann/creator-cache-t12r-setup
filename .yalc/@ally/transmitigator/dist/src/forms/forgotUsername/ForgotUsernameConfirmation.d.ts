import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface ForgotUsernameConfirmationScreen {
    type: 'ForgotUsernameConfirmation';
    payload: ForgotUsernameConfirmationProps;
}
export interface ForgotUsernameConfirmationProps {
    status: AsyncStatus;
    username: string;
    onComplete: () => void;
}
export interface ForgotUsernameConfirmationPayload {
    loginName: string;
}
export declare class ForgotUsernameConfirmationFormSession extends BaseUIFormSession {
    protected username: string;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { loginName }: ForgotUsernameConfirmationPayload);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

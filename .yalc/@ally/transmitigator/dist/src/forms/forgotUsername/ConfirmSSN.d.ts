import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
import { BaseUIFormSession } from '../Base';
export interface ConfirmSsnScreen {
    type: 'ConfirmSSN';
    payload: ConfirmSsnProps;
}
export interface ConfirmSsnPayload {
    ssn: string;
}
export interface ConfirmSsnProps {
    onComplete: (payload: ConfirmSsnPayload) => void;
    status: AsyncStatus;
    error?: string;
    errorCode: string | undefined;
}
export interface ConfirmSsnInput {
    errorCode: string | undefined;
}
export declare class ConfirmSsnFormSession extends BaseUIFormSession {
    protected errorCode: string | undefined;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { errorCode }: ConfirmSsnInput);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

import { BaseUIFormSession } from './Base';
import { AsyncStatus, JourneyDispatcher } from '../AllyTransmitUIHandler';
import { OTPDeliveryMethods } from './SetupSecurity';
export interface SetupData {
    deliveryOptions: OTPDeliveryMethods;
    deviceRegistration: boolean;
    passwordHint: string | null;
    isEsignAccepted: boolean;
}
export interface SetupCompleteScreen {
    type: 'SetupComplete';
    payload: SetupCompleteProps;
}
export interface SetupCompleteProps {
    setupData: SetupData;
    onComplete: () => void;
    status: AsyncStatus;
}
export declare class SetupCompleteFormSession extends BaseUIFormSession {
    protected setupData: SetupData;
    constructor(onScreenUpdate: JourneyDispatcher, onScreenLoading: () => void, setupData: SetupData);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.SetupCompleteFormSession = void 0;
var sdk_1 = require("../sdk");
var Base_1 = require("./Base");
var SetupCompleteFormSession = /** @class */ (function (_super) {
    __extends(SetupCompleteFormSession, _super);
    function SetupCompleteFormSession(onScreenUpdate, onScreenLoading, setupData) {
        var _this = _super.call(this, onScreenUpdate, onScreenLoading) || this;
        _this.setupData = setupData;
        return _this;
    }
    SetupCompleteFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'SetupComplete',
                payload: {
                    setupData: _this.setupData,
                    onComplete: function () {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest({}));
                    },
                    status: 'None',
                },
            });
        });
    };
    return SetupCompleteFormSession;
}(Base_1.BaseUIFormSession));
exports.SetupCompleteFormSession = SetupCompleteFormSession;
//# sourceMappingURL=SetupComplete.js.map
import { noop } from '../utils';
import { JourneyDispatcher, ErrorDispatcher } from '../AllyTransmitUIHandler';
export interface TransmitFormErrorPayload {
    errorCode: string;
    errorDesc: string;
}
export declare abstract class BaseUIFormSession implements TransmitSDK.UIFormSession {
    protected error: Error | string | undefined;
    protected value?: string;
    protected onScreenUpdate: JourneyDispatcher;
    protected onScreenError?: ErrorDispatcher;
    protected onScreenLoading: () => void;
    protected context?: object | null;
    startSession(clientContext: object | null, actionContext: TransmitSDK.PolicyAction | null): void;
    onContinue: typeof noop;
    constructor(onScreenUpdate: JourneyDispatcher, onScreenLoading: () => void, onScreenError?: ErrorDispatcher);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
    endSession(): void;
    /**
     * The payload returned by transmit is as follows:
     * {
     *   data: {
     *     data: { // this payload object below
     *       errorCode: '',
     *       errorDesc: ''
     *     }
     *   }
     * }
     */
    onError(payload: TransmitFormErrorPayload | null): void;
}

import { BaseUIFormSession } from '../Base';
import { AsyncStatus, JourneyDispatcher } from '../../AllyTransmitUIHandler';
export interface ChangePasswordCreatorCacheScreen {
    type: 'ChangePasswordCreatorCache';
    payload: ChangePasswordCreatorCacheProps;
}
export interface ChangePasswordCreatorCacheIncomingPayload {
    userID: string;
    errorCode?: string;
}
export interface ChangePasswordCreatorCacheOutgoingPayload {
    newPassword: string;
}
export interface ChangePasswordCreatorCacheProps {
    onComplete: (payload: ChangePasswordCreatorCacheOutgoingPayload) => void;
    status: AsyncStatus;
    username: string;
    error?: string | Error;
}
export declare class ChangePasswordCreatorCacheFormSession extends BaseUIFormSession {
    protected username: string;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { errorCode, userID }: ChangePasswordCreatorCacheIncomingPayload);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

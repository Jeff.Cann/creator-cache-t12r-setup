"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChangePasswordCreatorCacheFormSession = void 0;
var Base_1 = require("../Base");
var sdk_1 = require("../../sdk");
var ChangePasswordCreatorCacheFormSession = /** @class */ (function (_super) {
    __extends(ChangePasswordCreatorCacheFormSession, _super);
    function ChangePasswordCreatorCacheFormSession(onScreenLoading, onScreenUpdate, _a) {
        var errorCode = _a.errorCode, userID = _a.userID;
        var _this = _super.call(this, onScreenLoading, onScreenUpdate) || this;
        _this.error = errorCode;
        _this.username = userID;
        return _this;
    }
    ChangePasswordCreatorCacheFormSession.prototype.promiseFormInput = function () {
        var _this = this;
        return new Promise(function (resolve, _reject) {
            _this.onScreenUpdate({
                type: 'ChangePasswordCreatorCache',
                payload: {
                    onComplete: function (input) {
                        _this.onScreenLoading();
                        resolve(sdk_1.FormInput.createFormInputSubmissionRequest(input));
                    },
                    status: 'None',
                    /* username is needed for client side validation so we can ensure that new password !== username */
                    username: _this.username,
                    error: _this.error,
                },
            });
        });
    };
    return ChangePasswordCreatorCacheFormSession;
}(Base_1.BaseUIFormSession));
exports.ChangePasswordCreatorCacheFormSession = ChangePasswordCreatorCacheFormSession;
//# sourceMappingURL=ChangePassword.js.map
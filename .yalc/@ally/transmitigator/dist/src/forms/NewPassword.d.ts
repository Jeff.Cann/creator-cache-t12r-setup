import { AsyncStatus, JourneyDispatcher } from '../AllyTransmitUIHandler';
import { BaseUIFormSession } from './Base';
export interface NewPasswordScreen {
    type: 'NewPassword';
    payload: NewPasswordProps;
}
export interface PasswordPayload {
    newPassword: string;
    confirmPassword: string;
    passwordHint: string;
}
export interface NewPasswordProps {
    onComplete: (payload: PasswordPayload) => void;
    status: AsyncStatus;
    username?: string;
    isBiometricEnrolled?: boolean;
    errorCode: null | string;
}
export interface NewPasswordInput {
    userID: string;
    errorCode: null | string;
    isBiometricEnrolled?: boolean;
}
export declare class NewPasswordFormSession extends BaseUIFormSession {
    protected username: string;
    protected isBiometricEnrolled: boolean;
    protected errorCode: null | string;
    constructor(onScreenLoading: JourneyDispatcher, onScreenUpdate: () => void, { userID, errorCode, isBiometricEnrolled }: NewPasswordInput);
    promiseFormInput(): Promise<TransmitSDK.FormInput>;
}

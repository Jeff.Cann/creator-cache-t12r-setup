import { Hooks } from './hooks';
import * as rawActions from './actions';
import { Setup, Actions, Options, TransmitigatorOptions } from './types';
declare type A = typeof rawActions;
export declare type TransmitRef = Actions<A> & Hooks<A>;
export declare function maybeStoreOtpHeaders(headers: any[]): void;
/**
 * Derived values to be "injected" into the given factory options.
 * This is used to store off the `sdkReadyPromise` and to setup the SDK for
 * public consumption.
 */
export declare function setup(options: Options): Setup;
/**
 * Transmit factory function.
 * Returns an object with all of the transmit actions and (optional react hooks)
 */
export default function factory(options?: TransmitigatorOptions): TransmitRef;
export {};

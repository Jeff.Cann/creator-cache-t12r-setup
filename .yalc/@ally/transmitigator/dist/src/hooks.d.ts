import { ScreenPayload } from './AllyTransmitUIHandler';
import { Actions, FMap, LDFlagSet } from './types';
import * as act from './actions';
import { SessionStatus } from './reducer';
import { TransmitAuthPayload } from './utils';
export declare type HookActions<A extends FMap> = {
    [K in keyof Actions<A>]: {
        (input?: Omit<Parameters<Actions<A>[K]>[0], 'onScreenUpdate' | 'onScreenError' | 'onScreenLoading' | 'onScreenRedirect' | 'deviceData'>): Promise<TransmitAuthPayload | null>;
    };
};
export declare type UseTransmitHook<A extends FMap> = {
    actions: HookActions<A>;
    activeScreen: ScreenPayload;
    sessionStatus: SessionStatus;
    navigateToLogin: (payload?: any) => void;
    navigateToForgotUsername: () => void;
    navigateToForgotPassword: (payload?: any) => void;
    navigateToResetPassword: () => void;
    navigateToOleCustomer: () => void;
    navigateToGuestOle: () => void;
    ticketID: string | null;
    clearTicketID: () => void;
    setFeatureFlags: (featureFlags: LDFlagSet) => void;
};
export declare type Hooks<A extends FMap> = {
    useTransmit: () => UseTransmitHook<A>;
};
declare type A = typeof act;
export declare type UseTransmitReactHook = UseTransmitHook<A>;
export declare type TransmitFunc = (...args: any[]) => TransmitAuthPayload | null;
declare const _default: (actions: Actions<A>) => Hooks<A>;
export default _default;

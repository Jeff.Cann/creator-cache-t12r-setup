"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var whisper_1 = __importDefault(require("@ally/whisper"));
var logger = whisper_1.default({
    silent: process.env.NODE_ENV === 'test',
    nameSpace: '@ally/transmitigator',
});
exports.default = logger;
//# sourceMappingURL=whisper.js.map
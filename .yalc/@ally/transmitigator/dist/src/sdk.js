"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransportResponse = exports.TransportRequest = exports.AuthenticationErrorRecovery = exports.TargetBasedAuthenticatorInput = exports.AuthenticatorSelectionResult = exports.JsonDataProcessingResult = exports.AuthenticationErrorCode = exports.InputOrControlResponse = exports.SDKConnectionSettings = exports.OtpInputRequestResend = exports.OtpInputOtpSubmission = exports.RedirectResponseType = exports.UIAssetsDownloadMode = exports.AuthenticationError = exports.ControlRequestType = exports.ConfirmationInput = exports.ControlRequest = exports.PasswordInput = exports.RedirectInput = exports.RedirectType = exports.OtpChannel = exports.FormInput = exports.LogLevel = exports.myRiskID = exports.xmsdk = exports.com = exports.getSdk = exports.isObject = void 0;
// DO NOT IMPORT ANYTHING IN THIS FILE.
// IT'S MORE THAN LIKELY YOU'LL CREATE A CIRCULAR DEPENDENCY CHAIN!
/*
  NOTE: making an import exception for this one case, alternative would be recreating the which-env code in this file
*/
var which_env_1 = __importDefault(require("@ally/which-env"));
var isObject = function (x) {
    return x !== typeof undefined && typeof x === 'object';
};
exports.isObject = isObject;
// The Transmit SDK is attached to the window.
// Both of these are requirements for this package to work, so we'll throw an
// error if either of them is missing. These should be loaded in by a <script>
// tag in your project's Webpack HTML template.
var getSdk = function (global) {
    if (global === void 0) { global = window; }
    var xmsdk = global.xmsdk, com = global.com;
    if (!exports.isObject(xmsdk) || !exports.isObject(com)) {
        throw new ReferenceError('Missing Transmit SDK');
    }
    return {
        com: com,
        xmsdk: xmsdk,
    };
};
exports.getSdk = getSdk;
function initRiskIdSdk() {
    // CIAM-9112 stop multiple initializations of RiskID
    if (window.myRiskID) {
        return window.myRiskID;
    }
    var clientIDs = {
        qa1: '33d6f7.stage.ally.riskid.security',
        qa2: '33d6f7.stage.ally.riskid.security',
        qa3: '33d6f7.stage.ally.riskid.security',
        cap: '33d6f7.stage.ally.riskid.security',
        psp: '33d6f7.stage.ally.riskid.security',
        dev: 'a5cd67.signin.ally.riskid.security',
        local: 'a5cd67.signin.ally.riskid.security',
        fix: 'a5cd67.signin.ally.riskid.security',
        emulatorLLE: 'a5cd67.signin.ally.riskid.security',
        emulatorProd: 'a5cd67.signin.ally.riskid.security',
        prod: 'd95954.prod.ally.riskid.security',
    };
    var riskIdBaseUrls = {
        qa1: 'https://secure-qa1.ally.com/acs/',
        qa2: 'https://secure-qa2.ally.com/acs/',
        qa3: 'https://secure-qa3.ally.com/acs/',
        cap: 'https://secure-cap.ally.com/acs/',
        psp: 'https://secure-prodsupt.ally.com/acs/',
        dev: 'https://secure-dev.ally.com/acs/',
        local: window.location.origin + "/acs/",
        fix: 'https://secure-fix.ally.com/acs/',
        emulatorLLE: 'https://secure-dev.ally.com/acs/',
        emulatorProd: 'https://secure-dev.ally.com/acs/',
        prod: 'https://secure.ally.com/acs/',
    };
    var clientID = which_env_1.default(window.location.href).pick(clientIDs);
    var riskIdBaseUrl = which_env_1.default(window.location.href).pick(riskIdBaseUrls);
    var RiskID = window.RiskID;
    var myRiskID = new RiskID(riskIdBaseUrl, clientID);
    myRiskID.init();
    window.myRiskID = myRiskID;
    return myRiskID;
}
var _a = exports.getSdk(), com = _a.com, xmsdk = _a.xmsdk;
exports.com = com;
exports.xmsdk = xmsdk;
var myRiskID = initRiskIdSdk();
exports.myRiskID = myRiskID;
var _b = com.ts.mobile.sdk, LogLevel = _b.LogLevel, FormInput = _b.FormInput, OtpChannel = _b.OtpChannel, RedirectType = _b.RedirectType, RedirectInput = _b.RedirectInput, PasswordInput = _b.PasswordInput, ControlRequest = _b.ControlRequest, ConfirmationInput = _b.ConfirmationInput, ControlRequestType = _b.ControlRequestType, AuthenticationError = _b.AuthenticationError, UIAssetsDownloadMode = _b.UIAssetsDownloadMode, RedirectResponseType = _b.RedirectResponseType, OtpInputOtpSubmission = _b.OtpInputOtpSubmission, OtpInputRequestResend = _b.OtpInputRequestResend, SDKConnectionSettings = _b.SDKConnectionSettings, InputOrControlResponse = _b.InputOrControlResponse, AuthenticationErrorCode = _b.AuthenticationErrorCode, JsonDataProcessingResult = _b.JsonDataProcessingResult, AuthenticationErrorRecovery = _b.AuthenticationErrorRecovery, AuthenticatorSelectionResult = _b.AuthenticatorSelectionResult, TargetBasedAuthenticatorInput = _b.TargetBasedAuthenticatorInput, TransportRequest = _b.TransportRequest, TransportResponse = _b.TransportResponse;
exports.LogLevel = LogLevel;
exports.FormInput = FormInput;
exports.OtpChannel = OtpChannel;
exports.RedirectType = RedirectType;
exports.RedirectInput = RedirectInput;
exports.PasswordInput = PasswordInput;
exports.ControlRequest = ControlRequest;
exports.ConfirmationInput = ConfirmationInput;
exports.ControlRequestType = ControlRequestType;
exports.AuthenticationError = AuthenticationError;
exports.UIAssetsDownloadMode = UIAssetsDownloadMode;
exports.RedirectResponseType = RedirectResponseType;
exports.OtpInputOtpSubmission = OtpInputOtpSubmission;
exports.OtpInputRequestResend = OtpInputRequestResend;
exports.SDKConnectionSettings = SDKConnectionSettings;
exports.InputOrControlResponse = InputOrControlResponse;
exports.AuthenticationErrorCode = AuthenticationErrorCode;
exports.JsonDataProcessingResult = JsonDataProcessingResult;
exports.AuthenticationErrorRecovery = AuthenticationErrorRecovery;
exports.AuthenticatorSelectionResult = AuthenticatorSelectionResult;
exports.TargetBasedAuthenticatorInput = TargetBasedAuthenticatorInput;
exports.TransportRequest = TransportRequest;
exports.TransportResponse = TransportResponse;
//# sourceMappingURL=sdk.js.map
"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var headers_1 = require("./headers");
var reducer_1 = require("./reducer");
var sdk_1 = require("./sdk");
var whisper_1 = __importDefault(require("./whisper"));
function usesSession(_a) {
    var _this = this;
    var dispatch = _a.dispatch, getDeviceData = _a.getDeviceData, _b = _a.isRehydrating, isRehydrating = _b === void 0 ? false : _b;
    return function (func) { return function (input) {
        var rest = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            rest[_i - 1] = arguments[_i];
        }
        return __awaiter(_this, void 0, void 0, function () {
            var deviceData, onScreenUpdate, onScreenError, onScreenLoading, onScreenRedirect, first, transmitAuthPayload, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!isRehydrating) {
                            // This adds loading spinner to entry page. We don't want rehydration to trigger that
                            dispatch({ type: reducer_1.TransmitActionType.HandleRequest });
                        }
                        return [4 /*yield*/, getDeviceData()];
                    case 1:
                        deviceData = _a.sent();
                        if (!deviceData) {
                            whisper_1.default.error({
                                message: 'Failed to fetch device data. Unable to start Transmit policy.',
                            });
                            return [2 /*return*/, null];
                        }
                        onScreenUpdate = function (payload) {
                            dispatch({
                                type: reducer_1.TransmitActionType.UpdateScreen,
                                payload: payload,
                            });
                        };
                        onScreenError = function (payload) {
                            dispatch({
                                type: reducer_1.TransmitActionType.UpdateScreenError,
                                payload: {
                                    payload: payload,
                                },
                            });
                        };
                        onScreenLoading = function () {
                            dispatch({ type: reducer_1.TransmitActionType.HandleRequest });
                        };
                        onScreenRedirect = function () {
                            dispatch({ type: reducer_1.TransmitActionType.HandleRedirect });
                        };
                        first = __assign(__assign({}, (input || {})), { onScreenUpdate: onScreenUpdate,
                            onScreenLoading: onScreenLoading,
                            onScreenError: onScreenError,
                            onScreenRedirect: onScreenRedirect,
                            deviceData: deviceData });
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, func.apply(void 0, __spread([first], rest))];
                    case 3:
                        transmitAuthPayload = _a.sent();
                        switch (transmitAuthPayload === null || transmitAuthPayload === void 0 ? void 0 : transmitAuthPayload.type) {
                            case 'session':
                                dispatch({
                                    type: reducer_1.TransmitActionType.AuthenticateUser,
                                    payload: transmitAuthPayload.session,
                                });
                                break;
                            case 'ticket':
                                dispatch({
                                    type: reducer_1.TransmitActionType.AuthenticateUserWithTicket,
                                    payload: transmitAuthPayload.ticketID,
                                });
                                break;
                            case 'legacySession':
                                dispatch({
                                    type: reducer_1.TransmitActionType.CreateLegacySession,
                                    payload: transmitAuthPayload.legacySession,
                                });
                                break;
                            case 'cenlarSSO':
                                dispatch({
                                    type: reducer_1.TransmitActionType.CreateCenlarSSO,
                                    payload: transmitAuthPayload.cenlarSSO,
                                });
                                break;
                            default:
                                dispatch({
                                    type: reducer_1.TransmitActionType.ResetStatus,
                                });
                                break;
                        }
                        return [2 /*return*/, transmitAuthPayload];
                    case 4:
                        error_1 = _a.sent();
                        if (!(error_1 instanceof sdk_1.AuthenticationError)) {
                            whisper_1.default.error({
                                message: "Error occurred: " + { error: error_1 } + ". Navigating to error screen.",
                            });
                            dispatch({ type: reducer_1.TransmitActionType.HandleFailure });
                            return [2 /*return*/, null];
                        }
                        switch (error_1.getErrorCode()) {
                            case sdk_1.AuthenticationErrorCode.ControlFlowExpired:
                                whisper_1.default.error({ message: 'Control flow expired.' });
                                onScreenUpdate({
                                    type: 'TemporarilyUnavailable',
                                    payload: {
                                        error: '',
                                        errorMode: 'AuthSessionExpired',
                                    },
                                });
                                return [2 /*return*/, null];
                            case sdk_1.AuthenticationErrorCode.PolicyRejection:
                                whisper_1.default.error({
                                    message: "Control flow ended with policy rejection: " + error_1.getMessage(),
                                });
                                // we don't want to show a screen here... its already handled in handlePolicyRejection method
                                return [2 /*return*/, null];
                            case sdk_1.AuthenticationErrorCode.UserCanceled:
                                return [2 /*return*/, null
                                    /**
                                     * Since we cannot enable the feature that returns the DeviceNotFound error code due to user
                                     * harvesting concerns, we are going to aggressively clear the users LS item when we receive
                                     * this error code from Transmit.
                                     */
                                ];
                            /**
                             * Since we cannot enable the feature that returns the DeviceNotFound error code due to user
                             * harvesting concerns, we are going to aggressively clear the users LS item when we receive
                             * this error code from Transmit.
                             */
                            case sdk_1.AuthenticationErrorCode.Internal:
                                window.localStorage.removeItem('users');
                                onScreenUpdate({
                                    type: 'Password',
                                    payload: {
                                        status: 'None',
                                        error: 'AuthErrorInternal',
                                    },
                                });
                                return [2 /*return*/, null];
                            default:
                                whisper_1.default.error({
                                    message: "Ending with Transmit Error: " + error_1.getMessage(),
                                });
                                dispatch({ type: reducer_1.TransmitActionType.HandleFailure });
                                return [2 /*return*/, null];
                        }
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    }; };
}
/**
 * A react hook that will keep state of the current Transmit session and the
 * currently rendered transmit screen. Exposes a wrapped set of transmit actions
 * that will update the various states internally.
 */
function usesTransmit(actions) {
    return function useTransmit() {
        var _this = this;
        var _a = __read(react_1.useReducer(reducer_1.AuthenticationSessionReducer, reducer_1.initialState), 2), _b = _a[0], activeScreen = _b.activeScreen, sessionStatus = _b.sessionStatus, deviceData = _b.deviceData, dispatch = _a[1];
        var _c = __read(react_1.useState(null), 2), ticketID = _c[0], setTicketID = _c[1];
        var clearTicketID = function () { return setTicketID(null); };
        var _d = __read(react_1.useState({}), 2), featureFlags = _d[0], setFeatureFlags = _d[1];
        var getDeviceData = react_1.useCallback(function () { return __awaiter(_this, void 0, void 0, function () {
            var result, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (deviceData) {
                            return [2 /*return*/, deviceData];
                        }
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, headers_1.getAllDeviceData()];
                    case 2:
                        result = _b.sent();
                        dispatch({ type: reducer_1.TransmitActionType.SetDeviceData, payload: result });
                        return [2 /*return*/, result];
                    case 3:
                        _a = _b.sent();
                        whisper_1.default.error({
                            message: 'Failed to retrieve device data. Check vendor files or HDM service.',
                        });
                        dispatch({ type: reducer_1.TransmitActionType.HandleFailure });
                        return [2 /*return*/, null];
                    case 4: return [2 /*return*/];
                }
            });
        }); }, [deviceData, dispatch]);
        var updatesSession = react_1.useCallback(usesSession({
            dispatch: dispatch,
            getDeviceData: getDeviceData,
        }), [dispatch, getDeviceData]);
        var withFeatureFlags = function (action) {
            return function (args) {
                return action(__assign(__assign({}, args), { featureFlags: featureFlags }));
            };
        };
        var navigateToLogin = function (_a) {
            var _b = _a === void 0 ? {} : _a, _c = _b.username, username = _c === void 0 ? '' : _c;
            return dispatch({
                type: reducer_1.TransmitActionType.NavigateToEntryScreen,
                payload: {
                    entryScreenType: 'Password',
                    entryScreenPayload: {
                        username: username,
                    },
                },
            });
        };
        var navigateToForgotPassword = function (_a) {
            var _b = _a === void 0 ? {} : _a, _c = _b.username, username = _c === void 0 ? '' : _c;
            return dispatch({
                type: reducer_1.TransmitActionType.NavigateToEntryScreen,
                payload: {
                    entryScreenType: 'ForgotPassword',
                    entryScreenPayload: {
                        username: username,
                    },
                },
            });
        };
        var navigateToForgotUsername = function () {
            dispatch({
                type: reducer_1.TransmitActionType.NavigateToEntryScreen,
                payload: { entryScreenType: 'ForgotUsername' },
            });
        };
        var navigateToResetPassword = function () {
            return dispatch({
                type: reducer_1.TransmitActionType.NavigateToEntryScreen,
                payload: { entryScreenType: 'ResetPassword' },
            });
        };
        var navigateToOleCustomer = function () {
            return dispatch({
                type: reducer_1.TransmitActionType.NavigateToEntryScreen,
                payload: { entryScreenType: 'Enroll' },
            });
        };
        var navigateToGuestOle = function () {
            return dispatch({
                type: reducer_1.TransmitActionType.NavigateToEntryScreen,
                payload: { entryScreenType: 'GuestCustomerInfo' },
            });
        };
        whisper_1.default.trace({
            message: ['Transmit hook update', { activeScreen: activeScreen }],
        });
        var rehydrate = react_1.useCallback(function (input) { return __awaiter(_this, void 0, void 0, function () {
            var transmitAuthPayload;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        dispatch({ type: reducer_1.TransmitActionType.StartRehydration });
                        return [4 /*yield*/, usesSession({
                                dispatch: dispatch,
                                getDeviceData: getDeviceData,
                                isRehydrating: true,
                            })(withFeatureFlags(actions.rehydrate))(__assign(__assign({}, input), { setTicketID: setTicketID }))];
                    case 1:
                        transmitAuthPayload = _a.sent();
                        if (!transmitAuthPayload) {
                            dispatch({ type: reducer_1.TransmitActionType.RehydrationFailure });
                        }
                        return [2 /*return*/, transmitAuthPayload];
                }
            });
        }); }, [dispatch, getDeviceData]);
        var refreshAccessToken = react_1.useCallback(function (input) { return __awaiter(_this, void 0, void 0, function () {
            var transmitAuthPayload;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, usesSession({
                            dispatch: dispatch,
                            getDeviceData: getDeviceData,
                        })(withFeatureFlags(actions.refreshAccessToken))(input)];
                    case 1:
                        transmitAuthPayload = _a.sent();
                        if (!transmitAuthPayload) {
                            dispatch({ type: reducer_1.TransmitActionType.HandleFailure });
                        }
                        return [2 /*return*/, transmitAuthPayload];
                }
            });
        }); }, [dispatch, getDeviceData]);
        return {
            activeScreen: activeScreen,
            sessionStatus: sessionStatus,
            navigateToLogin: navigateToLogin,
            navigateToForgotUsername: navigateToForgotUsername,
            navigateToForgotPassword: navigateToForgotPassword,
            navigateToResetPassword: navigateToResetPassword,
            navigateToOleCustomer: navigateToOleCustomer,
            navigateToGuestOle: navigateToGuestOle,
            actions: {
                logout: updatesSession(withFeatureFlags(actions.logout)),
                rehydrate: rehydrate,
                loginWithUser: updatesSession(withFeatureFlags(actions.loginWithUser)),
                forgotUsername: updatesSession(withFeatureFlags(actions.forgotUsername)),
                forgotPassword: updatesSession(withFeatureFlags(actions.forgotPassword)),
                recoverPassword: updatesSession(withFeatureFlags(actions.recoverPassword)),
                transferTicket: updatesSession(withFeatureFlags(actions.transferTicket)),
                refreshAccessToken: refreshAccessToken,
                legacySession: updatesSession(withFeatureFlags(actions.legacySession)),
                cenlarSso: updatesSession(withFeatureFlags(actions.cenlarSso)),
                onlineEnrollment: updatesSession(withFeatureFlags(actions.onlineEnrollment)),
                enrollGuest: updatesSession(withFeatureFlags(actions.enrollGuest)),
                addAllyId: updatesSession(withFeatureFlags(actions.addAllyId)),
                getSecurityProfile: updatesSession(withFeatureFlags(actions.getSecurityProfile)),
                updateSecurityProfile: updatesSession(withFeatureFlags(actions.updateSecurityProfile)),
                updateUsername: updatesSession(withFeatureFlags(actions.updateUsername)),
                getAtomicAccessToken: updatesSession(withFeatureFlags(actions.getAtomicAccessToken)),
                enrollCreatorCache: updatesSession(withFeatureFlags(actions.enrollCreatorCache)),
                loginCreatorCache: updatesSession(withFeatureFlags(actions.loginCreatorCache)),
                recoverPasswordCreatorCache: updatesSession(withFeatureFlags(actions.recoverPasswordCreatorCache)),
            },
            ticketID: ticketID,
            clearTicketID: clearTicketID,
            setFeatureFlags: setFeatureFlags,
        };
    };
}
exports.default = (function (actions) { return ({
    useTransmit: usesTransmit(actions),
}); });
//# sourceMappingURL=hooks.js.map
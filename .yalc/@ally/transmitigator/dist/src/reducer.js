"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationSessionReducer = exports.initialState = exports.TransmitActionType = void 0;
var masquerade_1 = require("@ally/masquerade");
var which_env_1 = __importDefault(require("@ally/which-env"));
var whisper_1 = __importDefault(require("./whisper"));
var TransmitActionType;
(function (TransmitActionType) {
    TransmitActionType[TransmitActionType["UpdateScreen"] = 0] = "UpdateScreen";
    TransmitActionType[TransmitActionType["UpdateScreenError"] = 1] = "UpdateScreenError";
    TransmitActionType[TransmitActionType["AuthenticateUser"] = 2] = "AuthenticateUser";
    TransmitActionType[TransmitActionType["NavigateToEntryScreen"] = 3] = "NavigateToEntryScreen";
    TransmitActionType[TransmitActionType["StartRehydration"] = 4] = "StartRehydration";
    TransmitActionType[TransmitActionType["RehydrationFailure"] = 5] = "RehydrationFailure";
    TransmitActionType[TransmitActionType["SetDeviceData"] = 6] = "SetDeviceData";
    TransmitActionType[TransmitActionType["HandleRequest"] = 7] = "HandleRequest";
    TransmitActionType[TransmitActionType["HandleFailure"] = 8] = "HandleFailure";
    TransmitActionType[TransmitActionType["StartPolicy"] = 9] = "StartPolicy";
    TransmitActionType[TransmitActionType["HandleRedirect"] = 10] = "HandleRedirect";
    TransmitActionType[TransmitActionType["AuthenticateUserWithTicket"] = 11] = "AuthenticateUserWithTicket";
    TransmitActionType[TransmitActionType["CreateLegacySession"] = 12] = "CreateLegacySession";
    TransmitActionType[TransmitActionType["CreateCenlarSSO"] = 13] = "CreateCenlarSSO";
    TransmitActionType[TransmitActionType["ResetStatus"] = 14] = "ResetStatus";
})(TransmitActionType = exports.TransmitActionType || (exports.TransmitActionType = {}));
var accountForForgotPasswordErrors = function (_a) {
    var oldScreen = _a.oldScreen, newScreen = _a.newScreen;
    if (newScreen.type === 'ForgotPassword' &&
        oldScreen.type === 'ResetPassword') {
        return __assign(__assign({}, newScreen), { type: oldScreen.type });
    }
    return newScreen;
};
var scrub = masquerade_1.scrubber();
var env = which_env_1.default(window.location.href);
exports.initialState = {
    activeScreen: { type: 'Unauthenticated' },
    sessionStatus: { status: 'Unauthenticated' },
    deviceData: null,
};
var AuthenticationSessionReducer = function (state, action) {
    var actionType = '';
    var newState = state;
    switch (action.type) {
        case TransmitActionType.UpdateScreen:
            actionType = 'UpdateScreen';
            newState = __assign(__assign({}, state), { activeScreen: accountForForgotPasswordErrors({
                    oldScreen: state.activeScreen,
                    newScreen: action.payload,
                }), sessionStatus: { status: 'Authenticating' } });
            break;
        case TransmitActionType.UpdateScreenError:
            actionType = 'UpdateScreenError';
            newState = __assign(__assign({}, state), { activeScreen: __assign(__assign({}, state.activeScreen), { payload: __assign(__assign({}, state.activeScreen.payload), action.payload.payload) }) });
            break;
        case TransmitActionType.AuthenticateUser:
            actionType = 'AuthenticateUser';
            newState = __assign(__assign({}, state), { activeScreen: {
                    type: 'Authenticated',
                    payload: action.payload,
                }, sessionStatus: {
                    status: 'Authenticated',
                    data: action.payload,
                } });
            break;
        case TransmitActionType.AuthenticateUserWithTicket:
            actionType = 'AuthenticateUserWithTicket';
            newState = __assign(__assign({}, state), { activeScreen: {
                    type: 'AuthenticatedWithTicket',
                    payload: action.payload,
                }, sessionStatus: {
                    status: 'AuthenticatedWithTicket',
                    ticketID: action.payload,
                } });
            break;
        case TransmitActionType.CreateLegacySession:
            actionType = 'CreateLegacySession';
            newState = __assign(__assign({}, state), { sessionStatus: {
                    status: 'LegacyAuthenticated',
                    data: action.payload,
                } });
            break;
        case TransmitActionType.CreateCenlarSSO:
            actionType = 'CreateCenlarSSO';
            newState = __assign(__assign({}, state), { sessionStatus: {
                    status: 'CenlarSSOAuthenticated',
                    data: action.payload,
                } });
            break;
        case TransmitActionType.StartRehydration:
            actionType = 'StartRehydration';
            newState = __assign(__assign({}, state), { sessionStatus: { status: 'Rehydrating' } });
            break;
        case TransmitActionType.HandleRedirect:
            return __assign(__assign({}, state), { sessionStatus: { status: 'Redirecting' } });
        case TransmitActionType.RehydrationFailure: {
            actionType = 'RehydrationFailure';
            newState = __assign(__assign({}, state), { sessionStatus: { status: 'RehydrationFailed' } });
            break;
        }
        case TransmitActionType.HandleRequest: {
            actionType = 'HandleRequest';
            var currentScreen = state.activeScreen;
            /**
             * This is treated as an opt-out to having the payload status updated to "Loading"
             */
            switch (currentScreen.type) {
                case 'Unauthenticated':
                case 'Authenticated':
                case 'TemporarilyUnavailable':
                case 'MFASuppressionNoOptions':
                case 'AuthenticatedWithTicket':
                    newState = state;
                    break;
                default:
                    newState = __assign(__assign({}, state), { activeScreen: {
                            type: currentScreen.type,
                            payload: __assign(__assign({}, currentScreen.payload), { status: 'Loading' }),
                        } });
            }
            break;
        }
        case TransmitActionType.NavigateToEntryScreen: {
            actionType = 'NavigateToEntryScreen';
            var oldScreenType = state.activeScreen.type;
            var newScreenType = action.payload.entryScreenType;
            var newScreenPayload = action.payload.entryScreenPayload || {};
            if (oldScreenType === newScreenType ||
                state.sessionStatus.status === 'Redirecting' ||
                oldScreenType === 'Authenticated') {
                newState = state;
                break;
            }
            return __assign(__assign({}, state), { activeScreen: {
                    type: newScreenType,
                    payload: __assign({ status: 'None' }, newScreenPayload),
                } });
        }
        case TransmitActionType.SetDeviceData:
            actionType = 'SetDeviceData';
            newState = __assign(__assign({}, state), { deviceData: action.payload });
            break;
        case TransmitActionType.HandleFailure:
            actionType = 'HandleFailure';
            newState = __assign(__assign({}, state), { activeScreen: {
                    type: 'TemporarilyUnavailable',
                    payload: { error: '', errorMode: 'Default' },
                }, sessionStatus: { status: 'AuthenticationFailed' } });
            break;
        case TransmitActionType.ResetStatus:
            actionType = 'ResetStatus';
            // Resets payload status to 'None' from 'Loading'. This is needed to let the UI
            // know that a dispatched action has completed and not returned one of the expected
            // payload types
            newState = __assign(__assign({}, state), { activeScreen: __assign(__assign({}, state.activeScreen), { payload: __assign(__assign({}, state.activeScreen.payload), { status: 'None' }) }) });
            break;
        default:
            actionType = 'default';
    }
    whisper_1.default.debug({
        message: [
            "AuthenticationSessionReducer: action type " + actionType,
            env.isProd ? scrub(newState) : newState,
        ],
    });
    return newState;
};
exports.AuthenticationSessionReducer = AuthenticationSessionReducer;
//# sourceMappingURL=reducer.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionEvent = void 0;
// eslint-disable-next-line import/no-unresolved
require("../vendor/com.ts.mobile.sdk");
var ActionEvent;
(function (ActionEvent) {
    ActionEvent["login"] = "login";
    ActionEvent["register"] = "register";
    ActionEvent["password_reset"] = "password_reset";
})(ActionEvent = exports.ActionEvent || (exports.ActionEvent = {}));
//# sourceMappingURL=types.js.map
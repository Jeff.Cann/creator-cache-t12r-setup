import '../vendor/com.ts.mobile.sdk';
import { AsyncStatus, ErrorDispatcher, JourneyDispatcher } from './AllyTransmitUIHandler';
import { DEFAULT_POLICY_NAMES, SESSION_KEY_MAP } from './constants';
import { DeviceData } from './headers';
export declare type StringEnumValues<TEnum extends string> = `${TEnum}`;
interface ActionToken {
    actionToken: string;
}
export declare enum ActionEvent {
    login = "login",
    register = "register",
    password_reset = "password_reset"
}
export interface NewRiskID {
    init: () => void;
    triggerActionEvent: (event: ActionEvent) => Promise<ActionToken>;
    setUser: (username: string) => Promise<boolean>;
    clearUser: () => Promise<boolean>;
}
declare global {
    interface Window {
        myRiskID: NewRiskID;
        RiskID: {
            new (...args: string[]): NewRiskID;
        };
        com: {
            ts: {
                mobile: {
                    sdk: typeof TransmitSDK;
                };
            };
        };
        xmsdk: {
            XmSdk: () => TransmitSDK.TransmitSDKXm;
        };
        nsp?: {
            cfn: () => string;
            afn: (arg1: any, arg2: string[]) => void;
        };
    }
}
export declare type FMap = Record<string, AnyFunction>;
export declare type Tail<Args extends unknown[]> = ((...args: Args) => unknown) extends (_: any, ...rest: infer T) => unknown ? T : [];
export declare type ResolvedValue<P> = P extends Promise<infer R> ? R : unknown;
export declare type PolicyName = keyof typeof DEFAULT_POLICY_NAMES;
export interface DispatchUIHandler {
    new (config: Options, onScreenUpdate: JourneyDispatcher, onScreenError: ErrorDispatcher, onScreenLoading: () => void, onScreenRedirect: () => void): TransmitSDK.UIHandler;
}
export interface Application {
    id: string;
    name: string;
    version: string;
    apigeeApiKey: string;
}
interface ClientInfo {
    name: string;
    location: string;
    version: string;
}
export interface ApplicationParams {
    applicationId: string;
    applicationName: string;
    applicationVersion: string;
    'api-key': string;
    client: ClientInfo;
}
export interface Options {
    app: string;
    myRiskID: NewRiskID;
    sdk: TransmitSDK.TransmitSDKXm;
    server: string;
    policies: Record<PolicyName, string>;
    logLevel: TransmitSDK.LogLevel;
    UIHandler: DispatchUIHandler;
    application: Application;
    loginParams?: Record<string, any>;
    allowCrossTabSession?: boolean;
}
export interface TransmitigatorOptions extends Partial<Omit<Options, 'policies'>> {
    policies?: Partial<Options['policies']>;
}
export interface Setup {
    sdk: TransmitSDK.TransmitSDKXm;
    activeAction: Promise<unknown>;
    sdkInitPromise: Promise<boolean>;
}
export interface Config extends Options, Setup {
}
export interface DeliveryMethodData {
    deliveryMethodId: number;
    deliveryMethodType: 'SMS' | 'EMAIL' | '';
    deliveryMethodValuePvtBlock: string;
}
export interface MFA {
    mfaSuppress?: boolean;
    mfaDeliveryMethods: DeliveryMethodData[];
    rsaStatusIndicator: string;
}
export interface AllyUserRole {
    guest: boolean;
    creditcard: boolean;
    auto: boolean;
    bank: boolean;
    mortgage: boolean;
    investment: boolean;
    wealth: boolean;
}
export interface AnnualVerification {
    version: string;
    finalVerificationDate: string | null;
    annualVerificationDate: string | null;
    annualVerificationBypass: boolean;
    annualVerificationStatus: 'VERIFIED' | 'NON_VERIFIED' | 'NOT_VERIFIED_PROMPT';
}
export declare type RelationshipStatusTypes = 'ACTIVE' | 'FUNDED' | 'NOT_FUNDED' | 'INACTIVE' | 'PAID_OFF' | 'RESTRICTED' | 'PROSPECT' | 'CLIENT_UNFUNDED' | 'CLIENT_FUNDED' | 'NOT_PROSPECT' | 'TARGET' | 'IN_CONSULTATION' | 'CLOSED' | 'CLOSED_GREATER_THAN_1YR' | 'RESTRICTED_GREATER_THAN_1YR' | 'NOT_INTERESTED' | 'PAIDOFF';
export declare type RelationshipStatus = {
    indicator: 'YES' | 'NO';
    status: RelationshipStatusTypes;
};
export declare type Relationships = {
    invest?: {
        selfDirected?: RelationshipStatus;
        roboPortfolios?: RelationshipStatus;
        wealth?: RelationshipStatus;
    };
    mortgage?: {
        servicing?: RelationshipStatus;
    };
    lapi?: {
        creditcard?: RelationshipStatus;
    };
    tools?: {
        debtReduction?: RelationshipStatus;
    };
    auto?: RelationshipStatus;
    deposits?: RelationshipStatus;
};
export declare type MappedSessionKeys = Record<StringEnumValues<SESSION_KEY_MAP>, any>;
/**
 * Set any properties that are required or have nested data structures
 */
interface RequiredSessionKeys {
    access_token: string;
    relationships: Relationships;
    userNamePvtEncrypt: string;
    lastLoginTime: string | null;
    rememberMeToken: string | null;
    profileVerification: AnnualVerification;
    allyUserRole: AllyUserRole;
    resourceIndex: string;
    deviceData: DeviceData;
    scope: string;
}
/**
 * Assume the rest of the keys returned by Transmit are optional by default
 */
declare type OptionalSessionKeys = Partial<Omit<MappedSessionKeys, keyof RequiredSessionKeys>>;
/**
 * The following properties are no longer supported in Transmit AFG
 * @deprecated
 */
interface DeprecatedSessionKeys {
    mfa?: MFA;
    mxEnrolled?: boolean;
    jwtToken?: string;
    passwordHintPvtBlock?: string;
    AwsAccessToken?: string;
    CSRFChallengeToken?: string;
    legacyChatToken?: string;
}
/**
 * Unmapped / value pairs sent from Transmit. These should be ignored by front-end consumers.
 */
declare type UnmappedSessionKeys = Record<string, any>;
export declare type Session = RequiredSessionKeys & OptionalSessionKeys & DeprecatedSessionKeys & UnmappedSessionKeys;
export interface LegacySession {
    CSRFChallengeToken: string;
    AwsAccessToken: string;
    legacyChatToken: string;
}
export interface CenlarSSOAuthenticated {
    redirect_url: string;
}
export interface MFADeliveryMethod {
    deliveryMethodType: string;
    deliveryMethodValuePvtBlock: string;
}
export interface SecurityProfile {
    passwordHint: string;
    alwaysChallenge: {
        web: boolean;
        mobile: boolean;
    };
    isBiometricEnrolled: boolean;
    mfaDeliveryMethods: MFADeliveryMethod[];
}
export interface TransmitResponse {
    authenticationResult: TransmitSDK.AuthenticationResult;
    deviceData: DeviceData;
    ignoreTabRegistration?: boolean;
    journey_version: string;
    token_type?: string;
}
export interface ScreenErrorPayload {
    status: AsyncStatus;
    error: string | Error | undefined;
}
export declare type Actions<A extends Record<string, AnyFunction>> = {
    [K in keyof A]: (...args: Tail<Parameters<A[K]>>) => ReturnType<A[K]>;
};
export declare type Destination = 'bank_ember' | 'bank_react' | 'invest' | 'wealth' | 'auto' | 'mortgage';
export declare type AnyFunction = (...args: any[]) => any;
export interface PhysicalAddress {
    street1: string;
    street2: string;
    city: string;
    state: string;
    postalCode: string;
}
export interface UserData {
    email: string;
    homePhone: string;
    mobilePhone: string;
    workPhone: string;
    physicalAddress: PhysicalAddress;
    alertEmail: string;
    alertPhone: string;
}
export interface LDFlagSet {
    [key: string]: any;
}
export interface ActionFeatureFlagOptions {
    featureFlags?: LDFlagSet;
    featureFlagsOverride?: LDFlagSet;
}
export {};

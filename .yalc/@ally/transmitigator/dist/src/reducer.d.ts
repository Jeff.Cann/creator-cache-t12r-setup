import { ScreenPayload } from './AllyTransmitUIHandler';
import { Session, LegacySession, CenlarSSOAuthenticated } from './types';
import { DeviceData } from './headers';
export declare type SessionStatus = {
    status: 'Authenticated';
    data: Session;
} | {
    status: 'LegacyAuthenticated';
    data: LegacySession;
} | {
    status: 'CenlarSSOAuthenticated';
    data: CenlarSSOAuthenticated;
} | {
    status: 'AuthenticatedWithTicket';
    ticketID: string;
} | {
    status: 'Unauthenticated';
} | {
    status: 'Rehydrating';
} | {
    status: 'Redirecting';
} | {
    status: 'RehydrationFailed';
} | {
    status: 'AuthenticationFailed';
} | {
    status: 'Authenticating';
};
export interface AuthenticationSessionState {
    activeScreen: ScreenPayload;
    sessionStatus: SessionStatus;
    deviceData: DeviceData | null;
}
export declare enum TransmitActionType {
    UpdateScreen = 0,
    UpdateScreenError = 1,
    AuthenticateUser = 2,
    NavigateToEntryScreen = 3,
    StartRehydration = 4,
    RehydrationFailure = 5,
    SetDeviceData = 6,
    HandleRequest = 7,
    HandleFailure = 8,
    StartPolicy = 9,
    HandleRedirect = 10,
    AuthenticateUserWithTicket = 11,
    CreateLegacySession = 12,
    CreateCenlarSSO = 13,
    ResetStatus = 14
}
export declare type TransmitAction = {
    type: TransmitActionType.UpdateScreen;
    payload: ScreenPayload;
} | {
    type: TransmitActionType.UpdateScreenError;
    payload: Partial<ScreenPayload>;
} | {
    type: TransmitActionType.AuthenticateUser;
    payload: Session;
} | {
    type: TransmitActionType.CreateLegacySession;
    payload: LegacySession;
} | {
    type: TransmitActionType.CreateCenlarSSO;
    payload: CenlarSSOAuthenticated;
} | {
    type: TransmitActionType.AuthenticateUserWithTicket;
    payload: string;
} | {
    type: TransmitActionType.NavigateToEntryScreen;
    payload: NavigateToEntryScreenPayload;
} | {
    type: TransmitActionType.SetDeviceData;
    payload: DeviceData;
} | {
    type: TransmitActionType.StartRehydration | TransmitActionType.RehydrationFailure | TransmitActionType.HandleFailure | TransmitActionType.HandleRequest | TransmitActionType.HandleRedirect | TransmitActionType.ResetStatus;
};
declare type EntryScreenType = 'ForgotUsername' | 'ForgotPassword' | 'ResetPassword' | 'Password' | 'Enroll' | 'GuestCustomerInfo';
interface EntryScreenPayload {
    username: string;
}
interface NavigateToEntryScreenPayload {
    entryScreenType: EntryScreenType;
    entryScreenPayload?: EntryScreenPayload;
}
export declare const initialState: AuthenticationSessionState;
export declare const AuthenticationSessionReducer: (state: AuthenticationSessionState, action: TransmitAction) => AuthenticationSessionState;
export {};

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllyTransmitUIHandler = exports.InformationScreenType = void 0;
/* eslint-disable @typescript-eslint/ban-types */
var jwt_decode_1 = __importDefault(require("jwt-decode"));
var authenticators_1 = require("./authenticators");
var constants_1 = require("./constants");
var sdk_1 = require("./sdk");
var utils_1 = require("./utils");
var whisper_1 = __importDefault(require("./whisper"));
var forms_1 = require("./forms");
var BaseUIHandler_1 = require("./BaseUIHandler");
var InformationScreenType;
(function (InformationScreenType) {
    InformationScreenType["OBSA"] = "OBSA";
    InformationScreenType["ESign"] = "ESign";
    InformationScreenType["PasswordUpdated"] = "PasswordUpdated";
    InformationScreenType["SetupWelcome"] = "SetupWelcome";
})(InformationScreenType = exports.InformationScreenType || (exports.InformationScreenType = {}));
var AllyTransmitUIHandler = /** @class */ (function (_super) {
    __extends(AllyTransmitUIHandler, _super);
    function AllyTransmitUIHandler(options, onScreenUpdate, onScreenError, onScreenLoading, onScreenRedirecting) {
        var _this = _super.call(this) || this;
        _this.endActivityIndicator = utils_1.noop;
        _this.startActivityIndicator = utils_1.noop;
        _this.options = options;
        _this.onScreenUpdate = onScreenUpdate;
        _this.onScreenError = onScreenError;
        _this.onScreenLoading = onScreenLoading;
        _this.onScreenRedirecting = onScreenRedirecting;
        return _this;
    }
    AllyTransmitUIHandler.prototype.controlFlowStarting = function (clientContext) {
        _super.prototype.controlFlowStarting.call(this, clientContext);
        this.currentClientContext = this.currentClientContext
            ? // this handles policy redirects
             __assign(__assign({}, this.currentClientContext), clientContext) : clientContext;
    };
    /**
     * No-op placeholder to override the default Transmit method
     */
    AllyTransmitUIHandler.prototype.selectAuthenticator = function (_authenticatorArray) {
        return new Promise(function (_resolve, _reject) {
            whisper_1.default.trace({ message: 'selectAuthenticator no-op' });
        });
    };
    /**
     * Triggers the `Password` authenticator, creating a set of lifecycle hooks
     * that will be called to get the user's password
     */
    AllyTransmitUIHandler.prototype.createPasswordAuthSession = function () {
        return new authenticators_1.PasswordAuthenticatorHandler(this.onScreenUpdate, this.onScreenLoading);
    };
    /**
     * Handle policy redirection. This callback is invoked in response to the
     * policy asking to redirect to another policy with a possibly different user
     * Id.
     *
     * Redirection target policy has a RedirectType indicating which SDK API
     * function is used for starting the next policy. RedirectTypeBind redirects
     * to Id-less device bind policy, having null policyId value. For
     * authentication policy or general policy, RedirectTypeAuthenticate or
     * RedirectTypeInvokePolicy with policyId will be provided, respectively.
     *
     * RedirectTypeBindOrAuthenticate allows the client to decide which type to
     * trigger according to local bind state of the SDK.
     *
     * The client context object passed to the invocation which triggered the
     * redirect (which is the one passed to this callback) will continue to
     * be in effect for the redirected policy.
     *
     * @param redirectType Policy redirect type for next policy invocation.
     * @param policyId  Optional policy Id
     * @param userId Current or different user handle (e.g. userId, idToken) to
     * use in next policy invocation. Null value when redirected to anonymous
     * policy.
     * @param additionalParameters A JSON with additional parameters to be sent
     * for next policy evaluation.
     * @param clientContext The clientContext for the SDK operation invocation
     * for which this callback is invoked.
     *
     * @return A promise object. On successful result, returns: A RedirectInput
     * object encoding client choice.
     */
    AllyTransmitUIHandler.prototype.handlePolicyRedirect = function (_redirectType, _policy, userID, _params, clientContext) {
        this.onScreenRedirecting();
        // eslint-disable-next-line no-param-reassign
        this.currentClientContext = this.currentClientContext
            ? __assign(__assign({}, this.currentClientContext), { username: userID }) : { username: userID };
        return Promise.resolve(sdk_1.RedirectInput.create(sdk_1.RedirectResponseType.RedirectToPolicy));
    };
    /**
     * Notify policy evaluation rejection. This callback is invoked in response to
     * the policy asking to reject the authentication flow (either using a Reject
     * action or rejection defined for other actions on failure).
     *
     * Implementations should present a rejection screen as defined in the
     * callback parameters to the user, and once policy evaluation may continue
     * (i.e the policy will abort and the SDK invocation will asynchronously
     * return) the implementation must return a ConfirmationInput object
     * asynchronously to the SDK with a user selection value of "-1".
     *
     * @param title Title of rejection screen to display.
     * @param text Text to display in rejection screen.
     * @param buttonText Label for a confirmation button (to dismiss the screen)
     * @param failureData Optional data which may be specified on the Transmit SP
     * Server for session rejection cases. Holds one JSON field named "value",
     * where it's value type may vary.
     * @param actionContext Information on the policy action currently executing,
     * within which this callback is invoked.
     * @param clientContext The clientContext for the SDK operation invocation for
     * which this callback is invoked.
     *
     * @return A promise object. On successful result, returns: A
     * ConfirmationInput object encoding user choice. For the
     * ConfirmationInput#userChoice property, set a value of "-1"
     */
    AllyTransmitUIHandler.prototype.handlePolicyRejection = function (title, _text, _buttonText, failureData, _actionContext, _clientContext) {
        var _this = this;
        var _a, _b;
        whisper_1.default.warn({ message: "Handling Policy Rejection: " + title });
        var policyID = (_b = (_a = failureData === null || failureData === void 0 ? void 0 : failureData.reason) === null || _a === void 0 ? void 0 : _a.data) === null || _b === void 0 ? void 0 : _b.POLICY_ID;
        // The set of policies in which we want to ignore all errors and redirect
        // the user to the login page. Note: `internalSessionRehydrate` is a policy
        // that's internal to Transmit and NOT the rehydration (discovery) policy.
        var policiesIgnoreErrors = [
            this.options.policies.discovery,
            this.options.policies.investComeback,
            this.options.policies.internalSessionRehydrate,
        ];
        /**
         * A white list of error codes that we want to handle even for the
         * DISCOVERY_SECURE policy. This allows us to handle displaying errors
         * coming from transmit during a discovery secure w/ a ticketID from
         * storefront.
         */
        var whitelistedDiscoveryErrorCodes = [
            'CIAM_E01_01_001',
            'CIAM_E01_01_002',
            'CIAM_E01_01_003',
            'CIAM_E01_01_004',
            'CIAM_E01_01_006',
            'CIAM_E01_01_008',
            'CIAM_E00_00_014',
            'CIAM_E01_01_016',
        ];
        if (policyID === this.options.policies.discovery &&
            whitelistedDiscoveryErrorCodes.includes(title || '')) {
            whisper_1.default.debug({
                message: this.options.policies.discovery + " policy rejected with " + title,
            });
        }
        else if (policiesIgnoreErrors.includes(policyID)) {
            return Promise.resolve(sdk_1.ConfirmationInput.create(-1));
        }
        return new Promise(function (resolve, _) {
            var _a, _b, _c;
            switch (title) {
                case 'CIAM_E01_01_003': // mobile code
                case 'CIAM_E01_01_002': // web code
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: {
                            error: title,
                            errorMode: 'Locked',
                        },
                    });
                    break;
                case 'CIAM_E01_01_001': // USER_NOT_FOUND
                case 'CIAM_E01_07_003': // AAOS_USER_DAO_REDIRECT_EXT
                case 'CIAM_E01_01_016': // USERNAME_CONTAINS_MASKED_VALUE
                    _this.onScreenUpdate({
                        type: 'Password',
                        payload: {
                            status: 'None',
                            error: title,
                        },
                    });
                    break;
                case 'CIAM_E01_01_010':
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: {
                            error: title,
                            errorMode: 'RSABlackList',
                        },
                    });
                    break;
                case 'CIAM_E08_01_002':
                    _this.onScreenUpdate({
                        type: 'ForgotPassword',
                        payload: { status: 'Error', error: 'CIAM_E08_01_002' },
                    });
                    break;
                case 'CIAM_E01_03_001':
                    _this.onScreenUpdate({
                        type: 'ForgotPassword',
                        payload: { status: 'Error', error: 'CIAM_E01_03_001' },
                    });
                    break;
                case 'CIAM_E08_07_001':
                    _this.onScreenUpdate({
                        type: 'ForgotPassword',
                        payload: { status: 'Error', error: 'CIAM_E08_07_001' },
                    });
                    break;
                case 'CIAM_E08_07_002':
                    _this.onScreenUpdate({
                        type: 'ForgotUsername',
                        payload: { status: 'Error', error: 'CIAM_E08_07_002' },
                    });
                    break;
                case 'CIAM_E01_03_014':
                    _this.onScreenUpdate({
                        type: 'ForgotUsername',
                        payload: { status: 'Error', error: 'CIAM_E01_03_014' },
                    });
                    break;
                case 'CIAM_E08_01_010':
                    _this.onScreenUpdate({
                        type: 'ForgotUsername',
                        payload: { status: 'Error', error: 'CIAM_E08_01_010' },
                    });
                    break;
                case 'CIAM_E08_00_006':
                    _this.onScreenUpdate({
                        type: 'ForgotUsername',
                        payload: { status: 'Error', error: 'CIAM_E08_00_006' },
                    });
                    break;
                case 'CIAM_E01_01_004':
                case 'CIAM_E01_01_006':
                case 'CIAM_E01_01_008':
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: {
                            error: title,
                            errorMode: 'BlockedDeceased',
                        },
                    });
                    break;
                case 'CIAM_E02_00_002':
                    _this.onScreenUpdate({
                        type: 'MFASuppressionNoOptions',
                    });
                    break;
                case 'CIAM_E02_00_001':
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: {
                            error: title,
                            errorMode: 'Straggler',
                        },
                    });
                    break;
                case 'CIAM_E01_03_002':
                case 'CIAM_E01_03_003':
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: {
                            error: title,
                            errorMode: 'Blocked',
                        },
                    });
                    break;
                case 'CIAM_E01_01_005':
                case 'CIAM_E02_00_003':
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: {
                            error: title,
                            errorMode: 'OTPIncorrect',
                        },
                    });
                    break;
                case 'CIAM_E01_03_004':
                case 'CIAM_E01_03_006':
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: {
                            error: title,
                            errorMode: 'AdditionalSecuritySetupRequired',
                        },
                    });
                    break;
                /* Begin OLE error states */
                case 'CIAM_E08_02_001': // USER_EXISTS_IN_LDAP
                case 'CIAM_E08_02_002': // MULTIPLE_RECORDS_IN_LDAP
                case 'CIAM_E08_03_001': // MULTIPLE_RECORDS_IN_ACM
                case 'CIAM_E08_03_002': // CUSTOMER_MISSING_IN_ACM
                case 'CIAM_E08_04_001': // CUSTOMER_MISSING
                case 'CIAM_E08_00_003': // USER_NOT_VERIFIED_FOR_ONLINE_ENROLLMENT
                case 'CIAM_E08_00_008': // FAILED_USER_CREATION_IN_LDAP
                case 'CIAM_E08_00_004': // INVALID_OTP_DELIVERY_OPTION
                case 'CIAM_E08_00_009': // MULTIPLE_OTP_DELIVERY_OPTIONS_SELECTED
                case 'CIAM_E08_00_010': // OTP_DELIVERY_OPTION_UNSELECTED
                case 'CIAM_E08_00_011': // INVALID_OTP_DEFINITION
                case 'CIAM_E02_00_004': // otp mismatch, mfa failed
                case 'CIAM_E08_00_013': // Email address already in use - used in both Guest and Creator Cache enroll
                case 'CIAM_E08_01_007': // Invalid email address format - used in both Guest and Creator Cache enroll
                    _this.onScreenError({
                        status: 'None',
                        error: title,
                    });
                    break;
                /* End OLE error states */
                /* Begin alternate auto MFA error states */
                case 'CIAM_E08_08_001': // Max input attempts reached
                    _this.onScreenUpdate({
                        type: 'AutoAltMfaMaxRetries',
                    });
                    break;
                /* End alternate auto MFA error states */
                /* Begin Enrollment Data error states */
                /**
                 * LOGIN_USING_EXISTING_CREDENTIALS
                 * Used for credit card enrollment when user in enrollmentData has
                 * existing credentials
                 */
                case 'CIAM_E08_02_006': // Customer has existing credentials from decrypted enrollmentData (OLE)
                case 'CIAM_E08_03_004': // Customer doesn't exist in ACM from decrypted enrollmentData (OLE)
                case 'CIAM_E08_03_003': // Multiple Records for a Customer exist  in ACM from decrypted enrollmentData (OLE)
                case 'CIAM_E08_03_005': // ACM eSign API failure
                case 'CIAM_E08_03_006': // ACM eSign API failure
                case 'CIAM_E00_00_014': // Temp Password Link Expired failure
                    _this.onScreenUpdate({
                        type: 'Password',
                        payload: {
                            status: 'None',
                            error: title,
                        },
                    });
                    break;
                case 'CIAM_E01_07_001': // AAOS redirect (non-migrated user, need MFA on AAOS)
                    // Required to prevent the login remote from showing the default route while the redirect to AAOS occurs
                    _this.onScreenUpdate({
                        type: 'Password',
                        payload: {
                            status: 'None',
                            isRedirectingToAAOS: true,
                        },
                    });
                    window.location.href = constants_1.aaosUrl + "?ticketID=" + ((_b = (_a = failureData === null || failureData === void 0 ? void 0 : failureData.reason) === null || _a === void 0 ? void 0 : _a.data) === null || _b === void 0 ? void 0 : _b.ticketId);
                    break;
                case 'CIAM_E04_01_001': {
                    var _d = (_c = failureData === null || failureData === void 0 ? void 0 : failureData.reason) === null || _c === void 0 ? void 0 : _c.data, username = _d.username, userSoftLinkedRelationships = _d.userSoftLinkedRelationships;
                    _this.onScreenUpdate({
                        type: 'AutoToBankLinking',
                        payload: {
                            status: 'None',
                            username: username,
                            userSoftLinkedRelationships: userSoftLinkedRelationships,
                        },
                    });
                    break;
                }
                /* Begin Auto forgot password & forgot username */
                case 'CIAM_E01_07_002': // Forgot Password
                    _this.onScreenUpdate({
                        type: 'AutoForgotUsernamePassword',
                        payload: {
                            status: 'None',
                            error: 'CIAM_E01_07_002',
                        },
                    });
                    break;
                case 'CIAM_E08_07_007': // Forgot Username
                    _this.onScreenUpdate({
                        type: 'AutoForgotUsernamePassword',
                        payload: {
                            status: 'None',
                            error: 'CIAM_E08_07_007',
                        },
                    });
                    break;
                /* End Auto forgot password & forgot username*/
                /* Start account linking errors */
                case 'CIAM_E08_02_009': // Auto add account - Max retries reached
                    _this.onScreenUpdate({
                        type: 'AutoLinkFailedMaxRetries',
                    });
                    break;
                /* End account linking errors */
                case 'CIAM_E08_08_011':
                    _this.onScreenUpdate({
                        type: 'AddAutoAccountFailure',
                    });
                    break;
                case 'CIAM_E08_08_012': // AAOS_ADD_ACCOUNT_FAILURE generic failue
                    _this.onScreenUpdate({
                        type: 'AddAutoAccountGenericFailure',
                    });
                    break;
                /* End account linking errors */
                /* Begin Creator Cache errors */
                // case 'CIAM_E08_00_013': // TODO fix, already caught above and handled as Guest...
                // case 'CIAM_E08_01_007': // TODO same as above
                case 'CIAM_E08_01_008': // INVALID_PHONE_NUMBER - might not be unique to Creator Cache, just introduced during its implementation
                case 'CIAM_E01_09_001': // EMAIL_NOT_IN_BETA_CREATOR_CACHE
                    _this.onScreenError({
                        status: 'None',
                        error: title,
                    });
                    break;
                /* End Creator Cache errors */
                default:
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: {
                            error: title,
                            errorMode: 'Default',
                        },
                    });
            }
            resolve(sdk_1.ConfirmationInput.create(-1));
        });
    };
    /**
     * Get user response to an information screen. This callback is invoked by the
     * SDK to execute an information screen policy action.
     *
     * The callback is expected to present an information screen to the user, wait
     * for confirmation and return asynchronously to the SDK with a
     * ConfirmationInput object with user selection "-1" once policy evaluation
     * may resume.
     *
     * @param title Title of information screen to display.
     * @param text  Text to display in information screen.
     * @param continueText  Label for the confirmation button
     * @param actionContext Information on the policy action currently executing,
     * within which this callback is invoked.
     * @param clientContext The clientContext for the SDK operation invocation for
     * which this callback is invoked.
     *
     * @return A promise object. On successful result, returns: A
     * ConfirmationInput object. The ConfirmationInput#userChoice property must
     * be set to "-1".
     */
    AllyTransmitUIHandler.prototype.getInformationResponse = function (title, _text, _continueText, _actionContext, _clientContext) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            whisper_1.default.debug({
                message: "Getting Information Response: " + title,
            });
            var onComplete = function () {
                _this.onScreenLoading();
                resolve(sdk_1.ConfirmationInput.create(-1));
            };
            switch (title) {
                case InformationScreenType.SetupWelcome:
                    _this.onScreenUpdate({
                        type: InformationScreenType.SetupWelcome,
                        payload: {
                            onComplete: onComplete,
                            status: 'None',
                        },
                    });
                    break;
                default:
                    whisper_1.default.error({
                        message: 'Rejecting getInformationResponse',
                    });
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: { error: '', errorMode: 'Default' },
                    });
                    reject(new Error("Invalid title for getInformationResponse: " + title));
            }
        });
    };
    /**
     * Get user response to a confirmation screen. This callback is invoked by the
     * SDK to execute a confirmation screen policy action.
     *
     * The callback is expected to present a confirmation screen to the user,
     * collect its input and return it asynchronously to the SDK as a
     * ConfirmationInput object.
     *
     * @param title Title of confirmation screen to display.
     * @param text Text to display in confirmation screen.
     * @param continueText  Label for a confirmation button (pressing this button
     * will cause policy execution to continue)
     * @param cancelText Label for a cancellation button (pressing this button
     * will cause policy execution to abort)
     * @param actionContext Information on the policy action currently executing,
     * within which this callback is invoked.
     * @param clientContext The clientContext for the SDK operation invocation for
     * which this callback is invoked.
     *
     * @return A promise object. On successful result, returns: A
     * ConfirmationInput object encoding user choice. For the
     * ConfirmationInput#userChoice property, a value of "0" means continue and a
     * value of "1" means cancel.
     */
    AllyTransmitUIHandler.prototype.getConfirmationInput = function (title, _text, _continueText, _cancelText, _actionContext, _clientContext) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            whisper_1.default.debug({
                message: "Getting Information Response: " + title,
            });
            // Everything but Register Device should probably be an information response
            var onComplete = function () {
                _this.onScreenLoading();
                resolve(sdk_1.ConfirmationInput.create(0));
            };
            var onCompleteWithAnswer = function (answer) {
                _this.onScreenLoading();
                resolve(answer === 'Yes'
                    ? sdk_1.ConfirmationInput.create(0)
                    : sdk_1.ConfirmationInput.create(1));
            };
            switch (title) {
                case 'register_device':
                    _this.onScreenUpdate({
                        type: 'RegisterDevice',
                        payload: {
                            onComplete: onCompleteWithAnswer,
                            status: 'None',
                        },
                    });
                    break;
                case 'ALLY_ESIGN':
                    _this.onScreenUpdate({
                        type: InformationScreenType.ESign,
                        payload: {
                            onComplete: onComplete,
                            status: 'None',
                        },
                    });
                    break;
                case 'ALLY_OBSA':
                    _this.onScreenUpdate({
                        type: InformationScreenType.OBSA,
                        payload: {
                            onComplete: onComplete,
                            status: 'None',
                        },
                    });
                    break;
                case 'CHANGE_PWD_CONFIRM':
                    _this.onScreenUpdate({
                        type: InformationScreenType.PasswordUpdated,
                        payload: {
                            onComplete: onComplete,
                            status: 'PasswordHint',
                        },
                    });
                    break;
                default:
                    whisper_1.default.error({ message: 'Rejecting getConfirmationInput' });
                    _this.onScreenUpdate({
                        type: 'TemporarilyUnavailable',
                        payload: { error: '', errorMode: 'Default' },
                    });
                    reject(new Error("Invalid title for getConfirmationInput: " + title));
            }
        });
    };
    /**
     * Start a form session. This callback is invoked by the SDK in response to a
     * policy-initiated request.
     *
     * The callback is expected to create a UIFormSession object that represents
     * the UI session for showing forms. The SDK will invoke methods on this
     * session object as necessary to implement the session lifecycle.
     *
     * @param formId defined in Management UI Server policy as a identifier for
     * this form.
     * @param payload JSON defined in Management UI Server policy to initialize
     * the form session.
     *
     * @return a UIFormSession object representing the form session.
     */
    AllyTransmitUIHandler.prototype.createFormSession = function (formId, payload) {
        whisper_1.default.debug({
            message: ["Creating form session. id: " + formId + " | payload:", payload],
        });
        switch (formId) {
            case 'recoveryOptions':
                return new forms_1.RecoveryOptionsFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            case 'change_password':
                return new forms_1.NewPasswordFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            case 'mfa_setup_id':
                return new forms_1.SetupSecurityFormSession(this.onScreenUpdate, this.onScreenLoading, payload, false);
            case 'mfa_reset_id':
                return new forms_1.SetupSecurityFormSession(this.onScreenUpdate, this.onScreenLoading, payload, true);
            case 'mfaSetup_complete_show_id':
                return new forms_1.SetupCompleteFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            /* Begin OLE */
            case 'verifyCustomer':
                return new forms_1.EnrollVerifyCustomerFormSession(this.onScreenUpdate, this.onScreenLoading);
            case 'enrollSendCode':
                return new forms_1.EnrollSendCodeFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            case 'enrollVerifyCode':
                return new forms_1.EnrollVerifyCodeFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            /* Begin alternate Auto verification */
            case 'accountNumberVinSelector':
                return new forms_1.EnrollAdditionalVerificationAutoTypeSelectFormSession(this.onScreenUpdate, this.onScreenLoading);
            case 'accountNumberOrVinInput':
                return new forms_1.EnrollAdditionalVerificationAutoInputFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            /* End alternate Auto verification */
            case 'enrollCredentialCreation':
                return new forms_1.EnrollCredentialCreationFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            case 'enrollConfirmation':
                return new forms_1.EnrollConfirmationFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            /* End OLE */
            /* Begin Guest Enrollment */
            case 'guestCredentialCreation':
                return new forms_1.GuestEnrollCredentialCreationFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            case 'userConsents':
                return new forms_1.GuestEnrollConsentFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            case 'guestEnrollConfirmation':
                return new forms_1.GuestEnrollConfirmationFormSession(this.onScreenUpdate, this.onScreenLoading);
            /* End Guest Enrollment */
            case 'confirmSSN':
                return new forms_1.ConfirmSsnFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            case 'select_account':
                return new forms_1.SelectAccountFormSession(this.onScreenUpdate, this.onScreenLoading);
            case 'usernameConfirmation':
                return new forms_1.ForgotUsernameConfirmationFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            /* Start Auto SPOG screens */
            case 'linkAccounts':
                return new forms_1.BankToAutoLinkingFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            case 'loginLob':
                return new forms_1.LoginAccountTypeSelectorFormSession(this.onScreenUpdate, this.onScreenLoading);
            case 'addAutoAccount':
                return new forms_1.AddAutoAccountFormSession(this.onScreenUpdate, this.onScreenLoading);
            case 'addAutoAccountSuccess':
                return new forms_1.AddAutoAccountSuccessFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            case 'linkingSuccess':
                return new forms_1.LinkingSuccessFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            /* End Auto SPOG screens */
            /* Begin Creator Cache screens */
            case 'change_password_cc':
                return new forms_1.ChangePasswordCreatorCacheFormSession(this.onScreenUpdate, this.onScreenLoading, payload);
            /* End Creator Cache screens */
            default:
                // TODO: perhaps this should throw error?
                whisper_1.default.error({ message: 'Rejecting createFormSession' });
                this.onScreenUpdate({
                    type: 'TemporarilyUnavailable',
                    payload: { error: '', errorMode: 'Default' },
                });
                return null;
        }
    };
    AllyTransmitUIHandler.prototype.createOtpAuthSession = function (title, _username, _possibleTargets, _autoExceedTarget) {
        var _a;
        whisper_1.default.debug({ message: 'Starting OTP Authenticator' });
        return new authenticators_1.OTPAuthenticatorHandler(this.onScreenUpdate, this.onScreenLoading, Boolean((_a = this.currentClientContext) === null || _a === void 0 ? void 0 : _a.isSuppressed));
    };
    AllyTransmitUIHandler.prototype.processJsonData = function (jsonData, _actionContext, _clientContext) {
        var _this = this;
        return new Promise(function (resolve, _) {
            var _a, _b, _c;
            if (jsonData.token) {
                var decodedToken = jwt_decode_1.default(jsonData.token);
                _this.currentClientContext = __assign(__assign({}, _this.currentClientContext), { username: (_b = (_a = decodedToken === null || decodedToken === void 0 ? void 0 : decodedToken['CIAM-App-Token']) === null || _a === void 0 ? void 0 : _a.userNamePvtEncrypt) !== null && _b !== void 0 ? _b : '' });
            }
            if (jsonData.id_token) {
                var decodedToken = utils_1.decodeTokenV2(jsonData.id_token);
                _this.currentClientContext = __assign(__assign({}, _this.currentClientContext), { username: (_c = decodedToken === null || decodedToken === void 0 ? void 0 : decodedToken.userNamePvtEncrypt) !== null && _c !== void 0 ? _c : '' });
            }
            if (jsonData.PASSWORD_HINT !== undefined) {
                var _d = jsonData, _e = _d.email, email = _e === void 0 ? '' : _e, _f = _d.phoneNumber, phoneNumber = _f === void 0 ? '' : _f;
                var passwordHintMethod = email.length > 0 ? email : phoneNumber;
                _this.onScreenUpdate({
                    type: 'Password',
                    payload: {
                        status: 'None',
                        passwordHintMethod: passwordHintMethod,
                    },
                });
            }
            if (jsonData.OTP_SUPPRESS_FLAG !== undefined) {
                _this.currentClientContext = __assign(__assign({}, _this.currentClientContext), { isSuppressed: jsonData.OTP_SUPPRESS_FLAG });
            }
            resolve(sdk_1.JsonDataProcessingResult.create(true));
        });
    };
    return AllyTransmitUIHandler;
}(BaseUIHandler_1.BaseUIHandler));
exports.AllyTransmitUIHandler = AllyTransmitUIHandler;
//# sourceMappingURL=AllyTransmitUIHandler.js.map
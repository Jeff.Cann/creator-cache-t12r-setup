import { PasswordScreen, SendCodeScreen, VerifyCodeScreen } from './authenticators';
import { AllyUserRole, Options, ScreenErrorPayload, Session } from './types';
import { noop } from './utils';
import { AddAutoAccountScreen, AddAutoAccountSuccessScreen, BankToAutoLinkingScreen, ConfirmSsnScreen, EnrollAdditionalAutoInputScreen, EnrollAdditionalAutoTypeSelectScreen, EnrollConfirmationScreen, EnrollCredentialCreationScreen, EnrollVerifyCustomerInfoScreen, ForgotUsernameConfirmationScreen, GuestEnrollConfirmationScreen, GuestEnrollConsentScreen, GuestEnrollCredentialCreationScreen, GuestEnrollCustomerInfoScreen, LinkingSuccessScreen, LoginAccountTypeSelectorScreen, NewPasswordScreen, RecoveryOptionsScreen, SelectAccountScreen, SetupCompleteScreen, SetupSecurityScreen, ChangePasswordCreatorCacheScreen } from './forms';
import { DeviceData } from './headers';
import { BaseUIHandler } from './BaseUIHandler';
export declare type AsyncStatus = 'None' | 'Loading';
interface ConfirmationScreen {
    type: 'RegisterDevice';
    payload: {
        onComplete: (message: 'Yes' | 'No') => void;
        status: AsyncStatus;
    };
}
export declare enum InformationScreenType {
    OBSA = "OBSA",
    ESign = "ESign",
    PasswordUpdated = "PasswordUpdated",
    SetupWelcome = "SetupWelcome"
}
export interface InformationScreen {
    type: InformationScreenType;
    payload: {
        onComplete: () => void;
        status: AsyncStatus;
    };
}
declare type TemporarilyUnavailableErrorModes = 'Default' | 'Straggler' | 'Locked' | 'Blocked' | 'BlockedDeceased' | 'OTPIncorrect' | 'AdditionalSecuritySetupRequired' | 'AuthSessionExpired' | 'RSABlackList';
interface TemporarilyUnavailable {
    type: 'TemporarilyUnavailable';
    payload: {
        error: string | null;
        errorMode: TemporarilyUnavailableErrorModes;
    };
}
interface UnauthenticatedStatus {
    type: 'Unauthenticated';
    payload?: any;
}
export interface AuthenticatedStatus {
    type: 'Authenticated';
    payload: Session;
}
export interface AuthenticatedWithTicketStatus {
    type: 'AuthenticatedWithTicket';
    payload: string;
}
interface MFASuppressionNoOptionsScreen {
    type: 'MFASuppressionNoOptions';
    payload?: any;
}
export interface PasswordChangeProps {
    status: AsyncStatus | 'Blocked' | 'Error';
    error?: string | undefined;
}
export interface UsernameChangeProps {
    status: AsyncStatus | 'Blocked' | 'Error';
    error?: string | undefined;
}
interface ForgotUsernameScreen {
    type: 'ForgotUsername';
    payload: UsernameChangeProps;
}
interface ForgotPasswordScreen {
    type: 'ForgotPassword';
    payload: PasswordChangeProps;
}
interface ResetPasswordScreen {
    type: 'ResetPassword';
    payload: PasswordChangeProps;
}
interface EnrollCustomer {
    type: 'Enroll';
    payload: {
        status: AsyncStatus;
    };
}
interface AutoForgotUsernamePasswordScreen {
    type: 'AutoForgotUsernamePassword';
    payload: {
        status: AsyncStatus;
        error: string;
    };
}
export interface AutoToBankLinkingScreen {
    type: 'AutoToBankLinking';
    payload: {
        status: AsyncStatus;
        username: string;
        userSoftLinkedRelationships: AllyUserRole;
    };
}
export interface AutoLinkFailedMaxRetriesScreen {
    type: 'AutoLinkFailedMaxRetries';
    payload?: any;
}
export interface AutoAltMfaMaxRetriesScreen {
    type: 'AutoAltMfaMaxRetries';
    payload?: any;
}
export interface AddAutoAccountFailureScreen {
    type: 'AddAutoAccountFailure';
    payload?: any;
}
export interface AddAutoAccountGenericFailureScreen {
    type: 'AddAutoAccountGenericFailure';
    payload?: any;
}
export declare type ScreenPayload = UnauthenticatedStatus | AuthenticatedStatus | ConfirmationScreen | InformationScreen | PasswordScreen | SendCodeScreen | SetupCompleteScreen | SetupSecurityScreen | TemporarilyUnavailable | VerifyCodeScreen | NewPasswordScreen | MFASuppressionNoOptionsScreen | ForgotPasswordScreen | ForgotUsernameScreen | RecoveryOptionsScreen | ResetPasswordScreen | AuthenticatedWithTicketStatus | EnrollCustomer | EnrollVerifyCustomerInfoScreen | EnrollCredentialCreationScreen | EnrollConfirmationScreen | GuestEnrollCustomerInfoScreen | GuestEnrollCredentialCreationScreen | GuestEnrollConsentScreen | GuestEnrollConfirmationScreen | ConfirmSsnScreen | SelectAccountScreen | ForgotUsernameConfirmationScreen | EnrollAdditionalAutoTypeSelectScreen | EnrollAdditionalAutoInputScreen | BankToAutoLinkingScreen | LoginAccountTypeSelectorScreen | AutoToBankLinkingScreen | AddAutoAccountSuccessScreen | AddAutoAccountFailureScreen | AutoForgotUsernamePasswordScreen | AddAutoAccountScreen | LinkingSuccessScreen | AutoLinkFailedMaxRetriesScreen | AutoAltMfaMaxRetriesScreen | AddAutoAccountGenericFailureScreen | ChangePasswordCreatorCacheScreen;
export declare type TransmitScreen = ScreenPayload['type'];
export declare type JourneyDispatcher = (screen: ScreenPayload) => void;
export declare type ErrorDispatcher = (payload: ScreenErrorPayload) => void;
interface JSONData {
    CAPI_CSRF?: string;
    AWS_AUTHTOKEN?: string;
    ACCESS_TOKEN?: string;
    OTP_SUPPRESS_FLAG?: boolean;
    PASSWORD_HINT: string;
    token?: string;
    id_token?: string;
    ticketID?: string;
}
export interface ClientContext {
    password?: string;
    authData?: DeviceData;
    isSuppressed?: boolean;
    isComingFromRememberMe?: boolean;
    username?: string;
}
export declare class AllyTransmitUIHandler extends BaseUIHandler {
    protected options: Options;
    protected onScreenUpdate: JourneyDispatcher;
    protected onScreenError: ErrorDispatcher;
    protected onScreenLoading: () => void;
    protected onScreenRedirecting: () => void;
    constructor(options: Options, onScreenUpdate: JourneyDispatcher, onScreenError: ErrorDispatcher, onScreenLoading: () => void, onScreenRedirecting: () => void);
    endActivityIndicator: typeof noop;
    startActivityIndicator: typeof noop;
    currentClientContext?: ClientContext | null;
    controlFlowStarting(clientContext: object | null): void;
    /**
     * No-op placeholder to override the default Transmit method
     */
    selectAuthenticator(_authenticatorArray: TransmitSDK.AuthenticationOption[]): Promise<TransmitSDK.AuthenticatorSelectionResult>;
    /**
     * Triggers the `Password` authenticator, creating a set of lifecycle hooks
     * that will be called to get the user's password
     */
    createPasswordAuthSession(): TransmitSDK.UIAuthenticatorSession<TransmitSDK.PasswordInput>;
    /**
     * Handle policy redirection. This callback is invoked in response to the
     * policy asking to redirect to another policy with a possibly different user
     * Id.
     *
     * Redirection target policy has a RedirectType indicating which SDK API
     * function is used for starting the next policy. RedirectTypeBind redirects
     * to Id-less device bind policy, having null policyId value. For
     * authentication policy or general policy, RedirectTypeAuthenticate or
     * RedirectTypeInvokePolicy with policyId will be provided, respectively.
     *
     * RedirectTypeBindOrAuthenticate allows the client to decide which type to
     * trigger according to local bind state of the SDK.
     *
     * The client context object passed to the invocation which triggered the
     * redirect (which is the one passed to this callback) will continue to
     * be in effect for the redirected policy.
     *
     * @param redirectType Policy redirect type for next policy invocation.
     * @param policyId  Optional policy Id
     * @param userId Current or different user handle (e.g. userId, idToken) to
     * use in next policy invocation. Null value when redirected to anonymous
     * policy.
     * @param additionalParameters A JSON with additional parameters to be sent
     * for next policy evaluation.
     * @param clientContext The clientContext for the SDK operation invocation
     * for which this callback is invoked.
     *
     * @return A promise object. On successful result, returns: A RedirectInput
     * object encoding client choice.
     */
    handlePolicyRedirect(_redirectType: TransmitSDK.RedirectType, _policy: string | null, userID: string, _params: object | null, clientContext: null | ClientContext): Promise<TransmitSDK.RedirectInput>;
    /**
     * Notify policy evaluation rejection. This callback is invoked in response to
     * the policy asking to reject the authentication flow (either using a Reject
     * action or rejection defined for other actions on failure).
     *
     * Implementations should present a rejection screen as defined in the
     * callback parameters to the user, and once policy evaluation may continue
     * (i.e the policy will abort and the SDK invocation will asynchronously
     * return) the implementation must return a ConfirmationInput object
     * asynchronously to the SDK with a user selection value of "-1".
     *
     * @param title Title of rejection screen to display.
     * @param text Text to display in rejection screen.
     * @param buttonText Label for a confirmation button (to dismiss the screen)
     * @param failureData Optional data which may be specified on the Transmit SP
     * Server for session rejection cases. Holds one JSON field named "value",
     * where it's value type may vary.
     * @param actionContext Information on the policy action currently executing,
     * within which this callback is invoked.
     * @param clientContext The clientContext for the SDK operation invocation for
     * which this callback is invoked.
     *
     * @return A promise object. On successful result, returns: A
     * ConfirmationInput object encoding user choice. For the
     * ConfirmationInput#userChoice property, set a value of "-1"
     */
    handlePolicyRejection(title: string | null, _text: string, _buttonText: string | null, failureData: {
        reason?: Record<string, any>;
    } | null, _actionContext: TransmitSDK.PolicyAction | null, _clientContext: object | null): Promise<TransmitSDK.ConfirmationInput>;
    /**
     * Get user response to an information screen. This callback is invoked by the
     * SDK to execute an information screen policy action.
     *
     * The callback is expected to present an information screen to the user, wait
     * for confirmation and return asynchronously to the SDK with a
     * ConfirmationInput object with user selection "-1" once policy evaluation
     * may resume.
     *
     * @param title Title of information screen to display.
     * @param text  Text to display in information screen.
     * @param continueText  Label for the confirmation button
     * @param actionContext Information on the policy action currently executing,
     * within which this callback is invoked.
     * @param clientContext The clientContext for the SDK operation invocation for
     * which this callback is invoked.
     *
     * @return A promise object. On successful result, returns: A
     * ConfirmationInput object. The ConfirmationInput#userChoice property must
     * be set to "-1".
     */
    getInformationResponse(title: string, _text: string, _continueText: string, _actionContext: TransmitSDK.PolicyAction | null, _clientContext: object | null): Promise<TransmitSDK.ConfirmationInput>;
    /**
     * Get user response to a confirmation screen. This callback is invoked by the
     * SDK to execute a confirmation screen policy action.
     *
     * The callback is expected to present a confirmation screen to the user,
     * collect its input and return it asynchronously to the SDK as a
     * ConfirmationInput object.
     *
     * @param title Title of confirmation screen to display.
     * @param text Text to display in confirmation screen.
     * @param continueText  Label for a confirmation button (pressing this button
     * will cause policy execution to continue)
     * @param cancelText Label for a cancellation button (pressing this button
     * will cause policy execution to abort)
     * @param actionContext Information on the policy action currently executing,
     * within which this callback is invoked.
     * @param clientContext The clientContext for the SDK operation invocation for
     * which this callback is invoked.
     *
     * @return A promise object. On successful result, returns: A
     * ConfirmationInput object encoding user choice. For the
     * ConfirmationInput#userChoice property, a value of "0" means continue and a
     * value of "1" means cancel.
     */
    getConfirmationInput(title: string, _text: string, _continueText: string, _cancelText: string, _actionContext: TransmitSDK.PolicyAction | null, _clientContext: object | null): Promise<TransmitSDK.ConfirmationInput>;
    /**
     * Start a form session. This callback is invoked by the SDK in response to a
     * policy-initiated request.
     *
     * The callback is expected to create a UIFormSession object that represents
     * the UI session for showing forms. The SDK will invoke methods on this
     * session object as necessary to implement the session lifecycle.
     *
     * @param formId defined in Management UI Server policy as a identifier for
     * this form.
     * @param payload JSON defined in Management UI Server policy to initialize
     * the form session.
     *
     * @return a UIFormSession object representing the form session.
     */
    createFormSession(formId: string, payload: object): TransmitSDK.UIFormSession;
    createOtpAuthSession(title: string, _username: string, _possibleTargets: TransmitSDK.OtpTarget[], _autoExceedTarget: TransmitSDK.OtpTarget | null): TransmitSDK.UIAuthenticatorSessionOtp;
    processJsonData(jsonData: JSONData, _actionContext: TransmitSDK.PolicyAction | null, _clientContext: object | null): Promise<TransmitSDK.JsonDataProcessingResult>;
}
export {};

"use strict";
/* istanbul ignore file */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/ban-types */
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseUIHandler = void 0;
var BaseUIHandler = /** @class */ (function () {
    function BaseUIHandler() {
    }
    BaseUIHandler.prototype.controlFlowStarting = function (clientContext) { };
    BaseUIHandler.prototype.controlFlowCancelled = function (clientContext) { };
    BaseUIHandler.prototype.controlFlowEnded = function (error, clientContext) { };
    BaseUIHandler.prototype.controlFlowActionStarting = function (actionContext, clientContext) { };
    BaseUIHandler.prototype.controlFlowActionEnded = function (error, actionContext, clientContext) { };
    BaseUIHandler.prototype.handleAuthenticatorUnregistration = function (authenticatorDescription, isPlaceholder, actionContext, clientContext) {
        throw new Error("Method not implemented - handleAuthenticatorUnregistration()");
    };
    BaseUIHandler.prototype.shouldIncludeDisabledAuthenticatorsInMenu = function (actionContext, clientContext) {
        throw new Error("Method not implemented - shouldIncludeDisabledAuthenticatorsInMenu()");
    };
    BaseUIHandler.prototype.selectAuthenticatorFallbackAction = function (validOptions, fallbackAuth, session, actionContext, clientContext) {
        throw new Error("Method not implemented - selectAuthenticatorFallbackAction()");
    };
    BaseUIHandler.prototype.controlOptionForCancellationRequestInSession = function (validOptions, session) {
        throw new Error("Method not implemented - controlOptionForCancellationRequestInSession()");
    };
    BaseUIHandler.prototype.createFingerprintAuthSession = function (title, username) {
        throw new Error("Method not implemented - createFingerprintAuthSession()");
    };
    BaseUIHandler.prototype.createNativeFaceAuthSession = function (title, username) {
        throw new Error("Method not implemented - createNativeFaceAuthSession()");
    };
    BaseUIHandler.prototype.createDeviceBiometricsAuthSession = function (title, username) {
        throw new Error("Method not implemented - createDeviceBiometricsAuthSession()");
    };
    BaseUIHandler.prototype.createPinAuthSession = function (title, username, pinLength) {
        throw new Error("Method not implemented - createPinAuthSession()");
    };
    BaseUIHandler.prototype.createPatternAuthSession = function (title, username, gridWidth, gridHeight) {
        throw new Error("Method not implemented - createPinAuthSession()");
    };
    BaseUIHandler.prototype.createFaceAuthSession = function (title, username) {
        throw new Error("Method not implemented - createFaceAuthSession()");
    };
    BaseUIHandler.prototype.createVoiceAuthSession = function (title, username) {
        throw new Error("Method not implemented - createVoiceAuthSession()");
    };
    BaseUIHandler.prototype.createPlaceholderAuthSession = function (placeholderName, placeholderType, title, username, authenticatorConfiguredData, serverPayload) {
        throw new Error("Method not implemented - createPlaceholderAuthSession()");
    };
    BaseUIHandler.prototype.createMobileApproveAuthSession = function (title, username, instructions) {
        throw new Error("Method not implemented - createMobileApproveAuthSession()");
    };
    BaseUIHandler.prototype.createTotpAuthSession = function (title, username) {
        throw new Error("Method not implemented - createTotpAuthSession()");
    };
    BaseUIHandler.prototype.createSecurityQuestionAuthSession = function (title, username) {
        throw new Error("Method not implemented - createSecurityQuestionAuthSession()");
    };
    BaseUIHandler.prototype.createFido2AuthSession = function (title, username) {
        throw new Error("Method not implemented - createFido2AuthSession()");
    };
    BaseUIHandler.prototype.createScanQrSession = function (actionContext, clientContext) {
        throw new Error("Method not implemented - createScanQrSession()");
    };
    BaseUIHandler.prototype.createTicketWaitSession = function (actionContext, clientContext) {
        throw new Error("Method not implemented - createTicketWaitSession()");
    };
    BaseUIHandler.prototype.createAuthenticationConfigurationSession = function (userId) {
        throw new Error("Method not implemented - createAuthenticationConfigurationSession()");
    };
    BaseUIHandler.prototype.createRegistrationPromotionSession = function (userId, actionContext) {
        throw new Error("Method not implemented - createRegistrationPromotionSession()");
    };
    BaseUIHandler.prototype.createDeviceManagementSession = function (userId) {
        throw new Error("Method not implemented - createDeviceManagementSession()");
    };
    BaseUIHandler.prototype.createApprovalsSession = function (userId) {
        throw new Error("Method not implemented - createApprovalsSession()");
    };
    BaseUIHandler.prototype.createTotpGenerationSession = function (userId, generatorName) {
        throw new Error("Method not implemented - createTotpGenerationSession()");
    };
    BaseUIHandler.prototype.localAuthenticatorInvalidated = function (description, clientContext) {
        throw new Error("Method not implemented - localAuthenticatorInvalidated()");
    };
    return BaseUIHandler;
}());
exports.BaseUIHandler = BaseUIHandler;
//# sourceMappingURL=BaseUIHandler.js.map
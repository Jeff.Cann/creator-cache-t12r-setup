import { BaseAuthenticatorHandler } from './Base';
import { AnyFunction } from '../types';
import { AsyncStatus } from '../AllyTransmitUIHandler';
export interface OnCompleteOpts {
    value: string;
    resolve: AnyFunction;
}
export interface PasswordProps {
    status: AsyncStatus;
    username?: string;
    passwordHintMethod?: string;
    error?: Error | string;
    isRedirectingToAAOS?: boolean;
}
export interface PasswordScreen {
    type: 'Password';
    payload: PasswordProps;
}
/**
 * The PasswordAuthenticatorHandler.
 * Dispatches the `Authenticator` event type named `Password` which will render
 * the user's `authenticators.Password` component. The user should invoke the
 * `onComplete` function when the user has finished entering their password.
 */
export declare class PasswordAuthenticatorHandler extends BaseAuthenticatorHandler<TransmitSDK.PasswordInput> {
    promiseInput(): Promise<TransmitSDK.InputOrControlResponse<TransmitSDK.PasswordInput>>;
}

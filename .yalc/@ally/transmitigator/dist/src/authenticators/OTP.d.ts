import { AsyncStatus, JourneyDispatcher } from '../AllyTransmitUIHandler';
import { BaseAuthenticatorHandler } from './Base';
export interface Target extends TransmitSDK.OtpTarget {
    _channel: TransmitSDK.OtpChannel;
    _channelAssertionId: string;
    _channelIndex: number;
    _description: string;
    _deviceDetails: null;
    _targetIdentifier: string;
}
declare type SuppressionStatus = 'NoEmail' | 'None';
export interface SendCodeProps {
    status: AsyncStatus | 'Error';
    targets: Target[];
    onComplete: (target: Target) => void;
    getOptionType: OptionTypeFetcher;
    suppressionStatus: SuppressionStatus;
}
export declare type ReadableDeliveryMethod = 'text message' | 'email' | 'voice' | 'unknown';
declare type OptionTypeFetcher = (channel: TransmitSDK.OtpChannel) => ReadableDeliveryMethod;
export interface MaskedDeliveryOption {
    type: ReadableDeliveryMethod;
    value: string;
}
export interface VerifyCodeProps {
    status: AsyncStatus | 'Error';
    onComplete: (otp: string) => void;
    handleResend: () => void;
    navigateToSendCode: () => void;
    otpInputCount?: number;
    targetsLength: number;
    maskedDeliveryOption: MaskedDeliveryOption;
}
export interface SendCodeScreen {
    type: 'SendCode';
    payload: SendCodeProps;
}
export interface VerifyCodeScreen {
    type: 'VerifyCode';
    payload: VerifyCodeProps;
}
declare type TargetResponse = TransmitSDK.TargetBasedAuthenticatorInput<TransmitSDK.OtpInput, Target>;
export declare const getOptionType: OptionTypeFetcher;
export declare class OTPAuthenticatorHandler extends BaseAuthenticatorHandler<TargetResponse> implements TransmitSDK.UIAuthenticatorSessionOtp {
    private targets?;
    private activeOTPScreen;
    private maskedDeliveryOption;
    private isOTPResetting;
    private isSuppressed;
    constructor(onScreenUpdate: JourneyDispatcher, onScreenLoading: () => void, isSuppressed: boolean);
    setAvailableTargets(targets: Target[] | null): void;
    setGeneratedOtp(_format: Target | null, target: Target | null): void;
    promiseInput(): Promise<TransmitSDK.InputOrControlResponse<TargetResponse>>;
}
export {};

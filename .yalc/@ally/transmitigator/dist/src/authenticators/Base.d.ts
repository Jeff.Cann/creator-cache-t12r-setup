import { JourneyDispatcher } from '../AllyTransmitUIHandler';
import { noop } from '../utils';
/**
 * A "Base" authenticator.
 * This "stubs" all methods an authenticator must have except for the
 * `promiseInput` method which must be implemented by the extending subclass.
 */
export declare abstract class BaseAuthenticatorHandler<ResponseType extends TransmitSDK.InputResponseType> implements TransmitSDK.UIAuthenticatorSession<ResponseType> {
    protected error: Error | undefined;
    protected value?: string;
    protected onScreenUpdate: JourneyDispatcher;
    protected onScreenLoading: () => void;
    protected context?: object | null;
    constructor(onScreenUpdate: JourneyDispatcher, onScreenLoading: () => void);
    endSession: typeof noop;
    startSession(_description: TransmitSDK.AuthenticatorDescription, _mode: TransmitSDK.AuthenticatorSessionMode, _action: TransmitSDK.PolicyAction | null, context: object | null): void;
    promiseRecoveryForError(e: TransmitSDK.AuthenticationError, _validRecoveries: Array<TransmitSDK.AuthenticationErrorRecovery>, _defaultRecovery: TransmitSDK.AuthenticationErrorRecovery): Promise<number>;
    changeSessionModeToRegistrationAfterExpiration(): void;
    promiseInput(): Promise<TransmitSDK.InputOrControlResponse<ResponseType>>;
}

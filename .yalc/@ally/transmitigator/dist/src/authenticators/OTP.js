"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.OTPAuthenticatorHandler = exports.getOptionType = void 0;
var Base_1 = require("./Base");
var sdk_1 = require("../sdk");
var OTPScreen;
(function (OTPScreen) {
    OTPScreen[OTPScreen["SendCode"] = 0] = "SendCode";
    OTPScreen[OTPScreen["VerifyCode"] = 1] = "VerifyCode";
})(OTPScreen || (OTPScreen = {}));
// There's a utility method here to get the human readable form, because I'm
// unsure of how serialization will work if I make modifications to the Target
// object prior to sending that back to the server. Proper solution is to make
// this modification in the Transmit Server tool.
var getOptionType = function (channel) {
    switch (channel) {
        case sdk_1.OtpChannel.Sms:
            return 'text message';
        case sdk_1.OtpChannel.Email:
            return 'email';
        case sdk_1.OtpChannel.VoiceCall:
            return 'voice';
        default:
            return 'unknown';
    }
};
exports.getOptionType = getOptionType;
var OTPAuthenticatorHandler = /** @class */ (function (_super) {
    __extends(OTPAuthenticatorHandler, _super);
    function OTPAuthenticatorHandler(onScreenUpdate, onScreenLoading, isSuppressed) {
        var _this = _super.call(this, onScreenUpdate, onScreenLoading) || this;
        _this.activeOTPScreen = OTPScreen.SendCode;
        _this.isOTPResetting = false;
        _this.isSuppressed = isSuppressed;
        _this.maskedDeliveryOption = {
            type: 'unknown',
            value: '',
        };
        return _this;
    }
    OTPAuthenticatorHandler.prototype.setAvailableTargets = function (targets) {
        this.targets = targets;
    };
    OTPAuthenticatorHandler.prototype.setGeneratedOtp = function (_format, target) {
        if (this.isOTPResetting) {
            this.activeOTPScreen = OTPScreen.SendCode;
            this.isOTPResetting = false;
            return;
        }
        if (target) {
            this.activeOTPScreen = OTPScreen.VerifyCode;
            this.maskedDeliveryOption = {
                type: exports.getOptionType(target._channel),
                value: target._description,
            };
        }
    };
    OTPAuthenticatorHandler.prototype.promiseInput = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var _a, _b, _c, _d;
            switch (_this.activeOTPScreen) {
                case OTPScreen.SendCode:
                    _this.onScreenUpdate({
                        type: 'SendCode',
                        payload: {
                            status: ((_a = _this.error) === null || _a === void 0 ? void 0 : _a.name) ? 'Error' : 'None',
                            targets: _this.targets,
                            getOptionType: exports.getOptionType,
                            suppressionStatus: _this.isSuppressed ? 'NoEmail' : 'None',
                            onComplete: function (target) {
                                _this.onScreenLoading();
                                resolve(sdk_1.InputOrControlResponse.createInputResponse(sdk_1.TargetBasedAuthenticatorInput.createTargetSelectionRequest(target)));
                            },
                        },
                    });
                    break;
                case OTPScreen.VerifyCode: {
                    _this.onScreenUpdate({
                        type: 'VerifyCode',
                        payload: {
                            status: ((_b = _this.error) === null || _b === void 0 ? void 0 : _b.name) ? 'Error' : 'None',
                            targetsLength: (_d = (_c = _this.targets) === null || _c === void 0 ? void 0 : _c.length) !== null && _d !== void 0 ? _d : 1,
                            maskedDeliveryOption: _this.maskedDeliveryOption,
                            handleResend: function () {
                                // TODO: would adding loading here work?
                                return resolve(sdk_1.InputOrControlResponse.createInputResponse(sdk_1.TargetBasedAuthenticatorInput.createAuthenticatorInput(sdk_1.OtpInputRequestResend.createOtpResendRequest())));
                            },
                            navigateToSendCode: function () {
                                _this.isOTPResetting = true;
                                _this.error = undefined;
                                // This re-triggers setGeneratedOtp, then promiseInput. There's
                                // no way to nullify the original selected OTP Target which is
                                // why we're keeping track of an isOtpResetting boolean.
                                resolve(sdk_1.InputOrControlResponse.createControlResponse(sdk_1.ControlRequest.create(sdk_1.ControlRequestType.RetryAuthenticator)));
                            },
                            onComplete: function (otp) {
                                _this.onScreenLoading();
                                resolve(sdk_1.InputOrControlResponse.createInputResponse(sdk_1.TargetBasedAuthenticatorInput.createAuthenticatorInput(sdk_1.OtpInputOtpSubmission.createOtpSubmission(otp))));
                            },
                        },
                    });
                    break;
                }
                default:
                    reject(new Error('Invalid OTPScreen!'));
                    break;
            }
        });
    };
    return OTPAuthenticatorHandler;
}(Base_1.BaseAuthenticatorHandler));
exports.OTPAuthenticatorHandler = OTPAuthenticatorHandler;
//# sourceMappingURL=OTP.js.map
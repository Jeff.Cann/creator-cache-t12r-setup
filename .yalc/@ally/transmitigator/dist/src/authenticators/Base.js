"use strict";
/* eslint-disable @typescript-eslint/ban-types */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseAuthenticatorHandler = void 0;
var whisper_1 = __importDefault(require("../whisper"));
var utils_1 = require("../utils");
var sdk_1 = require("../sdk");
/**
 * A "Base" authenticator.
 * This "stubs" all methods an authenticator must have except for the
 * `promiseInput` method which must be implemented by the extending subclass.
 */
var BaseAuthenticatorHandler = /** @class */ (function () {
    function BaseAuthenticatorHandler(onScreenUpdate, onScreenLoading) {
        this.endSession = utils_1.noop;
        this.error = undefined;
        this.onScreenUpdate = onScreenUpdate;
        this.onScreenLoading = onScreenLoading;
    }
    BaseAuthenticatorHandler.prototype.startSession = function (_description, _mode, _action, context) {
        this.context = context;
    };
    BaseAuthenticatorHandler.prototype.promiseRecoveryForError = function (e, _validRecoveries, _defaultRecovery) {
        var _this = this;
        this.error = e && utils_1.toConsumableError(e);
        whisper_1.default.error({ message: "Transmit Error: " + this.error });
        return new Promise(function (resolve) {
            var _a;
            if ((_a = _this.error) === null || _a === void 0 ? void 0 : _a.toString().includes('Auth session expired')) {
                _this.onScreenUpdate({
                    type: 'TemporarilyUnavailable',
                    payload: { error: '', errorMode: 'AuthSessionExpired' },
                });
                resolve(sdk_1.AuthenticationErrorRecovery.Fail);
            }
            else
                resolve(sdk_1.AuthenticationErrorRecovery.RetryAuthenticator);
        });
    };
    BaseAuthenticatorHandler.prototype.changeSessionModeToRegistrationAfterExpiration = function () {
        whisper_1.default.warn({ message: 'Not Implemented' });
    };
    BaseAuthenticatorHandler.prototype.promiseInput = function () {
        return Promise.reject(new Error('Not implemented...'));
    };
    return BaseAuthenticatorHandler;
}());
exports.BaseAuthenticatorHandler = BaseAuthenticatorHandler;
//# sourceMappingURL=Base.js.map
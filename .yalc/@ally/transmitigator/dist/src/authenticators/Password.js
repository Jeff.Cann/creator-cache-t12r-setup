"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PasswordAuthenticatorHandler = void 0;
var Base_1 = require("./Base");
var sdk_1 = require("../sdk");
/**
 * The PasswordAuthenticatorHandler.
 * Dispatches the `Authenticator` event type named `Password` which will render
 * the user's `authenticators.Password` component. The user should invoke the
 * `onComplete` function when the user has finished entering their password.
 */
var PasswordAuthenticatorHandler = /** @class */ (function (_super) {
    __extends(PasswordAuthenticatorHandler, _super);
    function PasswordAuthenticatorHandler() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PasswordAuthenticatorHandler.prototype.promiseInput = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var password = (_this.context || { password: null }).password;
            if (password && !_this.error) {
                resolve(sdk_1.InputOrControlResponse.createInputResponse(sdk_1.PasswordInput.create(password)));
                return;
            }
            reject(_this.error);
        });
    };
    return PasswordAuthenticatorHandler;
}(Base_1.BaseAuthenticatorHandler));
exports.PasswordAuthenticatorHandler = PasswordAuthenticatorHandler;
//# sourceMappingURL=Password.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getHDM = void 0;
var constants_1 = require("../constants");
var getHDM = function () {
    var el = document.getElementById(constants_1.DEVICE_PRINT_ELEMENT_ID);
    return (el && el.value) || '';
};
exports.getHDM = getHDM;
//# sourceMappingURL=hdmJavascriptData.js.map
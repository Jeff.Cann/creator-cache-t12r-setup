"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getJavascriptData = void 0;
var getJavascriptData = function () {
    var javascriptData = window.nsp && window.nsp.cfn();
    if (!javascriptData) {
        throw Error('Missing JavascriptData. Make sure 41P vendor file is loaded.');
    }
    return javascriptData;
};
exports.getJavascriptData = getJavascriptData;
//# sourceMappingURL=javascriptData.js.map
export interface DeviceData {
    hdmJavascriptData: string | null;
    javascriptData: string;
}
export declare const getAllDeviceData: () => Promise<DeviceData>;

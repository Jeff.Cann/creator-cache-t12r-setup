import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
export interface UpdateUsernameOptions extends ActionFeatureFlagOptions {
    deviceData?: DeviceData;
    journey_version?: string;
    ticketId: string;
}
export declare function updateUsername(config: Config, options: UpdateUsernameOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options: UpdateUsernameOptions) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

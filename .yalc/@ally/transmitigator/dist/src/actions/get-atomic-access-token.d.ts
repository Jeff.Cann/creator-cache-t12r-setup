import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
export interface GetAtomicAccessTokenOptions extends ActionFeatureFlagOptions {
    access_token: string;
    username: string;
    guid: string;
    accountsArray: Account[];
    deviceData?: DeviceData;
    journey_version?: string;
}
export interface Account {
    accountNumber: string;
    routingNumber: string;
    title: string;
    type: string;
}
export declare function getAtomicAccessToken(config: Config, options: GetAtomicAccessTokenOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options: GetAtomicAccessTokenOptions) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

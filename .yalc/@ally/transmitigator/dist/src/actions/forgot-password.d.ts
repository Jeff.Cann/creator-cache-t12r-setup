import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
import { DeviceData } from '../headers';
export interface ForgotPasswordOptions extends ActionFeatureFlagOptions {
    username: string;
    recoveryOptions: 'changePassword' | 'emailHint';
    ssn: string;
    deviceData: DeviceData;
    journey_version?: string;
}
export declare function forgotPassword(config: Config, options: ForgotPasswordOptions): Promise<TransmitResponse>;
declare const _default: (config: Config, options?: (ForgotPasswordOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

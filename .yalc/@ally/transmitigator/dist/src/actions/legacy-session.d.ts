import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
export interface LegacySessionOptions extends ActionFeatureFlagOptions {
    access_token: string;
    deviceData?: DeviceData;
    username: string;
    journey_version?: string;
    token_type?: string;
}
/**
 * Creates a trusted transfer ticketID
 */
export declare function legacySession(config: Config, options: LegacySessionOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options: LegacySessionOptions) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

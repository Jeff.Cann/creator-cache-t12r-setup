import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
export interface OnlineEnrollmentOptions extends ActionFeatureFlagOptions {
    deviceData: DeviceData;
    journey_version?: string;
    enrollmentData?: string;
    enrollmentDataSource?: string;
    oaa_ole?: boolean;
}
export declare function onlineEnrollment(config: Config, options: OnlineEnrollmentOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options?: (OnlineEnrollmentOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, Destination, TransmitResponse } from '../types';
export interface TransferTicketOptions extends ActionFeatureFlagOptions {
    destination: Destination;
    access_token: string;
    deviceData?: DeviceData;
    username: string;
    journey_version?: string;
}
/**
 * Creates a trusted transfer ticketID
 */
export declare function transferTicket(config: Config, options: TransferTicketOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options: TransferTicketOptions) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

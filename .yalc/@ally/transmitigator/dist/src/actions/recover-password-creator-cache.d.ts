import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
import { DeviceData } from '../headers';
export interface RecoverPasswordCreatorCacheOptions extends ActionFeatureFlagOptions {
    deviceData: DeviceData;
    journey_version?: string;
    lastName: string;
    username: string;
}
export declare function recoverPasswordCreatorCache(config: Config, options: RecoverPasswordCreatorCacheOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options?: (RecoverPasswordCreatorCacheOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

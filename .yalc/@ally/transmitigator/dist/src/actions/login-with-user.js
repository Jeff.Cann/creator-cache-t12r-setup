"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loginWithUser = void 0;
var whisper_1 = __importDefault(require("../whisper"));
var logout_1 = __importDefault(require("./logout"));
var types_1 = require("../types");
var utils_1 = require("../utils");
var utilities_1 = require("../utilities");
/**
 * Logs in with a username/password combination.
 */
function loginWithUsername(_a, options) {
    var myRiskID = _a.myRiskID, sdk = _a.sdk, policies = _a.policies, application = _a.application, _b = _a.loginParams, loginParams = _b === void 0 ? {} : _b;
    return __awaiter(this, void 0, void 0, function () {
        var username, password, _c, rememberMeFlag, deviceData, _d, sType, _e, journey_version, actionToken, params, authenticationResult;
        return __generator(this, function (_f) {
            switch (_f.label) {
                case 0:
                    username = options.username, password = options.password, _c = options.rememberMeFlag, rememberMeFlag = _c === void 0 ? false : _c, deviceData = options.deviceData, _d = options.sType, sType = _d === void 0 ? 'jt' : _d, _e = options.journey_version, journey_version = _e === void 0 ? 'v2' : _e;
                    return [4 /*yield*/, myRiskID.setUser(username)];
                case 1:
                    _f.sent();
                    return [4 /*yield*/, myRiskID.triggerActionEvent(types_1.ActionEvent.login)];
                case 2:
                    actionToken = (_f.sent()).actionToken;
                    params = __assign(__assign(__assign({ actionToken: actionToken,
                        password: password, authData: __assign({}, deviceData), ticketID: utils_1.getSSOTicketID(), sType: sType,
                        rememberMeFlag: rememberMeFlag,
                        journey_version: journey_version }, utils_1.getAppInfoParams(application)), loginParams), utilities_1.getFlagParams(options));
                    return [4 /*yield*/, sdk.authenticate(username, policies.login, params, {
                            password: password,
                            username: username,
                        })];
                case 3:
                    authenticationResult = _f.sent();
                    return [2 /*return*/, {
                            authenticationResult: authenticationResult,
                            deviceData: deviceData,
                            journey_version: journey_version,
                        }];
            }
        });
    });
}
/**
 * Logs in with a rememberMeToken/password combination.
 * This will get the user's username using the REMEMBERME_SECURE policy and
 * *automatically* redirect to the AUTHENTICATION_CUSTOMER policy. There is
 * logic directly related to this redirect in:
 * @see AllyTransmitUIHandler#handlePolicyRedirect.
 *
 * For this to work (for some odd reason), the `rememberMeFlag` param must be
 * passed as `true`. /shrug...
 */
function loginWithRememberedUsername(_a, options) {
    var myRiskID = _a.myRiskID, sdk = _a.sdk, policies = _a.policies, application = _a.application, _b = _a.loginParams, loginParams = _b === void 0 ? {} : _b;
    return __awaiter(this, void 0, void 0, function () {
        var password, rememberMeToken, deviceData, _c, journey_version, actionToken, params, authenticationResult;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0:
                    password = options.password, rememberMeToken = options.rememberMeToken, deviceData = options.deviceData, _c = options.journey_version, journey_version = _c === void 0 ? 'v2' : _c;
                    return [4 /*yield*/, myRiskID.clearUser()];
                case 1:
                    _d.sent();
                    return [4 /*yield*/, myRiskID.triggerActionEvent(types_1.ActionEvent.login)];
                case 2:
                    actionToken = (_d.sent()).actionToken;
                    params = __assign(__assign(__assign({ password: password,
                        actionToken: actionToken, ticketID: utils_1.getSSOTicketID(), rememberMeFlag: true, rememberMeToken: rememberMeToken,
                        journey_version: journey_version, authData: __assign({}, deviceData) }, utils_1.getAppInfoParams(application)), loginParams), utilities_1.getFlagParams(options));
                    return [4 /*yield*/, sdk.invokeAnonymousPolicy(policies.loginRememberMe, params, {
                            password: password,
                            isComingFromRememberMe: true,
                            authData: __assign({}, deviceData),
                        })];
                case 3:
                    authenticationResult = _d.sent();
                    return [2 /*return*/, {
                            authenticationResult: authenticationResult,
                            deviceData: deviceData,
                            journey_version: journey_version,
                        }];
            }
        });
    });
}
/**
 * Logs in a user one of two ways:
 *
 * 1. Using a username and password.
 * 2. Using a rememberMeToken and password.
 *
 * You must provide either `username` OR `rememberMeToken`. If both are supplied
 * TypeScript will gripe at you and `username` will take precedence.
 *
 * NOTE: `rememberMeFlag` is a boolean indicating that a `rememberMeToken`
 * should be returned in the JWT CIAM token.
 */
function loginWithUser(config, options) {
    return __awaiter(this, void 0, void 0, function () {
        var username, rememberMeToken, _a, rememberMeFlag, deviceData, transmitResponse, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0: return [4 /*yield*/, logout_1.default(config)];
                case 1:
                    _c.sent();
                    username = options.username, rememberMeToken = options.rememberMeToken, _a = options.rememberMeFlag, rememberMeFlag = _a === void 0 ? false : _a, deviceData = options.deviceData;
                    whisper_1.default.info({
                        message: [
                            'Action: loginWithUser:',
                            { username: username, rememberMeFlag: rememberMeFlag, rememberMeToken: rememberMeToken, deviceData: deviceData },
                        ],
                    });
                    if (!username) return [3 /*break*/, 3];
                    return [4 /*yield*/, loginWithUsername(config, options)];
                case 2:
                    _b = _c.sent();
                    return [3 /*break*/, 5];
                case 3: return [4 /*yield*/, loginWithRememberedUsername(config, options)
                    // We need to reset the interrupts sessionStorage flag on successful login
                ];
                case 4:
                    _b = _c.sent();
                    _c.label = 5;
                case 5:
                    transmitResponse = _b;
                    // We need to reset the interrupts sessionStorage flag on successful login
                    utilities_1.resetInterrupts();
                    utils_1.deleteTransmitSession(config);
                    return [2 /*return*/, transmitResponse];
            }
        });
    });
}
exports.loginWithUser = loginWithUser;
exports.default = utils_1.asUIConsumer(utils_1.asSession(loginWithUser));
//# sourceMappingURL=login-with-user.js.map
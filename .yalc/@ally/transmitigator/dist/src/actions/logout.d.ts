import { Config } from '../types';
export interface LogoutOptions {
    journey_version?: string;
}
export declare function logout(config: Config, { journey_version }?: LogoutOptions): Promise<null>;
export default logout;

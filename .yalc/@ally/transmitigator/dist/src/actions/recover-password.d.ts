import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
import { DeviceData } from '../headers';
export interface RecoverPasswordOptions extends ActionFeatureFlagOptions {
    deviceData: DeviceData;
    journey_version: string;
    isAutoBusinessUser: boolean;
    username?: string;
    lastName?: string;
    isResetPassword?: boolean;
    tin?: string;
    vin?: string;
    accountNumber?: string;
    passwordHint?: boolean;
    createNewPassword?: boolean;
}
export declare function recoverPassword(config: Config, options: RecoverPasswordOptions): Promise<TransmitResponse>;
declare const _default: (config: Config, options?: (RecoverPasswordOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

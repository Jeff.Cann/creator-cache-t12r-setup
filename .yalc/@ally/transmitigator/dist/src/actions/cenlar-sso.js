"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cenlarSso = void 0;
var whisper_1 = __importDefault(require("../whisper"));
var headers_1 = require("../headers");
var utils_1 = require("../utils");
var utilities_1 = require("../utilities");
var handleFailure = function (e) {
    whisper_1.default.warn({ message: ['Cenlar SSO journey rejected', e] });
    return null;
};
function cenlarSso(config, options) {
    return __awaiter(this, void 0, void 0, function () {
        var sdk, policies, application, loan_number, username, access_token, _a, journey_version, deviceData, _b, params, authenticationResult, e_1;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    sdk = config.sdk, policies = config.policies, application = config.application;
                    loan_number = options.loan_number, username = options.username, access_token = options.access_token, _a = options.journey_version, journey_version = _a === void 0 ? 'v2' : _a;
                    _b = options.deviceData;
                    if (_b) return [3 /*break*/, 2];
                    return [4 /*yield*/, headers_1.getAllDeviceData()];
                case 1:
                    _b = (_c.sent());
                    _c.label = 2;
                case 2:
                    deviceData = _b;
                    whisper_1.default.info({
                        message: ['Action: Cenlar SSO Journey'],
                    });
                    if (!access_token || !username) {
                        whisper_1.default.debug({ message: 'Cenlar SSO journey aborted' });
                        return [2 /*return*/, null];
                    }
                    params = __assign(__assign(__assign({ access_token: access_token,
                        journey_version: journey_version,
                        loan_number: loan_number }, utils_1.getAppInfoParams(application)), { authData: __assign({}, deviceData) }), utilities_1.getFlagParams(options));
                    _c.label = 3;
                case 3:
                    _c.trys.push([3, 5, , 6]);
                    whisper_1.default.info({
                        message: ['PARAMS', params],
                    });
                    return [4 /*yield*/, sdk.authenticate(username, policies.cenlarSso, params, null)];
                case 4:
                    authenticationResult = _c.sent();
                    whisper_1.default.info({
                        message: ['authenticationResult', authenticationResult],
                    });
                    utils_1.deleteTransmitSession(config);
                    return [2 /*return*/, { deviceData: deviceData, authenticationResult: authenticationResult, journey_version: journey_version }];
                case 5:
                    e_1 = _c.sent();
                    return [2 /*return*/, handleFailure(e_1)];
                case 6: return [2 /*return*/];
            }
        });
    });
}
exports.cenlarSso = cenlarSso;
exports.default = utils_1.asSession(cenlarSso);
//# sourceMappingURL=cenlar-sso.js.map
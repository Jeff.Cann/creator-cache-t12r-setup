import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
interface EnrollGuestPayload {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
}
export interface EnrollGuestOptions extends ActionFeatureFlagOptions {
    deviceData: DeviceData;
    journey_version?: string;
    payload: EnrollGuestPayload;
}
export declare function enrollGuest(config: Config, options: EnrollGuestOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options?: (EnrollGuestOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

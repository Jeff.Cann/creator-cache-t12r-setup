import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
export interface GetSecurityProfileOptions extends ActionFeatureFlagOptions {
    access_token: string;
    username: string;
    deviceData?: DeviceData;
    journey_version?: string;
}
export declare function getSecurityProfile(config: Config, options: GetSecurityProfileOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options: GetSecurityProfileOptions) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

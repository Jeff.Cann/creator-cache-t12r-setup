import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
export interface ForgotUsernameOptions extends ActionFeatureFlagOptions {
    deviceData: DeviceData;
    isAutoBusinessUser: boolean;
    accountNumber?: string;
    email?: string;
    journey_version?: string;
    lastName?: string;
    tin?: string;
    vin?: string;
}
export declare function forgotUsername(config: Config, options: ForgotUsernameOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options?: (ForgotUsernameOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

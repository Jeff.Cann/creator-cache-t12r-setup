import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Application, Config, TransmitResponse } from '../types';
export interface RehydrateOptions extends ActionFeatureFlagOptions {
    deviceData?: DeviceData;
    setTicketID?: (ticketId: string | null) => void;
    journey_version?: string;
    allowDaoSessionRehydrate?: boolean;
    application?: Application;
    ticketID?: string;
}
/**
 * Attempts to rehydrate the user's session.
 * If `allowCrossTabSession` is false, this will prevent rehydration across
 * tabs unless a ticketId is provided. The `ticketId` is a mechanism for
 * rehydration coming from other sites and must begin with `iv` or `sf` for
 * Invest and StoreFront, respectively.
 */
export declare function rehydrate(config: Config, options?: RehydrateOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options?: (RehydrateOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

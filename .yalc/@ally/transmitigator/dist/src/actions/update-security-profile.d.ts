import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
interface UpdateSecurityProfileBase extends ActionFeatureFlagOptions {
    access_token: string;
    username: string;
    deviceData?: DeviceData;
    journey_version?: string;
}
export declare type MFAUpdateType = 'add' | 'delete' | 'update';
export interface MFADeliveryMethod {
    deliveryMethodType: string;
    deliveryMethodValuePvtBlock: string;
}
export interface UpdateUsernameActionProps extends UpdateSecurityProfileBase {
    currentPassword: string;
    newUsername: string;
}
export interface UpdatePasswordActionProps extends UpdateSecurityProfileBase {
    currentPassword: string;
    newPassword: string;
    newPasswordHint: string;
}
export interface AddMFAOptionActionProps extends UpdateSecurityProfileBase {
    currentPassword: string;
    mfaDeliveryMethods: MFADeliveryMethod[];
    mfaUpdateType: Extract<MFAUpdateType, 'add'>;
}
export interface RemoveMFAOptionActionProps extends UpdateSecurityProfileBase {
    mfaDeliveryMethods: MFADeliveryMethod[];
    mfaUpdateType: Extract<MFAUpdateType, 'delete'>;
}
export interface UpdateTPINActionProps extends UpdateSecurityProfileBase {
    currentPassword: string;
    newTelebankingPIN: string;
}
export interface UpdateSecurityQAActionProps extends UpdateSecurityProfileBase {
    currentPassword: string;
    newSecurityQuestion: string;
    newSecurityAnswer: string;
}
export declare type UpdateSecurityProfileOptions = UpdateUsernameActionProps | UpdatePasswordActionProps | AddMFAOptionActionProps | RemoveMFAOptionActionProps | UpdateTPINActionProps | UpdateSecurityQAActionProps;
export declare function getUpdateOptions(options: UpdateSecurityProfileOptions): Record<string, unknown>;
export declare function updateSecurityProfile(config: Config, options: UpdateSecurityProfileOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options: UpdateSecurityProfileOptions) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

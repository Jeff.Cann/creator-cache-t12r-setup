import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
export interface CenlarSSOOptions extends ActionFeatureFlagOptions {
    access_token: string;
    deviceData?: DeviceData;
    loan_number: string;
    journey_version?: string;
    username: string;
}
export declare function cenlarSso(config: Config, options: CenlarSSOOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options: CenlarSSOOptions) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

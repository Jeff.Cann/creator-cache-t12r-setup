import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
import { DeviceData } from '../headers';
export interface LoginCreatorCacheOptions extends ActionFeatureFlagOptions {
    deviceData: DeviceData;
    journey_version?: string;
    password: string;
    username: string;
}
export declare function loginCreatorCache(config: Config, options: LoginCreatorCacheOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options?: (LoginCreatorCacheOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
import { DeviceData } from '../headers';
export interface EnrollCreatorCacheOptions extends ActionFeatureFlagOptions {
    deviceData: DeviceData;
    journey_version?: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    phoneNumber: string;
}
export declare function enrollCreatorCache(config: Config, options: EnrollCreatorCacheOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options?: (EnrollCreatorCacheOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
interface BaseLoginOptions extends ActionFeatureFlagOptions {
    password: string;
    deviceData: DeviceData;
    sType?: 'ts' | 'jt';
    journey_version?: string;
}
interface UsernameOptions extends BaseLoginOptions {
    username: string;
    rememberMeFlag?: boolean;
    rememberMeToken?: never;
}
interface RememberedOptions extends BaseLoginOptions {
    username?: never;
    rememberMeFlag?: never;
    rememberMeToken: string;
}
export declare type LoginWithUserOptions = RememberedOptions | UsernameOptions;
/**
 * Logs in a user one of two ways:
 *
 * 1. Using a username and password.
 * 2. Using a rememberMeToken and password.
 *
 * You must provide either `username` OR `rememberMeToken`. If both are supplied
 * TypeScript will gripe at you and `username` will take precedence.
 *
 * NOTE: `rememberMeFlag` is a boolean indicating that a `rememberMeToken`
 * should be returned in the JWT CIAM token.
 */
export declare function loginWithUser(config: Config, options: LoginWithUserOptions): Promise<TransmitResponse>;
declare const _default: (config: Config, options?: (UsernameOptions & import("../utils").UIConsumer) | (RememberedOptions & import("../utils").UIConsumer)) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

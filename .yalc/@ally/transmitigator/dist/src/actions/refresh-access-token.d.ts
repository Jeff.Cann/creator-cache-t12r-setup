import { DeviceData } from '../headers';
import { ActionFeatureFlagOptions, Config, TransmitResponse } from '../types';
export interface RefreshAccessTokenOptions extends ActionFeatureFlagOptions {
    access_token: string;
    username: string;
    deviceData?: DeviceData;
    tokenID?: string;
    destination?: string;
    journey_version?: string;
}
/**
 * Creates a trusted transfer ticketID
 */
export declare function refreshAccessToken(config: Config, options: RefreshAccessTokenOptions): Promise<TransmitResponse | null>;
declare const _default: (config: Config, options?: (RefreshAccessTokenOptions & import("../utils").UIConsumer) | undefined) => Promise<import("../utils").SessionPayload | import("../utils").TicketPayload | import("../utils").LegacySessionPayload | import("../utils").CenlarSSOPayload | import("../utils").ChatTokenPayload | import("../utils").GetSecurityProfilePayload | import("../utils").GetAtomicAccessTokenPayload | null>;
export default _default;

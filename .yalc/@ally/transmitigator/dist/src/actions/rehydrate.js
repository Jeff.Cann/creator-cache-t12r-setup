"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.rehydrate = void 0;
var headers_1 = require("../headers");
var whisper_1 = __importDefault(require("../whisper"));
var types_1 = require("../types");
var utils_1 = require("../utils");
var utilities_1 = require("../utilities");
var handleRehydrationFailure = function (e) {
    whisper_1.default.warn({ message: ['Rehydration rejected', e] });
    return null;
};
/**
 * Attempts to rehydrate the user's session.
 * If `allowCrossTabSession` is false, this will prevent rehydration across
 * tabs unless a ticketId is provided. The `ticketId` is a mechanism for
 * rehydration coming from other sites and must begin with `iv` or `sf` for
 * Invest and StoreFront, respectively.
 */
function rehydrate(config, options) {
    var _a;
    if (options === void 0) { options = {}; }
    return __awaiter(this, void 0, void 0, function () {
        var sdk, policies, application, _b, allowCrossTabSession, myRiskID, _c, journey_version, _d, allowDaoSessionRehydrate, transmitTicketID, deviceData, _e, ticketID, isRefreshableTab, actionEvent, params, authenticationResult, e_1;
        return __generator(this, function (_f) {
            switch (_f.label) {
                case 0:
                    sdk = config.sdk, policies = config.policies, application = config.application, _b = config.allowCrossTabSession, allowCrossTabSession = _b === void 0 ? false : _b, myRiskID = config.myRiskID;
                    _c = options.journey_version, journey_version = _c === void 0 ? 'v2' : _c, _d = options.allowDaoSessionRehydrate, allowDaoSessionRehydrate = _d === void 0 ? false : _d, transmitTicketID = options.ticketID;
                    _e = options.deviceData;
                    if (_e) return [3 /*break*/, 2];
                    return [4 /*yield*/, headers_1.getAllDeviceData()];
                case 1:
                    _e = (_f.sent());
                    _f.label = 2;
                case 2:
                    deviceData = _e;
                    ticketID = transmitTicketID || utils_1.getTransmitTicket();
                    (_a = options.setTicketID) === null || _a === void 0 ? void 0 : _a.call(options, ticketID);
                    utils_1.deleteRefreshTicketID();
                    isRefreshableTab = allowCrossTabSession || utils_1.getIsSameTab() || allowDaoSessionRehydrate;
                    whisper_1.default.info({
                        message: ['Action: rehydrate:', { ticketID: ticketID, deviceData: deviceData, isRefreshableTab: isRefreshableTab }],
                    });
                    if (!ticketID && !isRefreshableTab) {
                        whisper_1.default.debug({ message: 'Rehydration aborted' });
                        return [2 /*return*/, null];
                    }
                    if (!(ticketID && /^pr-/.test(ticketID))) return [3 /*break*/, 5];
                    return [4 /*yield*/, myRiskID.clearUser()];
                case 3:
                    _f.sent();
                    return [4 /*yield*/, myRiskID.triggerActionEvent(types_1.ActionEvent.password_reset)];
                case 4:
                    actionEvent = _f.sent();
                    _f.label = 5;
                case 5:
                    params = __assign(__assign(__assign(__assign({ ticketID: ticketID,
                        journey_version: journey_version }, utils_1.getAppInfoParams(options.application || application)), { authData: __assign({}, deviceData) }), utilities_1.getFlagParams(options)), (actionEvent ? { actionToken: actionEvent.actionToken } : {}));
                    _f.label = 6;
                case 6:
                    _f.trys.push([6, 9, , 10]);
                    return [4 /*yield*/, utils_1.deleteTransmitSession(config)];
                case 7:
                    _f.sent();
                    return [4 /*yield*/, sdk.invokeAnonymousPolicy(policies.discovery, params, null)
                        // If this rehydrate is from a successful storefront login we need to reset the interrupts sessionStorage flag
                    ];
                case 8:
                    authenticationResult = _f.sent();
                    // If this rehydrate is from a successful storefront login we need to reset the interrupts sessionStorage flag
                    if (ticketID && /^sf-/.test(ticketID)) {
                        utilities_1.resetInterrupts();
                    }
                    utils_1.deleteTransmitSession(config);
                    return [2 /*return*/, { deviceData: deviceData, authenticationResult: authenticationResult, journey_version: journey_version }];
                case 9:
                    e_1 = _f.sent();
                    handleRehydrationFailure(e_1);
                    return [2 /*return*/, null];
                case 10: return [2 /*return*/];
            }
        });
    });
}
exports.rehydrate = rehydrate;
exports.default = utils_1.asSession(utils_1.asUIConsumer(rehydrate));
//# sourceMappingURL=rehydrate.js.map
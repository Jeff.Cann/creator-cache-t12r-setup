"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicePrint = void 0;
var react_1 = __importStar(require("react"));
var constants_1 = require("../constants");
var DevicePrint = function () {
    var getInitiateData = function (elementId) {
        var _a;
        var host = window.location.hostname === 'secure.ally.com' ? '' : 'uat.';
        var FORTY_ONE_P_RESOURCE_URL = "https://" + host + "globalsiteanalytics.com/resource/resource.png";
        var FORTY_ONE_P_HDM_URL = "https://" + host + "globalsiteanalytics.com/service/hdim";
        try {
            (_a = window.nsp) === null || _a === void 0 ? void 0 : _a.afn(null, [
                FORTY_ONE_P_RESOURCE_URL,
                FORTY_ONE_P_HDM_URL,
                elementId,
            ]);
        }
        catch (error) {
            // eslint-disable-next-line no-console
            console.error("41P initiate method failed: ", error);
        }
    };
    react_1.useEffect(function () {
        getInitiateData(constants_1.DEVICE_PRINT_ELEMENT_ID);
    }, []);
    return (react_1.default.createElement("input", { "data-testid": "41p-device-print", hidden: true, id: constants_1.DEVICE_PRINT_ELEMENT_ID, name: constants_1.DEVICE_PRINT_ELEMENT_ID }));
};
exports.DevicePrint = DevicePrint;
//# sourceMappingURL=DevicePrint.js.map